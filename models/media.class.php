<?php

/**
* CLASS MEDIA
*/
class media extends Base_SQL
{
    protected $id, $id_type, $lien, $libelle;
    
    public function __construct()
    {
        parent::__construct();
    }

    // SETTERS

    public function set_id($id){
        $this->id = $id;
    }

    public function set_id_type($data){
        $this->id_type = trim($data);
    }

    public function set_lien($data){
        $this->lien = trim($data);
    }

    public function set_libelle($data){
        $this->libelle = trim($data);
    }

    // GETTERS

    public function get_id($id){
        return $this->id;
    }

    public function get_id_type($title){
        return $this->id_type;
    }

    public function get_lien($title){
        return $this->lien;
    }

    public function get_libelle($title){
        return $this->libelle;
    }
}
