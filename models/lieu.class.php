<?php

/**
* CLASSE LIEU
*/
class lieu extends Base_SQL
{
    protected $id, $adresse, $logo, $code_postal, $ville;
    
    public function __construct()
    {
        parent::__construct();
    }

    // SETTERS

    public function set_id($id){
        $this->id = $id;
    }

    public function set_adresse($data){
        $this->adresse = trim($data);
    }

    public function set_code_postal($data){
        $this->code_postal = trim($data);
    }

    public function set_ville($data){
        $this->ville = trim($data);
    }

    // GETTERS

    public function get_id($id){
        return $this->id;
    }

    public function get_adresse($title){
        return $this->adresse;
    }

    public function get_logo($title){
        return $this->logo;
    }

    public function get_code_postal($title){
        return $this->code_postal;
    }

    public function get_ville($title){
        return $this->ville;
    }
}
