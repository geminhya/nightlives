<?php

/**
*
*/
class article extends Base_SQL
{
    protected $id, $id_redacteur, $titre_article, $id_article, $contenu, $id_media, $date_parution, $date_modification, $type_commentaire, $archive;
    protected $facebook, $twitter, $youtube, $soundcloud, $instagram;

    public function __construct()
    {
        parent::__construct();
    }

    // SETTERS

    public function set_id($id){
        $this->id = $id;
    }

    public function set_id_redacteur($data){
        $this->id_redacteur = trim($data);
    }

    public function set_titre_article($data){
        $this->titre_article = trim($data);
    }
    public function set_id_article($data){
        $this->id_article = trim($data);
    }

    public function set_type_commentaire($data){
        $this->type_commentaire = trim($data);
    }

    public function set_contenu($data){
        $this->contenu = trim($data);
    }

    public function set_id_media($data){
        $this->id_media = trim($data);
    }

    public function set_date_parution($data){
        $this->date_parution = trim($data);
    }

    public function set_date_modification($data){
        $this->date_modification = trim($data);
    }
    public function set_archive($archive){
        $this->archive = trim($archive);
    }
    // GETTERS
    public function get_id($id){
        return $this->id;
    }

    public function get_id_redacteur($title){
        return $this->id_redacteur;
    }

    public function get_id_article($data){
        return $this->id_article;
    }

    public function get_type_commentaire($data){
        return $this->type_commentaire;
    }

    public function get_titre_article($content){
        return $this->titre_article;
    }

    public function get_contenu($content){
        return $this->contenu;
    }

    public function get_id_media($content){
        return $this->id_media;
    }
    public function get_date_parution($data){
        return $this->date_parution;
    }
    public function get_archive($archive){
        return $this->archive;
    }
}
