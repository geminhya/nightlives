<?php
class commentaire extends Base_SQL{
    protected $id, $contenu, $id_user, $id_element, $type_element, $date_comment, $valid;

    public function __construct()
    {
        parent::__construct();
    }

    public function set_Id($data){
        $this->id = $data;
    }
    public function set_contenu($data){
        $this->contenu = $data;
    }
    public function set_id_user($data){
        $this->id_user = $data;
    }
    public function set_id_element($data){
        $this->id_element = $data;
    }
    public function set_type_element($data){
        $this->type_element = $data;
    }
    public function set_date_comment($data){
        $this->date_comment = $data;
    }
    public function set_valid($data){
        $this->valid = $data;
    }
    function getId() {
        return $this->id;
    }
    function getContenu() {
        return $this->contenu;
    }
    function getId_user() {
        return $this->id_user;
    }
    function getId_element() {
        return $this->id_article;
    }
    function getType_element() {
        return $this->date_ecriture;
    }
    function getDate_comment() {
        return $this->type_commentaire;
    }
    function getValid(){
        return $this->valid;
    }


}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

