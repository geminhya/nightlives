<?php  if(!empty($slides)){ ?>
<section class="slider_container">
	<div class="home_slider_arrows arrows"><div class="left"></div><div class="right"></div></div>
	<div class="home_slider">
		<div class="long_container">
		<?php foreach ($slides as $key => $slide) { ?>
			<div class="slide" style="background-image: url(<?php echo $slide['photo'] ?>);" id="home_slider_<?php echo $key + 1; ?>">
				<a href="<?php echo $slide['lien']; ?>#">
					<p class="big"><?php echo $slide['title'] ?></p>
					<p><?php echo $slide['subtitle'] ?></p>
				</a>
			</div>
		<?php } ?>
		</div>
		<div class="counting"></div>
	</div>
</section>
<?php }else{ ?>
<div class="home_space"></div>
<?php } ?>
<?php if(!empty($festivals)){ ?>
<section class="events homepage_events">
	<div class="content">
		<h1>Événements à venir</h1>
		<h2>Festivals</h2>
		<div class="list_events">
			<?php foreach ($festivals as $key => $value) { ?>
				<div class="event">
					<div class="logo_event"><img src="<?php echo $value['photo']; ?>"></div>
					<img class="top" src="<?php echo $value['banner']; ?>">
					<h3><?php echo utf8_encode($value['libelle']); ?></h3><p><?php echo print_date($value['date_deb'], $value['date_fin']); ?></p>
					<a class="call_to" href="<?php echo $home_url; ?>/event/<?php echo $value['easy_name']; ?>">Let's Party</a>
				</div>
			<?php } ?>
			<div class="lineclear"></div>
			<a class="call_to see_more" href="<?php echo $home_url; ?>/festivals">Voir tous les festivals</a>
		</div>
	</div>
</section>
<?php } ?>
<?php if(!empty($concerts)){ ?>
<section class="events grey">
	<div class="content">
		<h2>Concerts</h2>
		<div class="list_events">
			<?php foreach ($concerts as $key => $value) { ?>
				<div class="event">
					<div class="logo_event"><img src="<?php echo $value['photo']; ?>"></div>
					<img class="top" src="<?php echo $value['banner']; ?>">
					<h3><?php echo utf8_encode($value['libelle']); ?></h3><p><?php echo print_date($value['date_deb'], $value['date_fin']); ?></p>
					<a class="call_to" href="<?php echo $home_url; ?>/event/<?php echo $value['easy_name']; ?>">Let's Party</a>
				</div>
			<?php } ?>
			<div class="lineclear"></div>
			<a class="call_to see_more white" href="<?php echo $home_url; ?>/concerts">Voir tous les concerts</a>
		</div>
	</div>
</section>
<?php } ?>
<?php if(!empty($soirees)){ ?>
<section class="events black">
	<div class="content">
		<h2>Soirées</h2>
		<div class="list_events">
			<?php foreach ($soirees as $key => $value) { ?>
				<div class="event">
					<div class="logo_event"><img src="<?php echo $value['photo']; ?>"></div>
					<img class="top" src="<?php echo $value['banner']; ?>">
					<h3><?php echo utf8_encode($value['libelle']); ?></h3><p><?php echo print_date($value['date_deb'], $value['date_fin']); ?></p>
					<a class="call_to" href="<?php echo $home_url; ?>/event/<?php echo $value['easy_name']; ?>">Let's Party</a>
				</div>
			<?php } ?>class="call_to" href="<?php echo $home_url; ?>/event">Let's Party</a>
			</div>
			<div class="lineclear"></div>
			<a class="call_to see_more white" href="<?php echo $home_url; ?>/soirees">Voir toutes les soirées</a>
		</div>
	</div>
</section>
<?php } ?>
<section class="events artists">
	<div class="content">
		<h2>Artistes</h2>
		<div class="list_artists">
			<?php if(!empty($artistes)) { foreach ($artistes as $key => $element) { ?>
		        <div class="artist">
		          <div class="intern_artist">
		            <div class="logo_artist"><a href="<?php echo FRONT_URL; ?>artiste/<?php echo $element['easy_name']; ?>"><img src="<?php echo $element['photo']; ?>"></a></div>
		            <h3><a href="<?php echo FRONT_URL; ?>artiste/<?php echo $element['easy_name']; ?>"><?php echo utf8_encode($element['pseudo']); ?></a></h3>
		            <a class="call_to" href="<?php echo FRONT_URL; ?>artiste/<?php echo $element['easy_name']; ?>">Événements à venir</a>
		          </div>
		        </div>
		      <?php } } ?>
			<div class="lineclear"></div>
			<a class="call_to see_more" href="<?php echo $home_url; ?>/artistes">Voir tous les artistes</a>
		</div>
	</div>
</section>
