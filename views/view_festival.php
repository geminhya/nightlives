<div class="single_artist single_event">
  <section class="slider_container" style="background-image: url(images/temp/ultramusicfestival.jpg);">
    <h1 class="nameevent">Ultra Music Festival</h1>
  </section>

  <section class="eventheader">
    <div class="content">
      <div class="bioevent">
          <img src="<?php echo $home_url; ?>/images/temp/ultramusicfestival-logo.png" class="vignetteevent">
          <div class="minibio">
            <p class="titreevent">- Ultra Music Festival -</p>
            <p>Miami, Etats-Unis - 18,19,20 Mars 2016</p>
            <div class="social">
              <ul>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/Facebook.png"></a></li>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/Twitter.png"></a></li>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/YouTube.png"></a></li>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/Instagram.png"></a></li>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/Soundcloud.png"></a></li>
              </ul>

              <div class="lineclear"></div>
            </div>
        </div>
        <div class="bioevent">
          <?php
          $bio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eleifend ligula urna, vel tristique lacus placerat eget. Mauris vestibulum erat quis magna eleifend venenatis. Pellentesque ipsum mi, volutpat id augue ac, consectetur laoreet nunc. Maecenas ullamcorper at mauris non dictum. Sed dignissim massa non nisi laoreet, a efficitur erat iaculis. Phasellus venenatis leo ut augue ullamcorper dapibus. Aliquam in consequat mi, sed dignissim nisl. Etiam bibendum a lacus quis dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

            Vivamus nunc lacus, cursus eget odio et, tempor condimentum ante. Nulla facilisis maximus justo, scelerisque porttitor arcu faucibus a. Vivamus sit amet lorem et mi fringilla ornare in at ante. Sed non arcu quis ipsum finibus varius. Cras ultrices blandit elit, eget vulputate nisi dictum nec. Nunc fermentum feugiat consectetur. Aliquam erat volutpat. Etiam cursus ex molestie enim aliquet aliquet. Ut euismod scelerisque lacinia. Phasellus lobortis imperdiet faucibus. Nunc ac ante eget risus condimentum tempor. Ut eu nisl at arcu sollicitudin facilisis ut vel urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras ultrices euismod diam, at blandit mi tincidunt eget. Quisque semper est vel vulputate venenatis. Proin feugiat elementum ipsum hendrerit consequat. Etiam ac dictum risus. Morbi aliquet nisl non leo interdum bibendum. Aenean ac justo vitae neque sagittis lacinia maximus eget metus.

            Mauris eleifend enim ac velit egestas, vel eleifend nibh varius. Nullam turpis dui, dictum non gravida id, convallis in lorem. Cras mattis massa et justo pharetra, a commodo lectus mollis. Integer iaculis dictum scelerisque. Cras sit amet hendrerit dolor. Donec efficitur velit a nulla pharetra, eu volutpat dolor pretium. Sed malesuada ornare est, vel maximus dui faucibus non. Donec in venenatis dolor. Maecenas urna mi, varius in ex eu, fringilla maximus est. Aenean a molestie ligula, hendrerit condimentum enim. Cras viverra, ligula a ullamcorper cursus, mauris lacus sollicitudin odio, id egestas ante est placerat tortor. Fusce id rhoncus quam. Sed laoreet eros sed sem iaculis porttitor. Nullam ut tellus congue justo scelerisque mattis ut vel ligula.

            Fusce quis odio luctus, consequat nisl sed, porttitor odio. Nullam pretium lectus et risus blandit, non bibendum lectus fringilla. Quisque nisi purus, vestibulum ac urna non, suscipit egestas mi. Vivamus porttitor, lorem at tincidunt pellentesque, nisi libero convallis neque, ac eleifend mi lorem eu felis. Sed quis varius lacus. Nulla congue, felis sit amet mattis tristique, nibh metus vestibulum elit, eu semper est leo sit amet orci. Praesent sed enim lobortis sapien porta maximus. Mauris vitae interdum arcu, at rutrum lectus.";
          echo "<p>".nl2br($bio)."</p>";
          ?>
        </div>
      </div>
    </div>
  </section>



  <section class="events black">
    <div class="content">
      <div>
        <h2 class="presenta">Line Up</h2>
      </div>
      <div class="lineup">
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="artiste_lineup">
          <a href="page_artiste.php">
            <img src="<?php echo $home_url; ?>/images/temp/tchami_vignette.png">
            <p class="pseudo">Tchami</p>
          </a>
        </div>
        <div class="lineclear"></div>
      </div>
    </div>
  </section>

  <section class="fans">
    <div class="content">
        <h2><span class="nbFans">5</span> membres participent à cet event!</h2>
            <div class="fan">
              <img src="<?php echo $home_url; ?>/images/membres/michael.png">
              <p class="pseudo">Le Mayhem</p>
            </div>
            <div class="fan">
              <img src="<?php echo $home_url; ?>/images/membres/noemie.png">
              <p class="pseudo">Geminhya</p>
            </div>
            <div class="fan">
              <img src="<?php echo $home_url; ?>/images/membres/momo.png">
              <p class="pseudo">Tapeurdupieddu75</p>
            </div>
            <div class="fan">
              <img src="<?php echo $home_url; ?>/images/membres/nadir.png">
              <p class="pseudo">Eyes_lover</p>
            </div>
            <div class="fan">
              <img src="<?php echo $home_url; ?>/images/membres/victor.png">
              <p class="pseudo">Vicodu75</p>
            </div>

            <div class="lineclear"></div>
            <a class="call_to long" href="#">Je participe moi aussi !</a>


    </div>
  </section>

  <section class="commentaires">
    <h2>Commentaires</h2>
    <article>
      <div>
        <img src="<?php echo $home_url; ?>/images/membres/michael.png">
      </div>
      <div class="blocCommentaire">
        <p class="pseudo">Le Mayhem</p>
        <p class="timeComment">Posté le 05-02-2016 à 12:06</p>
        <hr>
        <p>Ceci est un commentaire très instructif sur Tchami que j'aime beaucoup, j'espère que vous aussi car il est très sympa et fait des musiques de qualité je trouve d'accord? ok allez salut bisous @+</p>
      </div>
    </article>
    <article>
      <div>
        <img src="<?php echo $home_url; ?>/images/membres/momo.png">
      </div>
      <div class="blocCommentaire">
        <p class="pseudo">Tapeurdupieddu75</p>
        <p class="timeComment">Posté le 05-02-2016 à 12:08</p>
        <hr>
        <p>Moi j'aime que les sons qui permettent de taper du pied en toute impunité, des sons plutôt sheitainisés et Tchami rempli parfaitement ces critères de choix, je recommande.</p>
      </div>
    </article>
    <hr>
    <article>
      <h3>Participez vous aussi au débat!</h3>
      <p>Saisissez ici votre commentaire</p>
      <textarea></textarea>
      <a class="call_to" href="#">Envoyer!</a>
    </article>

  </section>
</div>