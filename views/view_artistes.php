<section class="events artists list_all_artists" style="background-image: url(<?php echo FRONT_URL . $background_url; ?>);" >
    <h1><?php echo $title; ?></h1>
    <?php if(!empty($subtitle)) { ?>
    <p class="seacrh_subtitle"><?php echo $subtitle; ?></p>
    <?php } ?>
    <div class="filters">
      <div class="content">

        <!--Filter-->
        <form name="filter_form" action="" method="get" id="filter_form_artistes">
          <label>Voir</label>
          <select name="type">
            <option value="all" <?php if(!empty($_GET['type']) && $_GET['type'] == 'all' ) { echo 'selected'; } ?>>Tous</option>
            <option value="electro" <?php if(!empty($_GET['type']) && $_GET['type'] == 'electro' ) { echo 'selected'; } ?>>Electro</option>
            <option value="pop" <?php if(!empty($_GET['type']) && $_GET['type'] == 'pop' ) { echo 'selected'; } ?>>Pop</option>
            <option value="rock" <?php if(!empty($_GET['type']) && $_GET['type'] == 'rock' ) { echo 'selected'; } ?>>Rock</option>
          </select>

          <label>Trier par</label>
          <select name="order">
            <option value="pseudo" <?php if(!empty($_GET['order']) && $_GET['order'] == 'pseudo' ) { echo 'selected'; } ?>>Nom</option>
            <option value="rate" <?php if(!empty($_GET['order']) && $_GET['order'] == 'rate' ) { echo 'selected'; } ?>>Popularité</option>
            <option value="date" <?php if(!empty($_GET['order']) && $_GET['order'] == 'date' ) { echo 'selected'; } ?>>Date d'ajout</option>
          </select>

          <label>Pays</label>
          <select name="pays">
            <option value="all" <?php if(!empty($_GET['pays']) && $_GET['pays'] == 'all' ) { echo 'selected'; } ?>>Tous</option>
            <?php foreach ($select_pays as $key => $pays) { ?>
              <option value="<?php echo $pays['pays']; ?>" <?php if(!empty($_GET['pays']) && $_GET['pays'] == $pays['pays'] ) { echo 'selected'; } ?>><?php echo $pays['pays']; ?></option>
            <?php } ?>
          </select>
          <input type="text" class="text" name="text" value="<?php if(!empty($_GET['text'])){ echo $_GET['text']; } ?>" placeholder="Rechercher un artiste" />
          <input class="submit" type="submit" value="Valider" />
        </form>
      </div>
    </div>
    <!--Fin filter-->

  <div class="content">
    <div class="list_artists">
      <?php if(!empty($elements)) { foreach ($elements as $key => $element) { ?>
        <div class="artist">
          <div class="intern_artist">
            <div class="logo_artist"><a href="<?php echo FRONT_URL; ?>artiste/<?php echo $element['easy_name']; ?>"><img src="<?php echo $element['photo']; ?>"></a></div>
            <h3><a href="<?php echo FRONT_URL; ?>artiste/<?php echo $element['easy_name']; ?>"><?php echo utf8_encode($element['pseudo']); ?></a></h3>
            <a class="call_to" href="<?php echo FRONT_URL; ?>artiste/<?php echo $element['easy_name']; ?>">Événements à venir</a>
          </div>
        </div>
      <?php } } ?>

      <div class="lineclear"></div>
    </div>
      <!--Pagination-->
        <?php if($pages > 1){ ?>
            <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
            <?php if($current_page > 1){ ?>
                <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
            <?php } ?>

            <?php if($current_page < $pages){ ?>
                <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
            <?php } ?>
            </div>
            <div class="lineclear"></div>
        <?php } ?>
        <!--Fin pagination-->

  </div>
</section>
