<div class="single_artist">
  <section class="slider_container" style="background-image: url(<?php echo (isset($banner)) ? FRONT_URL . $banner : ''; ?>);">
    <h1 class="nameartiste"><?php echo utf8_encode($pseudo); ?></h1>
  </section>

  <section class="artisteheader">
    <div class="content">
      <div class="bioartiste">
          <?php if(isset($photo)){ ?>
          <img src="<?php echo FRONT_URL . $photo; ?>" class="vignetteartiste">
          <?php } ?>
          <span class="minibio">
            <p class="titreartiste"><?php echo utf8_encode($pseudo); ?></p>
            <?php if(!empty($pays)){ ?><p><?php echo $pays ?></p><?php } ?>
            <div class="social">
              <ul>
              <?php if(!empty($facebook)){ ?><li><a target="_blank" href="<?php echo $facebook; ?>"><img src="<?php echo $home_url; ?>/images/social/Facebook.png"></a></li><?php } ?>
              <?php if(!empty($twitter)){ ?><li><a target="_blank" href="<?php echo $twitter; ?>"><img src="<?php echo $home_url; ?>/images/social/Twitter.png"></a></li><?php } ?>
              <?php if(!empty($youtube)){ ?><li><a target="_blank" href="<?php echo $youtube; ?>"><img src="<?php echo $home_url; ?>/images/social/YouTube.png"></a></li><?php } ?>
              <?php if(!empty($instagram)){ ?><li><a target="_blank" href="<?php echo $instagram; ?>"><img src="<?php echo $home_url; ?>/images/social/Instagram.png"></a></li><?php } ?>
              <?php if(!empty($soundcloud)){ ?><li><a target="_blank" href="<?php echo $soundcloud; ?>"><img src="<?php echo $home_url; ?>/images/social/Soundcloud.png"></a></li><?php } ?>
              </ul>

          <div class="lineclear"></div>
            </div>
        </span>
        <div class="textebio">
          <?php echo "<p>".nl2br($biographie)."</p>"; ?>
          <div class="playersoundcloud">
            <?php if(!empty($iframe)){ ?> <div class="iframe_container"><?php echo $iframe; ?></div> <?php  } ?>
          </div>
        </div>
      </div>
    </div>
  </section>


  <?php if(!empty($events)){ ?>
    <section class="events black">
      <div class="content">
        <div>
          <h2 class="presenta">Il sera présent à...</h2>
        </div>

        <div class="lineclear"></div>
                <ul class="list_events">
                    <?php foreach ($events as $key => $user) { ?>

                    <li>
                        <div class="event">
                            <a href="<?php echo $home_url; ?>/event/<?php  echo $user['easy_name']; ?>">
                                <div class="logo_event">
                                   <img src="<?php if(!empty($user["photo"])){ echo FRONT_URL . $user["photo"]; }; ?>">
                                </div>
                                <img class="top" src="<?php if(!empty($user["banner"])){ echo FRONT_URL . $user["banner"]; }; ?>" alt="<?php echo (isset($alt)) ? $alt : ''; ?>">
                            </a>
                            <h3>
                                <?php  echo utf8_encode($user["libelle"]); ?>
                            </h3>
                            <p>du <?php echo utf8_encode( date("d-m-Y", strtotime($user["date_deb"]))); ?> au <?php echo utf8_encode( date("d-m-Y", strtotime($user["date_fin"]))); ?></p>
                            <a class="call_to" href="<?php echo $home_url; ?>/event?id=<?php  echo $user['id']; ?>">Let's Party</a>
                        </div>
                    </li>
                    <?php } ?>
                </ul>


      </div>
    </section>
  <?php } ?>

  <section class="fans">
    <div class="content">
        <?php if(!empty($participants_results)) { ?>
            <h2><span class="nbFans"><?php echo $count; ?></span> membres sont fans de cet artiste!</h2>
             <?php foreach ($participants_results as $key => $participant) {  ?>
                <div class="fan">
                    <a href="<?php echo FRONT_URL; ?>user?pseudo=<?php echo $participant['pseudo']; ?>">
                  <img src="<?php echo $home_url . '/' .$participant['photo']; ?>">
                  <p class="pseudo"><?php echo $participant['pseudo']; ?></p>
                </div>
            <?php } ?>
        <?php } else { ?>
            <h2>Pas encore de fans pour cet artiste!</h2>
        <?php } ?>
            <div class="lineclear"></div>
            <?php if(!empty($user)) { ?><div id="participate_container"><?php echo $participate_button ?></div><?php } ?>
                    <?php if(empty($user)) { ?> <div class="call_to long"> <a  id="bouton_participe"  class="callPopin" href="#connexion">Je suis fan moi aussi !</a></div> <?php } ?>
    </div>
  </section>

  <section class="commentaires">
    <h2>Commentaires</h2>
        <?php if(!empty($commentaires_results)) { ?>
          <?php foreach ($commentaires_results as $key => $commentaire) {  ?>
        <article>
          <div>
            <img src="<?php echo $home_url . '/' .$commentaire['photo']; ?>">
          </div>
          <div class="blocCommentaire">
              <p class="pseudo"><?php echo $commentaire['pseudo']; ?></p>
            <p class="timeComment">Posté le <?php echo date("d-m-Y", strtotime($commentaire['date_comment'])); ?></p>
              <p class="valid"><?php if($commentaire['valid'] == 1) { ?> Validé <?php } ?></p>
              <p class="non_valid"><?php if($commentaire['valid'] == 0) { ?> En cours de validation <?php } ?></p>
            <p><?php echo utf8_encode ( $commentaire['contenu']); ?> </p>
          </div>
        </article>
      <?php } ?>
    <?php } ?>

    <hr>
    <article>
      <h3>Participez vous aussi au débat!</h3>
      <p>Saisissez ici votre commentaire</p>
        <textarea id="fun_commentaire"></textarea>
        <?php if(!empty($user)) { ?>
        <?php echo $commente_button; } ?>
        <?php if(empty($user)) { ?>
            <div class="call_to long"> <a  id="bouton_fun_commentaire"  class="callPopin" href="#connexion">Envoyer !</a></div>
        <?php } ?>
    </article>

  </section>
</div>
