<section class="events artists list_all_artists" id="section_contribution" style="background-image: url(<?php echo FRONT_URL . $background_url; ?>);" >
    <h1><?php echo $title; ?></h1>
  <div class="content">
    <p class="description">Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis sudoribus induratae.</p>
    <div class="list_artists">
      <div class="presentation_blocs">
        <div class="presentation_bloc bloc_event">
          <a href="#contribuer_event"><p>Proposer un évènement</></a>
        </div>
        <div class="presentation_bloc bloc_event">
          <a href=""><p>Proposer un artiste</></a>
        </div>
        <div class="presentation_bloc bloc_event">
          <a href=""><p>Proposer un lieu</></a>
        </div>
      </div>
      <div class="lineclear"></div>

    </div>

    <div class="contribuer_bloc" id="contribuer_event">
      <h2>Proposer un évènement</h2>
      <form name="add_event" method="post" action="" class="form_container" id="form_contribuer_add_event">
          <input placeholder="Nom de l'event" class="text" type="text" name="event[libelle]" value="<?php echo (!empty($datas['event']['libelle'])) ? $datas['event']['libelle'] : ''; ?>" />
          <input placeholder="Date de début" class="text" type="date" name="event[date_deb]" value="<?php echo (!empty($datas['event']['date_deb'])) ? $datas['event']['date_deb'] : ''; ?>" />
          <input placeholder="Date de fin" class="text" type="date" name="event[date_fin]" value="<?php echo (!empty($datas['event']['date_fin'])) ? $datas['event']['date_fin'] : ''; ?>" />
          <select name="event[id_type_event]">
            <option value="X" selected>Type d'evénement</option>
            <option value="1" <?php if(!empty($datas['event']['id_type_event']) && $datas['event']['id_type_event'] == "1") { echo 'selected'; } ?> >Festival</option>
            <option value="2" <?php if(!empty($datas['event']['id_type_event']) && $datas['event']['id_type_event'] == "2") { echo 'selected'; } ?> >Concert</option>
            <option value="3" <?php if(!empty($datas['event']['id_type_event']) && $datas['event']['id_type_event'] == "3") { echo 'selected'; } ?> >Soirée</option>
          </select>
          <select name="event[id_lieu]">
            <option value="X" selected>Lieu</option>
            <?php if(!empty($lieux)){foreach ($lieux as $key => $lieu) { ?>
            <option value="<?php echo $lieu['id'] ?>" <?php if(!empty($datas['event']['id_lieu']) && $datas['event']['id_lieu'] == $lieu['id']) { echo 'selected'; } ?>  ><?php echo $lieu['code_postal'] . ' - ' . utf8_encode($lieu['libelle']); ?></option>
          <?php } } ?>
          </select>
          <div class="autocomplete  autocomplete_artistes">
            <div class="autocomplete_valid <?php if(empty($artistes_event) || empty($artistes_event[0])){ echo 'hidden'; } ?>">
              <?php if(!empty($artistes_event) && !empty($artistes_event[0])){ 
                foreach ($artistes_event as $key => $art) {
                  $data = get_artist_by_id($art);
                  echo "<span class='autocomplete_valid_span' data-id='" . $art . "' id='id_artistes_" . $art . "'>" . $data['pseudo'] . "</span>";
                }
                } ?>
            </div>
            <div class="autocomplete_input_container">
              <input type="text" class="autocomplete_input" name="autocomplete[artistes]" placeholder="Artistes Présents" />
              <input type="hidden" name="event[artistes]" value="" />
              <div class="autcomplete_list" data-element="artiste">
              </div>
            </div>
          </div>
          <textarea name="event[informations]" placeholder="Informations"></textarea>
          <input type="submit" value="Valider" class="submit">
      </form>
    </div>
  </div>
</section>

<section class="events" style="background-color: #FFF;">
  <div class="content">
    <h2>Vos dernières contributions</h2>
    <div class="list_events">
      <div class="lineclear"></div>
      <a class="call_to see_more" href="<?php echo $home_url; ?>/artistes">Voir tous les artistes</a>
    </div>
  </div>
</section>