<section id="page_festivals" class="events artists list_all_artists_festivals">
  <div class="content">
    <h1>Festivals</h1>
    <?php if(!empty($subtitle)) { ?>
    <p class="seacrh_subtitle"><?php echo $subtitle; ?></p>
    <?php } ?>
    <div class="list_artists">
        <section class="events">

            <!-- Filter -->
            <div class="filters">
              <div class="content">
                <form name="filter_form" action="" method="get" id="filter_form_artistes">


                  <label>Trier par type</label>
                  <select name="type">
                    <option value="all" <?php if(!empty($_GET['type']) && $_GET['type'] == 'all' ) { echo 'selected'; } ?>>Tous</option>
                    <option value="electro" <?php if(!empty($_GET['type']) && $_GET['type'] == 'electro' ) { echo 'selected'; } ?>>Electro</option>
                    <option value="pop" <?php if(!empty($_GET['type']) && $_GET['type'] == 'pop' ) { echo 'selected'; } ?>>Pop</option>
                    <option value="rock" <?php if(!empty($_GET['type']) && $_GET['type'] == 'rock' ) { echo 'selected'; } ?>>Rock</option>
                  </select>

                  <input type="text" class="text" name="text" value="<?php if(!empty($_GET['text'])){ echo $_GET['text']; } ?>" placeholder="Rechercher un artiste" />
                  <input class="submit" type="submit" value="Valider" />
                </form>
              </div>
            </div>
            <!-- Fin filter -->

            <div class="content">
                <h2>Les festivals à venir <?php echo $head_title; ?></h2>
                <div class="lineclear"></div>
                <ul class="list_events">
                    <?php foreach ($elements as $key => $user) { ?>

                    <li>
                        <div class="event">
                            <a href="<?php echo $home_url; ?>/event/<?php  echo $user['easy_name']; ?>">
                                <div class="logo_event">
                                   <img src="<?php if(!empty($user["photo"])){ echo $home_url . '/' . $user["photo"]; }; ?>">
                                </div>
                                <img class="top" src="<?php if(!empty($user["banner"])){ echo $home_url . '/' . $user["banner"]; }; ?>" alt="<?php echo (isset($alt)) ? $alt : ''; ?>">
                            </a>
                            <h3>
                                <?php  echo utf8_encode($user["libelle"]); ?>
                            </h3>
                            <p>du <?php echo utf8_encode( date("d-m-Y", strtotime($user["date_deb"]))); ?> au <?php echo utf8_encode( date("d-m-Y", strtotime($user["date_fin"]))); ?></p>
                            <a class="call_to" href="<?php echo $home_url; ?>/event/<?php  echo $user['easy_name']; ?>">Let's Party</a>
                        </div>
                    </li>
                    <?php } ?>
                </ul>

            </div>
        </section>
        
        <!--Pagination-->
        <?php if($pages > 1){ ?>
            <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
            <?php if($current_page > 1){ ?>
                <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
            <?php } ?>

            <?php if($current_page < $pages){ ?>
                <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
            <?php } ?>
            </div>
            <div class="lineclear"></div>
        <?php } ?>
        <!--Fin pagination-->
      
      <div class="lineclear"></div>
    </div>
  </div>
</section>
