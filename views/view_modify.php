<div class="single_user_account">
    <section class="user_dashboard">
        <div class="content">
            <div class="user_menu">
                <div class="infos">
                    <div class="img">
                        <!-- <div class="change_picture"><img src="<?php //echo $home_url; ?>/images/membres/camera_icon.png"></div> -->
                        <img src="<?php echo $home_url."/".$profile_pic; ?>">
                    </div>
                    <h1><?php echo $user[0]['pseudo'];?></h1>
                    <hr>
                    <p class="mini_bio">"<?php echo $user[0]['mini_bio'] ?>"</p>

                </div>
            </div>
              <?php echo $section ?>

              <div class="user_data">
                <a href="<?php echo $home_url ?>/user?pseudo=<?php echo $user[0]['pseudo'] ?>">Revenir au profil</a>
              </div>
        </div>

        <div class="lineclear"></div>
        </div>
    </section>
</div>