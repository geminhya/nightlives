<script>myString = myString.replace(String.fromCharCode(65279), "" );</script>
<section id="page_festivals" class="events artists list_all_articles">
    <div class="page_article">
        <h1>Les Articless</h1>
        <?php if (!empty($subtitle)) { ?>
            <p class="seacrh_subtitle"><?php echo $subtitle; ?></p>
        <?php } ?>
        <div class="list_artists">
            <section class="events">

                <div class="content">
                    <div class="redacteurs">
                        <p onclick="afficheArticle(22)" class="redacteur"><img id="noemie" src="<?php echo $home_url; ?>/<?php echo $pp_noemie['lien']; ?>" alt="Noémie"> <span>Noémie</span></p>
                        <p onclick="afficheArticle(74)" class="redacteur"><img id="momo" src="<?php echo $home_url; ?>/<?php echo $pp_momo['lien']; ?>" alt="Momo"> <span>Momo</span></p>
                        <p onclick="afficheArticle(72)" class="redacteur"><img id="michael" src="<?php echo $home_url; ?>/<?php echo $pp_michael['lien']; ?>" alt="Lemayhem"> <span>Michael</span></p>
                        <p onclick="afficheArticle(73)" class="redacteur"><img id="nadir" src="<?php echo $home_url; ?>/<?php echo $pp_nadir['lien']; ?>" alt="Nadir"> <span>Nadir</span></p>
                    </div>
                    <div class="all_articles">
                        <?php
                        foreach ($articles as $kk => $vv) {
                            ?>
                            <div class="article <?php echo $vv['id_redacteur']; ?>" id="<?php echo $vv['id'] ?>" post-id="<?php echo $vv['id_redacteur']; ?>">
                                <h2 onclick="afficheArticleEntier('<?php echo $vv["id"] ?>')"><?php echo $vv['titre_article']; ?><br>
                                    <span id="date_publication">Publié le <?php echo date("d/m/Y", strtotime($vv['date_parution'])); ?> - par <?php echo $vv['prenom'] ?></span>
                                </h2>
                                <hr>
                                <?php if (!empty($vv['photo'])) { ?>
                                    <span class="image_article">
                                        <img class="thumbnail" post-id="<?php echo $kk; ?>" src="<?php echo FRONT_URL . $vv['photo']; ?>" alt="<?php echo (isset($alt)) ? $alt : ''; ?>">
                                    </span>
                                <?php } ?>
                                <div class="text">

                                    <p class="paragraphe_article" post-id="<?php echo $kk; ?>"><?php echo $vv['contenu']; ?></p>
                                    <p class="lire_plus" post-id="<?php echo $kk; ?>">Continuer la lecture</p>
                                    <p class="lire_moins" post-id="<?php echo $kk; ?>">Cacher</p>

                                </div>
                                <?php if (!empty($_SESSION)) {
                                    ?>
                                    <div class="commenter">
                                        <input type="button" value="Commenter cet article" post-id="<?php echo $kk; ?>" name="clickcomment" class="inputComment clickcomment" id="comment">
                                    </div>
                                    <p class="afficheCom" post-id="<?php echo $kk; ?>">Afficher les commentaires</p>
                                    <p class="cacherCom" post-id="<?php echo $kk; ?>">Cacher les commentaires</p>

                                    <div class="lescomms" post-id="<?php echo $kk; ?>">
                                        <div class="formCommentaire">
                                            <form class="commentairesForm" method="post" action="">
                                                <input type="hidden" name="id_article" value="<?php echo $vv['id']; ?>">
                                                <textarea id="commentairesText" class="ckeditor" name="blocCommentaire" placeholder="Commentez cet article"></textarea>
                                                <button class="annuleCommentaire inputComment" name="annuleCommentaire" post-id="<?php echo $kk; ?>">Annuler le commentaire</button>
                                                <input type="submit" value="Valider" class="inputComment" id="validCommentaire">
                                            </form>

                                        </div>
                                    </div>

                                    <div class="afficheCommentaire" post-id="<?php echo $kk; ?>">
                                        <?php
                                        foreach ($commentaires as $kkk => $vvv) {
                                            if ($vvv['id_element'] == $vv['id']) {
                                                ?>
                                                <div class="commentaire">
                                                    <h3> <?php echo $vvv['pseudo'] . " le " . date("d/m/Y", strtotime($vvv['date_comment'])) . " : " ?> </h3>
                                                    <p><?php echo html_entity_decode($vvv['contenu'],ENT_HTML5, 'UTF-8'); ?></p>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>

                                    </div>
                                    <?php
                                }
                                ?>
                            </div><br>
                        <?php } ?>
                        <div class="lineclear"></div>
                    </div>
                </div>
            </section>
            <!--Pagination-->
            <?php if ($pages > 1) { ?>
                <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
                    <?php if ($current_page > 1) { ?>
                        <?php if (!empty($id_redac)) { ?>
                            <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>&id_redacteur=<?php echo $id_redac; ?>"><span class="link prev"></span></a>
                        <?php } else { ?>
                            <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
                        <?php }
                    }
                    ?>

                    <?php if ($current_page < $pages) { ?>
                        <?php if (!empty($id_redac)) { ?>
                            <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>&id_redacteur=<?php echo $id_redac; ?>"><span class="link next"></span></a>
                        <?php } else { ?>
                            <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
        <?php } ?>
                    </div>
                    <div class="lineclear"></div>
    <?php }
} ?>
            <!--Fin pagination-->
        </div>
    </div>
</section>
