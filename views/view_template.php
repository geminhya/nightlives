<!DOCTYPE html>
<?php global $home_url; ?>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Time to feel the vibes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta property="og:image" content="images/common/thumbnail.png" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/style.css" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/font.css" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/artist.css" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/festivals.css" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/festival_soiree.css" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/soirees.css" /><link rel="stylesheet" href="<?php echo $home_url; ?>/styles/concerts.css" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/user.css" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/popin.css" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/articles.css" />
        <link rel="stylesheet" href="<?php echo $home_url; ?>/styles/contribuer.css" />
        <title><?php if (isset($head_title)) {
            echo $head_title;
        } ?>Nightlives</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script type="text/javascript" src="<?php echo $home_url; ?>/scripts/jquery.js"></script>
        <script type="text/javascript" src="<?php echo $home_url; ?>/scripts/on_scroll.js"></script>
        <script type="text/javascript" src="<?php echo $home_url; ?>/scripts/script.js"></script>
        <script type="text/javascript" src="<?php echo $home_url; ?>/scripts/slider.js"></script>
        <script type="text/javascript" src="<?php echo $home_url; ?>/scripts/popin.js"></script>
        <script type="text/javascript" src="<?php echo $home_url; ?>/scripts/ajax.js"></script>
        <script type="text/javascript" src="<?php echo $home_url; ?>/scripts/autocomplete.js"></script>
        <script type="text/javascript" src="<?php echo $home_url; ?>/scripts/comment.js"></script>
        <script type="text/javascript" src="<?php echo $home_url; ?>/scripts/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?php echo $home_url; ?>/ckeditor/ckeditor.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900' rel='stylesheet' type='text/css'>
        <link rel="icon"
              type="image/png"
              href="<?php echo $home_url; ?>/images/common/mini_logo.png">
    </head>
</head>

<body>
    <header class="">
        <div class="content">
            <div class="logo"><a href="<?php echo $home_url; ?>"><img src="<?php echo $home_url; ?>/images/common/logo.png"><p>Time to feel the vibes</p></a></div>
            <nav>
                <ul>
                    <li><a href="<?php echo $home_url; ?>/festivals">Festivals</a>
                        <!--<ul class="sous_menu">
                            <li><a href="<?php echo $home_url; ?>/festivals">Événements à venir</a></li>
                            <li><a href="<?php echo $home_url; ?>/artistes">Voir les artistes</a></li>
                        </ul>-->
                    </li>
                    <li><a href="<?php echo $home_url; ?>/soirees">Soirées</a>
                    <li><a href="<?php echo $home_url; ?>/concerts">Concerts</a>
                    </li>
                    <li><a href="<?php echo $home_url; ?>/artistes">Artistes</a>
                        <ul class="sous_menu">
                            <li><a href="<?php echo $home_url; ?>/artistes?type=electro">Electro</a></li>
                            <li><a href="<?php echo $home_url; ?>/artistes?type=pop">Pop</a></li>
                            <li><a href="<?php echo $home_url; ?>/artistes?type=rock"> Rock</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo $home_url; ?>/articles">Blog</a>
                </ul>
            </nav>
            <div class="account">
<?php if (!empty($_SESSION["user"])) { ?>
                    <?php if(!empty($_SESSION["user"]["photo"])){ ?>
                    <a class="header_profile" href="<?php echo $home_url; ?>/user?pseudo=<?php echo $_SESSION["user"]["pseudo"] ?>"><img src='<?php echo $home_url . "/" . $_SESSION["user"]["photo"] ?>'></a>
                    <?php } ?>
                    <?php if(!empty($_SESSION["user"]["pseudo"])){ ?>
                    <a href="<?php echo $home_url; ?>/user?pseudo=<?php echo $_SESSION["user"]["pseudo"] ?>"><?php echo $_SESSION["user"]["pseudo"]; ?></a>
                    <?php } ?>
                    <span class="separator">|</span>
                    <a href="<?php echo $home_url; ?>/user/deconnexion">Se déconnecter</a>
<?php } else { ?>
                    <a class="callPopin" href="#connexion">Se connecter</a>
                    <span class="separator">|</span>
                    <a class="callPopin" href="#subscribe">Inscription</a>
<?php } ?>
            </div>
            <div class="mob_menu">
                <div class="button"><img src="<?php echo $home_url; ?>/images/common/hamburger.png"></div>
                <ul>
                    <li><a href="<?php echo $home_url; ?>">Accueil</a></li>
                    <li><a href="<?php echo $home_url; ?>/festivals">Festivals</a></li>
                    <li><a href="<?php echo $home_url; ?>/soirees">Soirées</a></li>
                    <li><a href="<?php echo $home_url; ?>/concerts">Concerts</a></li>
                    <li><a href="<?php echo $home_url; ?>/artistes">Artistes</a></li>

                    <?php if (!empty($_SESSION["user"])) { ?>
                    
                    <?php if(!empty($_SESSION["user"]["pseudo"])){ ?>
                    <li class="mob_account"><a href="<?php echo $home_url; ?>/user?pseudo=<?php echo $_SESSION["user"]["pseudo"] ?>">Mon Compte</a></li>
                    <?php } ?>
                    <li class="mob_account"><a href="<?php echo $home_url; ?>/user/deconnexion">Se déconnecter</a></li>
                    <?php } else { ?>
                        <li class="mob_account"><a class="callPopin" href="#connexion">Se connecter</a></li>
                        <li class="mob_account"><a class="callPopin" href="#subscribe">S'inscrire</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </header>
<?php include($this->view); ?>
    <div id="overlay"></div>
    <div class="popin-container" id="subscribtion">
        <div class="popin" id="subscribe">
            <div class="closePopin">
                <img src="<?php echo $home_url; ?>/images/common/close.png" alt="Fermer">
            </div>
            <div id="entetepopin">
                <h2>Devenez un NightLiver :</h2>
                <p>
                    Vous voulez faire partie de la famille des NightLivers?<br>
                    <span>Rejoignez - nous !</span>
                </p>
            </div>
            <div  id="errorsubscribe">
            </div>
            <form id="formsubscribe">
                <input type="text" name="pseudo" placeholder="Pseudo" value="<?php echo (isset($_POST['pseudo'])) ? $_POST['pseudo'] : ""; ?>" required><br>
                <input type="text" name="nom" placeholder="Nom" value="<?php echo (isset($_POST['nom'])) ? $_POST['nom'] : ""; ?>" required><br>
                <input type="text" name="prenom" placeholder="Prenom" value="<?php echo (isset($_POST['prenom'])) ? $_POST['prenom'] : ""; ?>" required><br>
                <input type="text" name="ville" placeholder="Ville" value="<?php echo (isset($_POST['ville'])) ? $_POST['ville'] : ""; ?>" required><br>
                <input type="email" name="email" placeholder="Adresse email" value="<?php echo (isset($_POST['email'])) ? $_POST['email'] : ""; ?>" required><br>
                <input type="password" name="mdp" placeholder="Mot de passe" required><br>
                <input type="password" name="confirmmdp" placeholder="Confirmation mot de passe" required><br>
                <input type="submit" name="inscription" value="Valider" required>
            </form>
        </div>
        <!--<span class="separator">|</span>-->
        <div class="popin" id="connexion">
            <div class="closePopin">
                <img src="<?php echo $home_url; ?>/images/common/close.png" alt="Fermer">

            </div>
            <h2>Connexion</h2>
            <div id="entetepopin">
            </div>
            <div  id="errorconnexion">
            </div>
            <form id="formconnexion">
                <input type="text" id="emailco" name="emailco" placeholder="Email" value="<?php echo (isset($_POST['emailco'])) ? $_POST['emailco'] : ""; ?>" required><br>
                <input type="password" id="pwdco" name="pwdco" placeholder="Mot de passe" value="<?php echo (isset($_POST['pwdco'])) ? $_POST['pwdco'] : ""; ?>" required><br>
                <input type="submit" id="connexion" value="Se connecter !">
            </form>
        </div>
    </div>

    <footer>
        <div id="footer_container">
            <ul>
                <li><a href="<?php echo $home_url; ?>/users">Nightlivers</a></li>
                <li><a href="<?php echo $home_url; ?>/articles">Blog</a></li>
                <!--<li><a href="<?php echo $home_url; ?>/contribuer">Contribuer</a></li>-->
                <li><a href="<?php echo $home_url; ?>/mentionsLegales">Mentions Légales</a></li>
            </ul>
        </div>
        <div class="content">
            <div class="contact">
                <p class="title">Nous contacter</p>
                <p>242 rue du Faubourg<br/>Saint-Antoine, 75012 Paris</p>
                <p class="tel">+ (0)1 56 06 90 41</p>
            </div>
            <div class="social">
                <p class="title">Suivez-nous !</p>
                <ul>
                    <li><a href="https://www.facebook.com/nightlives.fr/"><img src="<?php echo $home_url; ?>/images/social/Facebook.png"></a></li>
                    <li><a href="https://twitter.com/NightLives_fr"><img src="<?php echo $home_url; ?>/images/social/Twitter.png"></a></li>
                    <li><a href="https://www.instagram.com/nightlivesfr/"><img src="<?php echo $home_url; ?>/images/social/Instagram.png"></a></li>
                    <li><a href="https://plus.google.com/106493455521527067911?hl=fr"><img src="<?php echo $home_url; ?>/images/social/Google+.png"></a></li>
                    <li><a target="_blank" href="<?php echo FRONT_URL ?>/fluxrss.php"><img src="<?php echo $home_url; ?>/images/social/RSS.png"></a></li>
                    <!-- <li><a href="#"><img src="<?php //echo $home_url; ?>/images/social/YouTube.png"></a></li>
                    <!-- <li><a href="#"><img src="<?php //echo $home_url; ?>/images/social/Tumblr.png"></a></li>
                    <li><a href="#"><img src="<?php //echo $home_url; ?>/images/social/Mail.png"></a></li>
                    <li><a href="#"><img src="<?php //echo $home_url; ?>/images/social/Soundcloud.png"></a></li> -->
                </ul>
            </div>
            <img class="black_logo" src="<?php echo $home_url; ?>/images/common/logo3.png">
        </div>
    </footer>
    <div class="cookie_container">
        <div id="cookiebar" style="display:none;">
            <div class="message"><p>Pour proposer un contenu en correspondance avec vos centres d’intérêt, ce site utilise des cookies.<br>
    En poursuivant votre navigation, vous acceptez cet usage.</p></div>
            <div class="close">Fermer</div>
        </div>
    </div>

    <script type="text/javascript">


    jQuery(document).ready(function($) {
        console.log(jQuery.cookie('cookiebar'));
        if (jQuery.cookie && jQuery.cookie('cookiebar') != 'true')
        {
            jQuery.cookie('cookiebar', 'true', {expires: 365});
            jQuery("#cookiebar").show();
        }
    });

    jQuery(document).on( 'click', '#cookiebar .close', function() {
        jQuery('#cookiebar').css('display', 'none');
    });

    var isMobile = 0;
    var isTablet = 0;
    </script>
</body>
</html>
