<div class="single_artist single_event">
  <section class="slider_container" style="background-image: url(<?php echo (isset($banner)) ? FRONT_URL . $banner : ''; ?>);">
    <h1 class="nameevent"><?php echo utf8_encode($libelle); ?></h1>
  </section>

  <section class="eventheader">
    <div class="content">
      <div class="bioevent">
          <?php if(isset($photo)){ ?>
          <img src="<?php echo FRONT_URL . $photo; ?>" class="vignetteevent">
          <?php } ?>
          <div class="minibio">
            <p class="titreevent">- <?php echo utf8_encode($libelle);; ?> -</p>
            <p><?php echo utf8_encode($lieu); ?> - <?php echo date("d.m.Y", strtotime($date_deb)); ?> - <?php echo date("d.m.Y", strtotime($date_fin)); ?></p>
            <div class="social">
              <ul>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/Facebook.png"></a></li>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/Twitter.png"></a></li>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/YouTube.png"></a></li>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/Instagram.png"></a></li>
                <li><a href="#"><img src="<?php echo $home_url; ?>/images/social/Soundcloud.png"></a></li>
              </ul>

              <div class="lineclear"></div>
            </div>
        </div>
        <div class="bioevent">
          <?php echo "<p>".nl2br($informations)."</p>"; ?>
        </div>
      </div>
    </div>
  </section>


  <?php if(!empty($artistes_results)) { ?>
  <section class="events black">
    <div class="content">
      <div>
        <h2 class="presenta">Line Up</h2>
      </div>
      <div class="lineup">
        <?php foreach ($artistes_results as $key => $artiste) {  ?>
          <div class="artiste_lineup">
            <a href="<?php echo FRONT_URL; ?>artiste?id=<?php echo $artiste['id']; ?>">
              <img src="<?php echo $home_url . '/' . $artiste['photo']; ?>">
              <p class="pseudo"><?php echo $artiste['pseudo']; ?></p>
            </a>
          </div>
        <?php } ?>
        <div class="lineclear"></div>
      </div>
    </div>
  </section>
  <?php } ?>
<section class="fans">
    <div class="content">
        <?php if(!empty($participants_results)) { ?>
            <h2><span class="nbFans"><?php echo $count; ?></span> membres participent à cet event!</h2>
             <?php foreach ($participants_results as $key => $participant) {  ?>
                <div class="fan">
                    <a href="<?php echo FRONT_URL; ?>user?pseudo=<?php echo $participant['pseudo']; ?>">
                  <img src="<?php echo $home_url . '/' . $participant['photo']; ?>">
                  <p class="pseudo"><?php echo $participant['pseudo']; ?></p>
                </div>
            <?php } ?>
        <?php } else { ?>
            <h2><span class="nbFans">0</span> membres participent à cet event!</h2>
        <?php } ?>
            <div class="lineclear"></div>
            <?php if(!empty($user)) { ?><div id="participate_container"><?php echo $participate_button ?></div><?php } ?>
                    <?php if(empty($user)) { ?> <div class="call_to long"> <a  id="bouton_participe_2"  class="callPopin" href="#connexion">Je participe moi aussi !</a></div> <?php } ?>
    </div>
  </section>
  <section class="commentaires">
    <h2>Commentaires</h2>
      <?php if(!empty($commentaires_results)) { ?>
          <?php foreach ($commentaires_results as $key => $commentaire) {  ?>
        <article>
          <div>
            <img src="<?php echo $home_url . '/' . $commentaire['photo']; ?>">
          </div>
          <div class="blocCommentaire">
              <p class="pseudo"><?php echo $commentaire['pseudo']; ?></p>
            <p class="timeComment">Posté le <?php echo date("d-m-Y", strtotime($commentaire['date_comment'])); ?></p>
              <p class="valid"><?php if($commentaire['valid'] == 1) { ?> Validé <?php } ?></p>
              <p class="non_valid"><?php if($commentaire['valid'] == 0) { ?> En cours de validation <?php } ?></p>
            <p><?php echo utf8_encode ( $commentaire['contenu']); ?> </p>
          </div>
        </article>
      <?php } ?>
    <?php } ?>

    <hr>
    <article>
      <h3>Participez vous aussi au débat!</h3>
      <p>Saisissez ici votre commentaire</p>
        <textarea id="mon_commentaire"></textarea>
        <?php if(!empty($user)) { ?>
        <?php echo $commente_button; } ?>
        <?php if(empty($user)) { ?>
            <div class="call_to long"> <a  id="bouton_commentaire"  class="callPopin" href="#connexion">Envoyer !</a></div>
        <?php } ?>
    </article>

  </section>
</div>
