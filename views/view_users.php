<section class="artists list_all_artists list_all_users" style="background-image: url(<?php echo FRONT_URL . $background_url; ?>);" >
    <h1><?php echo $title; ?></h1>
    <?php if(!empty($subtitle)) { ?>
    <p class="seacrh_subtitle"><?php echo $subtitle; ?></p>
    <?php } ?>
  <div class="content">
    <div class="list_artists" id="list_admins">
      <h2>L'équipe Nightlives</h2>
      <?php if(!empty($admins)) { foreach ($admins as $key => $element) { ?>
        <div class="artist">
        <a href="<?php echo $home_url . '/user?pseudo=' . $element['pseudo']; ?>">
          <div class="intern_artist">
            <div class="logo_artist">
            <img src="<?php echo $element['photo']; ?>">
            <div class="hover">
            </div>
            </div>
            <h3><a href="<?php echo $home_url . '/user?pseudo=' . $element['pseudo']; ?>"><?php echo utf8_encode($element['pseudo']); ?></a></h3>
          </div>
          </a>
        </div>
      <?php } } ?>
      <div class="lineclear"></div>
    </div>

    <div class="list_artists" id="list_users">
      <h2>Les Nightlivers</h2>
      <?php if(!empty($elements)) { foreach ($elements as $key => $element) { ?>
        <div class="artist">
        <a href="<?php echo $home_url . '/user?pseudo=' . $element['pseudo']; ?>">
          <div class="intern_artist">
            <div class="logo_artist">
            <img src="<?php echo $element['photo']; ?>">
            </div>
            <h3><a href="<?php echo $home_url . '/user?pseudo=' . $element['pseudo']; ?>"><?php echo utf8_encode($element['pseudo']); ?></a></h3>
          </div>
        </a>
        </div>
      <?php } } ?>
      <div class="lineclear"></div>
    </div>
  </div>
</section>
