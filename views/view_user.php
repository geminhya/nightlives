<div class="single_user_account">
    <section class="user_dashboard">
        <div class="content">
            <div class="user_menu">
                <div class="infos">
                    <div class="img">
                      <!-- <div class="change_picture"><img src="<?php echo $home_url; ?>/images/membres/camera_icon.png"></div> -->
                      <img src="<?php echo $home_url."/".$profile_pic; ?>">
                    </div>
                    <h1><?php echo $user[0]['pseudo'];?></h1>
                    <hr>
                    <p class="mini_bio"><?php echo $mini_bio ?></p>

                </div>
                <ul class="listing">
                    <li><a href="#informations" class="nice_scroll">Informations</a></li>
                    <li><a href="#next_events" class="nice_scroll">Prochains Évènements</a></li>
                    <li><a href="#last_events" class="nice_scroll">Dernières Participations</a></li>
                    <li><a href="#favorites" class="nice_scroll">Favoris</a></li>
                    <li><a href="#added" class="nice_scroll">Mes contributions</a></li>
                </ul>
            </div>
            <div class="user_data" id="informations">
                <h2>Mes informations</h2>
                <p><?php echo $user[0]['nom'] . " " . $user[0]['prenom']; ?></p>
                <p class="location"><?php echo $user[0]['ville']; ?></p>
                <p>Inscrit(e) depuis le <?php echo date("d/m/Y", strtotime($user[0]['date_inscription'])); ?></p>
                <br>
                <?php echo $personnalDashboard ?>
            </div>
            <div class="user_data" id="next_events">
                <h2>Mes prochains évènements</h2>
                <div class="list_events">
                    <?php foreach($participating as $event){
                      $nextevent = '<div class="event">';
                      $nextevent .= '<div class="logo_event"><img src="'.$home_url.'/'.$event["photo"].'"></div>';
                      $nextevent .= '<img class="top" src="'.$home_url.'/'.$event["banniere"].'">';
                      $nextevent .= '<h3>'.$event["libelle_event"].'</h3><p>Du '.$event["date_deb"].' au '.$event["date_fin"].'</p>';
                      $nextevent .= '<a class="call_to" href="'.$home_url.'/event?id='.$event["id"].'">Let\'s Party</a>';
                      $nextevent .= '</div>';
                      echo $nextevent;
                    }

                    if ($participating == NULL){
                      echo '<p>Je ne participe à aucun évènement... Pour le moment!</p>';
                    }
                  ?>
                    <div class="lineclear"></div>
                </div>
            </div>
            <div class="user_data" id="last_events">
                <h2>Mes dernières participations</h2>
                <div class="list_events">
                    <?php foreach($participated as $wasevent){
                      $lastevent = '<div class="event">';
                      $lastevent .= '<div class="logo_event"><img src="'.$home_url.'/'.$wasevent["photo"].'"></div>';
                      $lastevent .= '<img class="top" src="'.$home_url.'/'.$wasevent["banniere"].'">';
                      $lastevent .= '<h3>'.utf8_encode($wasevent["libelle_event"]).'</h3><p>Du '.$wasevent["date_deb"].' au '.$wasevent["date_fin"].'</p>';
                      $lastevent .= '<a class="call_to" href="'.$home_url.'/event?id='.$wasevent["id"].'">Let\'s Party</a>';
                      $lastevent .= '</div>';
                      echo $lastevent;
                    }

                    if ($participated == NULL){
                      echo '<p>Je n\'ai participé à aucun évènement</p>';
                    }
                    ?>
                    <div class="lineclear"></div>
                </div>
            </div>
            <div class="user_data" id="favorites">
                <h2>Mes artistes favoris</h2>
                <div class="list_artists">
                  <?php foreach ($fan as $artist){
                    $fanOf = '<div class="artist">';
                    $fanOf .= '<div class="logo_artist"><a href="'.$home_url.'/artiste?id='.$artist['id'].'"><img src="'.$home_url.'/'.$artist['photo'].'"></a></div>';
                    $fanOf .= '<h3><a href="'.$home_url.'/artiste?id='.$artist['id'].'">'.$artist['pseudo'].'</a></h3>';
                    $fanOf .= '<a class="call_to" href="'.$home_url.'/artiste?id='.$artist['id'].'">Événements à venir</a>';
                    $fanOf .= '</div>';
                    echo $fanOf;
                  }

                    if ($fan == NULL){
                      echo '<p>Je ne suis fan d\'aucun artiste... Pour le moment!</p>';
                    }
                  ?>
                    <div class="lineclear"></div>
                </div>
            </div>
            <div class="user_data" id="added">
              <h2>Mes dernières contributions</h2>
              <p>La création de contenu n'est pas encore disponible pour le moment!</p>
            </div>
            <div class="lineclear"></div>
        </div>

    </section>
</div>