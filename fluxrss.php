<?php

$xml_file = new DOMDocument("1.0");

function &init_news_rss(&$xml_file) {
    $root = $xml_file->createElement("rss"); // création de l'élément
    $root->setAttribute("version", "2.0"); // on lui ajoute un attribut
    $root = $xml_file->appendChild($root); // on l'insère dans le nœud parent

    $channel = $xml_file->createElement("channel");
    $channel = $root->appendChild($channel);
    return $channel;
}

function add_news_node(&$parent, $root, $id, $pseudo, $titre, $contenu, $date) {
    $item = $parent->createElement("item");
    $item = $root->appendChild($item);

    $title = $parent->createElement("title");
    $title = $item->appendChild($title);
    $text_title = $parent->createTextNode($titre);
    $text_title = $title->appendChild($text_title);

    $link = $parent->createElement("link");
    $link = $item->appendChild($link);
    $text_link = $parent->createTextNode("www.nightlives.fr/nightlives/articles");
    $text_link = $link->appendChild($text_link);

    $desc = $parent->createElement("description");
    $desc = $item->appendChild($desc);
    $text_desc = $parent->createTextNode($contenu);
    $text_desc = $desc->appendChild($text_desc);


    $author = $parent->createElement("author");
    $author = $item->appendChild($author);
    $text_author = $parent->createTextNode($pseudo);
    $text_author = $author->appendChild($text_author);

    $pubdate = $parent->createElement("pubDate");
    $pubdate = $item->appendChild($pubdate);
    $text_date = $parent->createTextNode($date);
    $text_date = $pubdate->appendChild($text_date);


    $src = $parent->createElement("source");
    $src = $item->appendChild($src);
    $text_src = $parent->createTextNode("www.nightlives.fr/nightlives");
    $text_src = $src->appendChild($text_src);
}

function rebuild_rss() {
    // on se connecte à la BDD
    $servername = "nightlivwjnlbd.mysql.db";
    $username = "nightlivwjnlbd";
    $password = "nlBD2016";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=nightlivwjnlbd", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "Connected successfully";
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }

    // on récupère les news
    $nws = "SELECT id, id_redacteur, titre_article, contenu, photo, date_parution, date_modification FROM article";
    $query = $conn->query($nws);
    $row = $query->fetchAll(PDO::FETCH_ASSOC);
    // on crée le fichier XML
    $xml_file = new DOMDocument("1.0");

    // on initialise le fichier XML pour le flux RSS
    $channel = init_news_rss($xml_file);
    // on ajoute chaque news au fichier RSS
    foreach ($row as $kk => $vv) {
        add_news_node($xml_file, $channel, $vv["id"], $vv["id_redacteur"], $vv["titre_article"], $vv["contenu"], $vv["date_parution"]);
    }
    if ($xml_file->save("news_FR_flux.xml")) {
        echo "Génération réussie...";
        header("Location: " . "news_FR_flux.xml");
    } else {
        echo "Génération échouée.";
    }
}

rebuild_rss();


