<?php

set_pdo();
init_datas();

function set_pdo() {
    global $pdo;
    $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
    $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
}

function email_exists($email) {
    global $pdo;
    $sql = "SELECT id FROM user WHERE email='$email';";
    $query = $pdo->query($sql);
    if ($query) {
        $row = $query->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $row["id"];
        }
    }
    return false;
}

function artist_exists($easy_name) {
    global $pdo;
    $sql = "SELECT id FROM artiste WHERE easy_name='$easy_name';";
    $query = $pdo->query($sql);
    if ($query) {
        $row = $query->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $row["id"];
        }
    }
    return false;
}

function get_artist_by_id($id){
    global $pdo;
    $sql = "SELECT id, pseudo FROM artiste WHERE id='$id';";
    $query = $pdo->query($sql);
    if($query){
        $row = $query->fetch(PDO::FETCH_ASSOC);
        if($row){
            return $row;
        }
    }
    return false;
}
function event_exists($easy_name){
    global $pdo;
    $sql = "SELECT id FROM evenement WHERE easy_name='$easy_name';";
    $query = $pdo->query($sql);
    if ($query) {
        $row = $query->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $row["id"];
        }
    }
    return false;
}

function save_page_count($url) {
    global $pdo;
    $sql = "SELECT id FROM page WHERE url='$url';";
    $query = $pdo->query($sql);
    if ($query) {
        $row = $query->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            $id = $row["id"];
            $sql = "UPDATE page SET count=count+1 WHERE id=$id;";
            $query = $pdo->prepare($sql);
            $query->execute();
        } else {
            $sql = "INSERT INTO page (url, count) VALUES ('$url', 1)";
            $query = $pdo->prepare($sql);
            $query->execute();
        }
    }
}

function easy_name($string) {
    $str1 = str_replace(" ", "-", no_accents(strtolower($string)));
    return str_replace("'", "", $str1);
}

function no_accents($string) {
    $unwanted_array = array('Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
        'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U',
        'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c',
        'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
        'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y');
    return strtr($string, $unwanted_array);
}


function is_logged_in() {
    return !empty($_SESSION['user']);
}

function getUser($email) {
    global $pdo;
    $query = $pdo->prepare("SELECT u.*, m.lien as photo, m.libelle as alt FROM user u, media m WHERE u.id_media = m.id AND email = :email;");
    $query->execute(array('email' => $email));
    $row = $query->fetch();

    return $row;
}

function getArticles($start, $nb_per_page) {
    global $pdo;
    $query = $pdo->prepare("SELECT DISTINCT(u.id), u.nom, u.prenom, u.email, a.id, a.id_redacteur, a.titre_article, a.contenu, a.photo, a.date_parution "
            . "FROM user u, article a WHERE u.id = a.id_redacteur AND a.archive = 0 ORDER BY date_parution DESC "
            . "LIMIT ".$start.",".$nb_per_page.";");
    $query->execute();
    $row = $query->fetchAll();
    return $row;
}

function getCommentaires() {
    global $pdo;
    $query = $pdo->prepare("SELECT u.id, u.nom, u.prenom, c.id_article, c.contenu, c.date_ecriture"
            . " FROM user u JOIN article a ON (a.id_redacteur = u.id) JOIN commentaire c ON (c.id_article = a.id)");
    $query->execute();
    $row = $query->fetchAll();
    return $row;
}

function init_datas() {
    global $t_departements;
    $t_departements = array();

    $t_departements['01'] = 'Ain';
    $t_departements['02'] = 'Aisne';
    $t_departements['03'] = 'Allier';
    $t_departements['04'] = 'Alpes-de-Haute-Provence';
    $t_departements['05'] = 'Hautes-Alpes';
    $t_departements['06'] = 'Alpes-Maritimes';
    $t_departements['07'] = 'Ardèche';
    $t_departements['08'] = 'Ardennes';
    $t_departements['09'] = 'Ariège';
    $t_departements['10'] = 'Aube';
    $t_departements['11'] = 'Aude';
    $t_departements['12'] = 'Aveyron';
    $t_departements['13'] = 'Bouches-du-Rhône';
    $t_departements['14'] = 'Calvados';
    $t_departements['15'] = 'Cantal';
    $t_departements['16'] = 'Charente';
    $t_departements['17'] = 'Charente-Maritime';
    $t_departements['18'] = 'Cher';
    $t_departements['19'] = 'Corrèze';

    $t_departements['21'] = 'Côte-d\'Or';
    $t_departements['22'] = 'Côtes-d\'Armor';
    $t_departements['23'] = 'Creuse';
    $t_departements['24'] = 'Dordogne';
    $t_departements['25'] = 'Doubs';
    $t_departements['26'] = 'Drôme';
    $t_departements['27'] = 'Eure';
    $t_departements['28'] = 'Eure-et-Loir';
    $t_departements['29'] = 'Finistère';
    $t_departements['30'] = 'Gard';
    $t_departements['31'] = 'Haute-Garonne';
    $t_departements['32'] = 'Gers';
    $t_departements['33'] = 'Gironde';
    $t_departements['34'] = 'Hérault';
    $t_departements['35'] = 'Ille-et-Vilaine';
    $t_departements['36'] = 'Indre';
    $t_departements['37'] = 'Indre-et-Loire';
    $t_departements['38'] = 'Isère';
    $t_departements['39'] = 'Jura';
    $t_departements['40'] = 'Landes';
    $t_departements['41'] = 'Loir-et-Cher';
    $t_departements['42'] = 'Loire';
    $t_departements['43'] = 'Haute-Loire';
    $t_departements['44'] = 'Loire-Atlantique';
    $t_departements['45'] = 'Loiret';
    $t_departements['46'] = 'Lot';
    $t_departements['47'] = 'Lot-et-Garonne';
    $t_departements['48'] = 'Lozère';
    $t_departements['49'] = 'Maine-et-Loire';
    $t_departements['50'] = 'Manche';
    $t_departements['51'] = 'Marne';
    $t_departements['52'] = 'Haute-Marne';
    $t_departements['53'] = 'Mayenne';
    $t_departements['54'] = 'Meurthe-et-Moselle';
    $t_departements['55'] = 'Meuse';
    $t_departements['56'] = 'Morbihan';
    $t_departements['57'] = 'Moselle';
    $t_departements['58'] = 'Nièvre';
    $t_departements['59'] = 'Nord';
    $t_departements['60'] = 'Oise';
    $t_departements['61'] = 'Orne';
    $t_departements['62'] = 'Pas-de-Calais';
    $t_departements['63'] = 'Puy-de-Dôme';
    $t_departements['64'] = 'Pyrénées-Atlantiques';
    $t_departements['65'] = 'Hautes-Pyrénées';
    $t_departements['66'] = 'Pyrénées-Orientales';
    $t_departements['67'] = 'Bas-Rhin';
    $t_departements['68'] = 'Haut-Rhin';
    $t_departements['69'] = 'Rhône';
    $t_departements['70'] = 'Haute-Saône';
    $t_departements['71'] = 'Saône-et-Loire';
    $t_departements['72'] = 'Sarthe';
    $t_departements['73'] = 'Savoie';
    $t_departements['74'] = 'Haute-Savoie';
    $t_departements['75'] = 'Paris';
    $t_departements['76'] = 'Seine-Maritime';
    $t_departements['77'] = 'Seine-et-Marne';
    $t_departements['78'] = 'Yvelines';
    $t_departements['79'] = 'Deux-Sèvres';
    $t_departements['80'] = 'Somme';
    $t_departements['81'] = 'Tarn';
    $t_departements['82'] = 'Tarn-et-Garonne';
    $t_departements['83'] = 'Var';
    $t_departements['84'] = 'Vaucluse';
    $t_departements['85'] = 'Vendée';
    $t_departements['86'] = 'Vienne';
    $t_departements['87'] = 'Haute-Vienne';
    $t_departements['88'] = 'Vosges';
    $t_departements['89'] = 'Yonne';
    $t_departements['90'] = 'Territoire de Belfort';
    $t_departements['91'] = 'Essonne';
    $t_departements['92'] = 'Hauts-de-Seine';
    $t_departements['93'] = 'Seine-Saint-Denis';
    $t_departements['94'] = 'Val-de-Marne';
    $t_departements['95'] = 'Val-d\'Oise';
    $t_departements['971'] = 'Guadeloupe';
    $t_departements['972'] = 'Martinique';
    $t_departements['973'] = 'Guyane';
    $t_departements['974'] = 'La Réunion';
    $t_departements['2A'] = 'Corse-du-Sud';
    $t_departements['2B'] = 'Haute-Corse';
}

function get_pays() {
    $countryCode = array(
        array('250', 'FRA', 'FR', 'France'),
        array('4', 'AFG', 'AF', 'Afghanistan'),
        array('710', 'ZAF', 'ZA', 'Afrique du Sud'),
        array('248', 'ALA', 'AX', 'Aland'),
        array('8', 'ALB', 'AL', 'Albanie'),
        array('12', 'DZA', 'DZ', 'Algérie'),
        array('276', 'DEU', 'DE', 'Allemagne'),
        array('20', 'AND', 'AD', 'Andorre'),
        array('24', 'AGO', 'AO', 'Angola'),
        array('660', 'AIA', 'AI', 'Anguilla'),
        array('10', 'ATA', 'AQ', 'Antarctique'),
        array('28', 'ATG', 'AG', 'Antigua-et-Barbuda'),
        array('682', 'SAU', 'SA', 'Arabie saoudite'),
        array('32', 'ARG', 'AR', 'Argentine'),
        array('51', 'ARM', 'AM', 'Arménie'),
        array('533', 'ABW', 'AW', 'Aruba'),
        array('36', 'AUS', 'AU', 'Australie'),
        array('40', 'AUT', 'AT', 'Autriche'),
        array('31', 'AZE', 'AZ', 'Azerbaïdjan'),
        array('44', 'BHS', 'BS', 'Bahamas'),
        array('48', 'BHR', 'BH', 'Bahreïn'),
        array('50', 'BGD', 'BD', 'Bangladesh'),
        array('52', 'BRB', 'BB', 'Barbade'),
        array('112', 'BLR', 'BY', 'Biélorussie'),
        array('56', 'BEL', 'BE', 'Belgique'),
        array('84', 'BLZ', 'BZ', 'Belize'),
        array('204', 'BEN', 'BJ', 'Bénin'),
        array('60', 'BMU', 'BM', 'Bermudes'),
        array('64', 'BTN', 'BT', 'Bhoutan'),
        array('68', 'BOL', 'BO', 'Bolivie'),
        array('535', 'BES', 'BQ', 'Bonaire', ' Saint-Eustache et Saba'),
        array('70', 'BIH', 'BA', 'Bosnie-Herzégovine'),
        array('72', 'BWA', 'BW', 'Botswana'),
        array('74', 'BVT', 'BV', 'Île Bouvet'),
        array('76', 'BRA', 'BR', 'Brésil'),
        array('96', 'BRN', 'BN', 'Brunei'),
        array('100', 'BGR', 'BG', 'Bulgarie'),
        array('854', 'BFA', 'BF', 'Burkina Faso'),
        array('108', 'BDI', 'BI', 'Burundi'),
        array('136', 'CYM', 'KY', 'Îles Caïmans'),
        array('116', 'KHM', 'KH', 'Cambodge'),
        array('120', 'CMR', 'CM', 'Cameroun'),
        array('124', 'CAN', 'CA', 'Canada'),
        array('132', 'CPV', 'CV', 'Cap-Vert'),
        array('140', 'CAF', 'CF', 'République centrafricaine'),
        array('152', 'CHL', 'CL', 'Chili'),
        array('156', 'CHN', 'CN', 'Chine'),
        array('162', 'CXR', 'CX', 'Île Christmas'),
        array('196', 'CYP', 'CY', 'Chypre'),
        array('166', 'CCK', 'CC', 'Îles Cocos'),
        array('170', 'COL', 'CO', 'Colombie'),
        array('174', 'COM', 'KM', 'Comores'),
        array('178', 'COG', 'CG', 'République du Congo'),
        array('180', 'COD', 'CD', 'République démocratique du Congo'),
        array('184', 'COK', 'CK', 'Îles Cook'),
        array('410', 'KOR', 'KR', 'Corée du Sud'),
        array('408', 'PRK', 'KP', 'Corée du Nord'),
        array('188', 'CRI', 'CR', 'Costa Rica'),
        array('384', 'CIV', 'CI', 'Côte d\'Ivoire'),
        array('191', 'HRV', 'HR', 'Croatie'),
        array('192', 'CUB', 'CU', 'Cuba'),
        array('531', 'CUW', 'CW', 'Curaçao'),
        array('208', 'DNK', 'DK', 'Danemark'),
        array('262', 'DJI', 'DJ', 'Djibouti'),
        array('214', 'DOM', 'DO', 'République dominicaine'),
        array('212', 'DMA', 'DM', 'Dominique'),
        array('818', 'EGY', 'EG', 'Égypte'),
        array('222', 'SLV', 'SV', 'Salvador'),
        array('784', 'ARE', 'AE', 'Émirats arabes unis'),
        array('218', 'ECU', 'EC', 'Équateur'),
        array('232', 'ERI', 'ER', 'Érythrée'),
        array('724', 'ESP', 'ES', 'Espagne'),
        array('233', 'EST', 'EE', 'Estonie'),
        array('840', 'USA', 'US', 'États-Unis'),
        array('231', 'ETH', 'ET', 'Éthiopie'),
        array('238', 'FLK', 'FK', 'Îles Malouines'),
        array('234', 'FRO', 'FO', 'Îles Féroé'),
        array('242', 'FJI', 'FJ', 'Fidji'),
        array('246', 'FIN', 'FI', 'Finlande'),
        array('266', 'GAB', 'GA', 'Gabon'),
        array('270', 'GMB', 'GM', 'Gambie'),
        array('268', 'GEO', 'GE', 'Géorgie'),
        array('239', 'SGS', 'GS', 'Géorgie du Sud-et-les Îles Sandwich du Sud'),
        array('288', 'GHA', 'GH', 'Ghana'),
        array('292', 'GIB', 'GI', 'Gibraltar'),
        array('300', 'GRC', 'GR', 'Grèce'),
        array('308', 'GRD', 'GD', 'Grenade'),
        array('304', 'GRL', 'GL', 'Groenland'),
        array('312', 'GLP', 'GP', 'Guadeloupe'),
        array('316', 'GUM', 'GU', 'Guam'),
        array('320', 'GTM', 'GT', 'Guatemala'),
        array('831', 'GGY', 'GG', 'Guernesey'),
        array('324', 'GIN', 'GN', 'Guinée'),
        array('624', 'GNB', 'GW', 'Guinée-Bissau'),
        array('226', 'GNQ', 'GQ', 'Guinée équatoriale'),
        array('328', 'GUY', 'GY', 'Guyana'),
        array('254', 'GUF', 'GF', 'Guyane'),
        array('332', 'HTI', 'HT', 'Haïti'),
        array('334', 'HMD', 'HM', 'Îles Heard-et-MacDonald'),
        array('340', 'HND', 'HN', 'Honduras'),
        array('344', 'HKG', 'HK', 'Hong Kong'),
        array('348', 'HUN', 'HU', 'Hongrie'),
        array('833', 'IMN', 'IM', 'Île de Man'),
        array('581', 'UMI', 'UM', 'Îles mineures éloignées des États-Unis'),
        array('92', 'VGB', 'VG', 'Îles Vierges britanniques'),
        array('850', 'VIR', 'VI', 'Îles Vierges des États-Unis'),
        array('356', 'IND', 'IN', 'Inde'),
        array('360', 'IDN', 'ID', 'Indonésie'),
        array('364', 'IRN', 'IR', 'Iran'),
        array('368', 'IRQ', 'IQ', 'Irak'),
        array('372', 'IRL', 'IE', 'Irlande'),
        array('352', 'ISL', 'IS', 'Islande'),
        array('376', 'ISR', 'IL', 'Israël'),
        array('380', 'ITA', 'IT', 'Italie'),
        array('388', 'JAM', 'JM', 'Jamaïque'),
        array('392', 'JPN', 'JP', 'Japon'),
        array('832', 'JEY', 'JE', 'Jersey'),
        array('400', 'JOR', 'JO', 'Jordanie'),
        array('398', 'KAZ', 'KZ', 'Kazakhstan'),
        array('404', 'KEN', 'KE', 'Kenya'),
        array('417', 'KGZ', 'KG', 'Kirghizistan'),
        array('296', 'KIR', 'KI', 'Kiribati'),
        array('414', 'KWT', 'KW', 'Koweït'),
        array('418', 'LAO', 'LA', 'Laos'),
        array('426', 'LSO', 'LS', 'Lesotho'),
        array('428', 'LVA', 'LV', 'Lettonie'),
        array('422', 'LBN', 'LB', 'Liban'),
        array('430', 'LBR', 'LR', 'Liberia'),
        array('434', 'LBY', 'LY', 'Libye'),
        array('438', 'LIE', 'LI', 'Liechtenstein'),
        array('440', 'LTU', 'LT', 'Lituanie'),
        array('442', 'LUX', 'LU', 'Luxembourg'),
        array('446', 'MAC', 'MO', 'Macao'),
        array('807', 'MKD', 'MK', 'Macédoine'),
        array('450', 'MDG', 'MG', 'Madagascar'),
        array('458', 'MYS', 'MY', 'Malaisie'),
        array('454', 'MWI', 'MW', 'Malawi'),
        array('462', 'MDV', 'MV', 'Maldives'),
        array('466', 'MLI', 'ML', 'Mali'),
        array('470', 'MLT', 'MT', 'Malte'),
        array('580', 'MNP', 'MP', 'Îles Mariannes du Nord'),
        array('504', 'MAR', 'MA', 'Maroc'),
        array('584', 'MHL', 'MH', 'Marshall'),
        array('474', 'MTQ', 'MQ', 'Martinique'),
        array('480', 'MUS', 'MU', 'Maurice'),
        array('478', 'MRT', 'MR', 'Mauritanie'),
        array('175', 'MYT', 'YT', 'Mayotte'),
        array('484', 'MEX', 'MX', 'Mexique'),
        array('583', 'FSM', 'FM', 'Micronésie'),
        array('498', 'MDA', 'MD', 'Moldavie'),
        array('492', 'MCO', 'MC', 'Monaco'),
        array('496', 'MNG', 'MN', 'Mongolie'),
        array('499', 'MNE', 'ME', 'Monténégro'),
        array('500', 'MSR', 'MS', 'Montserrat'),
        array('508', 'MOZ', 'MZ', 'Mozambique'),
        array('104', 'MMR', 'MM', 'Birmanie'),
        array('516', 'NAM', 'NA', 'Namibie'),
        array('520', 'NRU', 'NR', 'Nauru'),
        array('524', 'NPL', 'NP', 'Népal'),
        array('558', 'NIC', 'NI', 'Nicaragua'),
        array('562', 'NER', 'NE', 'Niger'),
        array('566', 'NGA', 'NG', 'Nigeria'),
        array('570', 'NIU', 'NU', 'Niue'),
        array('574', 'NFK', 'NF', 'Île Norfolk'),
        array('578', 'NOR', 'NO', 'Norvège'),
        array('540', 'NCL', 'NC', 'Nouvelle-Calédonie'),
        array('554', 'NZL', 'NZ', 'Nouvelle-Zélande'),
        array('86', 'IOT', 'IO', 'Territoire britannique de l\'océan Indien'),
        array('512', 'OMN', 'OM', 'Oman'),
        array('800', 'UGA', 'UG', 'Ouganda'),
        array('860', 'UZB', 'UZ', 'Ouzbékistan'),
        array('586', 'PAK', 'PK', 'Pakistan'),
        array('585', 'PLW', 'PW', 'Palaos'),
        array('275', 'PSE', 'PS', 'Autorité Palestinienne'),
        array('591', 'PAN', 'PA', 'Panama'),
        array('598', 'PNG', 'PG', 'Papouasie-Nouvelle-Guinée'),
        array('600', 'PRY', 'PY', 'Paraguay'),
        array('528', 'NLD', 'NL', 'Pays-Bas'),
        array('604', 'PER', 'PE', 'Pérou'),
        array('608', 'PHL', 'PH', 'Philippines'),
        array('612', 'PCN', 'PN', 'Îles Pitcairn'),
        array('616', 'POL', 'PL', 'Pologne'),
        array('258', 'PYF', 'PF', 'Polynésie française'),
        array('630', 'PRI', 'PR', 'Porto Rico'),
        array('620', 'PRT', 'PT', 'Portugal'),
        array('634', 'QAT', 'QA', 'Qatar'),
        array('638', 'REU', 'RE', 'La Réunion'),
        array('642', 'ROU', 'RO', 'Roumanie'),
        array('826', 'GBR', 'GB', 'Royaume-Uni'),
        array('643', 'RUS', 'RU', 'Russie'),
        array('646', 'RWA', 'RW', 'Rwanda'),
        array('732', 'ESH', 'EH', 'Sahara occidental'),
        array('652', 'BLM', 'BL', 'Saint-Barthélemy'),
        array('659', 'KNA', 'KN', 'Saint-Christophe-et-Niévès'),
        array('674', 'SMR', 'SM', 'Saint-Marin'),
        array('663', 'MAF', 'MF', 'Saint-Martin (Antilles françaises)'),
        array('534', 'SXM', 'SX', 'Saint-Martin'),
        array('666', 'SPM', 'PM', 'Saint-Pierre-et-Miquelon'),
        array('336', 'VAT', 'VA', 'Saint-Siège (État de la Cité du Vatican)'),
        array('670', 'VCT', 'VC', 'Saint-Vincent-et-les-Grenadines'),
        array('654', 'SHN', 'SH', 'Sainte-Hélène', ' Ascension et Tristan da Cunha'),
        array('662', 'LCA', 'LC', 'Sainte-Lucie'),
        array('90', 'SLB', 'SB', 'Salomon'),
        array('882', 'WSM', 'WS', 'Samoa'),
        array('16', 'ASM', 'AS', 'Samoa américaines'),
        array('678', 'STP', 'ST', 'Sao Tomé-et-Principe'),
        array('686', 'SEN', 'SN', 'Sénégal'),
        array('688', 'SRB', 'RS', 'Serbie'),
        array('690', 'SYC', 'SC', 'Seychelles'),
        array('694', 'SLE', 'SL', 'Sierra Leone'),
        array('702', 'SGP', 'SG', 'Singapour'),
        array('703', 'SVK', 'SK', 'Slovaquie'),
        array('705', 'SVN', 'SI', 'Slovénie'),
        array('706', 'SOM', 'SO', 'Somalie'),
        array('729', 'SDN', 'SD', 'Soudan'),
        array('728', 'SSD', 'SS', 'Soudan du Sud'),
        array('144', 'LKA', 'LK', 'Sri Lanka'),
        array('752', 'SWE', 'SE', 'Suède'),
        array('756', 'CHE', 'CH', 'Suisse'),
        array('740', 'SUR', 'SR', 'Suriname'),
        array('744', 'SJM', 'SJ', 'Svalbard et Île Jan Mayen'),
        array('748', 'SWZ', 'SZ', 'Swaziland'),
        array('760', 'SYR', 'SY', 'Syrie'),
        array('762', 'TJK', 'TJ', 'Tadjikistan'),
        array('158', 'TWN', 'TW', 'Taïwan / (République de Chine (Taïwan))'),
        array('834', 'TZA', 'TZ', 'Tanzanie'),
        array('148', 'TCD', 'TD', 'Tchad'),
        array('203', 'CZE', 'CZ', 'République tchèque'),
        array('260', 'ATF', 'TF', 'Terres australes et antarctiques françaises'),
        array('764', 'THA', 'TH', 'Thaïlande'),
        array('626', 'TLS', 'TL', 'Timor oriental'),
        array('768', 'TGO', 'TG', 'Togo'),
        array('772', 'TKL', 'TK', 'Tokelau'),
        array('776', 'TON', 'TO', 'Tonga'),
        array('780', 'TTO', 'TT', 'Trinité-et-Tobago'),
        array('788', 'TUN', 'TN', 'Tunisie'),
        array('795', 'TKM', 'TM', 'Turkménistan'),
        array('796', 'TCA', 'TC', 'Îles Turques-et-Caïques'),
        array('792', 'TUR', 'TR', 'Turquie'),
        array('798', 'TUV', 'TV', 'Tuvalu'),
        array('804', 'UKR', 'UA', 'Ukraine'),
        array('858', 'URY', 'UY', 'Uruguay'),
        array('548', 'VUT', 'VU', 'Vanuatu'),
        array('862', 'VEN', 'VE', 'Venezuela'),
        array('704', 'VNM', 'VN', 'Viêt Nam'),
        array('876', 'WLF', 'WF', 'Wallis-et-Futuna'),
        array('887', 'YEM', 'YE', 'Yémen'),
        array('894', 'ZMB', 'ZM', 'Zambie'),
        array('716', 'ZWE', 'ZW', 'Zimbabwe')
    );
    return $countryCode;

}
