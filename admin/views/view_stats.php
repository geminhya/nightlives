<div class="content">
  <form class="form_search" name="artist_search" method="post" action="">
    <input type="text" value="" placeholder="Rechercher un artiste" />
    <input type="submit" value="" />
  </form>
  <h1>Statistiques</h1>
  <div class="push_dahboard">
  <div class="lineclear"></div>
  <?php if(!empty($stats["pages"])){ ?>
    <h2>Les pages les plus visitées</h2>
    <div class="stats multicolor">
        <ul>
        <?php foreach ($stats["pages"] as $key => $page) { ?>
          <li><?php echo (!empty($page["nom"])) ? utf8_encode($page["nom"]) : "Page inconnue"; ?> <span class="percent <?php echo $page['class']; ?>"><?php echo $page['percent']; ?> %&nbsp;</span></li>
        <?php } ?>
        </ul>
    </div>
  <?php } ?>
  <?php if(!empty($stats["events"])){ ?>
    <h2>Les Événements les plus visités</h2>
    <div class="stats multicolor">
        <ul>
        <?php foreach ($stats["events"] as $key => $page) { ?>
          <li><?php echo (!empty($page["nom"])) ? utf8_encode($page["nom"]) : "Page inconnue"; ?> <span class="percent <?php echo $page['class']; ?>"><?php echo $page['percent']; ?> %&nbsp;</span></li>
        <?php } ?>
        </ul>
    </div>
  <?php } ?>
  <?php if(!empty($stats["events"])){ ?>
    <h2>Les artistes tendances</h2>
    <div class="stats multicolor">
        <ul>
        <?php foreach ($stats["artist"] as $key => $page) { ?>
          <li><?php echo (!empty($page["nom"])) ? utf8_encode($page["nom"]) : "Page inconnue"; ?> <span class="percent <?php echo $page['class']; ?>"><?php echo $page['percent']; ?> %&nbsp;</span></li>
        <?php } ?>
        </ul>
    </div>
  <?php } ?>
  <div class="lineclear"></div>
  </div>
</div>