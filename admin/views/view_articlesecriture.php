<script type="text/javascript" src="<?php echo $home_url; ?>/ckeditor/ckeditor.js"></script>
<div class="content">
    <h1>
        Ecrivez votre article
    </h1>
    <form id="articles_creation" method="post" action="" enctype="multipart/form-data">
        <input type="text" name="titre_article" placeholder="Entrez le titre de l'article"><br>
        <textarea name="article_ecrit" class="ckeditor" placeholder="Ecrivez votre article"></textarea>
        <br>
        <input type="file" name="photo" value=""><br>
        <input type="submit" value="Enregistrer">
    </form>
</div>
