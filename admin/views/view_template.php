<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Time to feel the vibes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta property="og:image" content="images/common/thumbnail.png" />
        <link rel="stylesheet" href="<?php echo ADMIN_URL; ?>/styles/style.css" />
        <link rel="stylesheet" href="<?php echo ADMIN_URL; ?>/styles/stats.css" />
        <link rel="stylesheet" href="<?php echo ADMIN_URL; ?>/styles/articles.css" />
        <title>Nightlives Administration</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
        <script type="text/javascript" src="scripts/jquery.js"></script>
        <script type="text/javascript" src="scripts/script.js"></script>
        <script type="text/javascript" src="scripts/autocomplete.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900' rel='stylesheet' type='text/css'>
</head>

<body>
<?php $admin_url = "/nightlives/admin"; ?>
<?php $home_url = "/nightlives"; ?>
<?php if(!isset($is_login) || !$is_login){ ?>
<div class="container admin">


    <header >
    <div class="top_header">
        <div class="logo"><a href="<?php echo $admin_url; ?>"><img src="<?php echo $admin_url; ?>/images/common/logo.png"></a></div>
        <ul>
            <li><a href="<?php echo $home_url; ?>"><img src="<?php echo $admin_url; ?>/images/common/home_icon.png"><span>Retour au site</span></a></li>
        </ul>
        <div id="disconnect"><img src="<?php echo $admin_url; ?>/images/common/power.png"></div>
    </div>
    <div class="left_header">
        <div class="admin_user">
            <img src="<?php echo FRONT_URL . $_SESSION['user']['photo']; ?>">
            <p><?php echo $_SESSION['user']['pseudo']; ?></p>
        </div>
        <nav>
            <ul class="menu">
                <li><a href="<?php echo $admin_url; ?>">Dashboard</a></li>
                <li><a href="<?php echo $admin_url; ?>/pages">Pages</a></li>
                <li><a href="<?php echo $admin_url; ?>/artistes">Artistes</a></li>
                <li><a href="<?php echo $admin_url; ?>/events">Événements</a></li>
                <li><a href="<?php echo $admin_url; ?>/lieux">Lieux</a></li>
                <li><a href="<?php echo $admin_url; ?>/mediatheque">Médiathèque</a></li>
                <li><a href="<?php echo $admin_url; ?>/users">Utilisateurs</a></li>
                <li><a href="<?php echo $admin_url; ?>/stats">Statistiques</a></li>
                <li><a href="<?php echo $admin_url; ?>/articles">Articles</a></li>
            </ul>
        </nav>
    </div>

    </header>
    <?php include($this->view); ?>
<?php }else{
    include($this->view);
    } ?>


</body>
</html>
