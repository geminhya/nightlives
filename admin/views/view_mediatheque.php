<div class="content">
  <h1>Médiathèque</h1>
  <form method="get" action="" class="auto_sumbit">
    <select name="sort">
      <option value="all" selected>Tous les médias</option>
      <option value="used" <?php if(!empty($_GET['sort']) && $_GET['sort'] == 'used'){ echo 'selected'; } ?>>Utilisés</option>
      <option value="unused" <?php if(!empty($_GET['sort']) && $_GET['sort'] == 'unused'){ echo 'selected'; } ?>>Non utilisés</option>
    </select>
    <select name="type">
      <option value="all" selected>Tous les types</option>
      <option value="1" <?php if(!empty($_GET['type']) && $_GET['type'] == '1'){ echo 'selected'; } ?>>Profils</option>
      <option value="2" <?php if(!empty($_GET['type']) && $_GET['type'] == '2'){ echo 'selected'; } ?>>Bannières</option>
    </select>
  </form>
  <div class="push_dahboard">
  <?php if($pages > 1){ ?>
    <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
    <?php if($current_page > 1){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
    <?php } ?>

    <?php if($current_page < $pages){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
    <?php } ?>
    </div>
    <div class="lineclear"></div>
  <?php } ?>
  <div class="lineclear"></div>
  <ul class="admin_list artists mediatheque_list">
    <?php foreach ($elements as $key => $user) { ?>

    <div class="img_container <?php if(!$user['used']){ echo 'unused'; } ?>">
      <a href="<?php echo $admin_url; ?>/media?id=<?php echo $user['id']; ?>"><img class="to_crop_h" src="<?php echo FRONT_URL . $user['lien']; ?>" alt="<?php echo $user['libelle']; ?>"  /></a>
    </div>
    <?php } ?>
  </ul>
  <div class="lineclear"></div>
  <?php if($pages > 1){ ?>
    <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
    <?php if($current_page > 1){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
    <?php } ?>

    <?php if($current_page < $pages){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
    <?php } ?>
    </div>
    <div class="lineclear"></div>
  <?php } ?>
  </div>
</div>
