<div class="content">
  <form class="form_search" name="user_search" method="post" action="">
    <input type="text" name='input_user' value="<?php if(!empty($_POST['input_user'])){ echo $_POST['input_user']; } ?>" placeholder="Rechercher un utilisateur" />
    <input type="submit" value="" />
  </form>
  <h1>Utilisateurs</h1>

  <div class="push_dahboard">
  <a class="add_sur" href="<?php echo $admin_url; ?>/user">Ajouter un utilisateur</a>
  <div class="pagination"><span>Page 1 sur 10</span><span class="link prev"></span><span class="link next"></span></div>
  <div class="lineclear"></div>
  <ul class="admin_list users">
    <?php foreach ($users as $key => $user) { ?>
      <li class="push_container <?php if($user['status'] == 0){ echo ' inactif'; } ?>"><a href="<?php echo $admin_url; ?>/user?user_id=<?php echo $user['id']; ?>"><img src="<?php if(!empty($user["photo"])){ echo FRONT_URL . $user["photo"]; }; ?>"><span class="name"><?php if(!empty($user["pseudo"])){ echo utf8_encode($user["pseudo"]); }else{ echo utf8_encode($user["prenom"]) . " " . utf8_encode($user["nom"]); } ?></span>
    <span class="email"><?php echo $user["email"]; ?></span></a></li>
    <?php } ?>
  </ul>
  <div class="lineclear"></div>
  <div class="pagination"><span>Page 1 sur 10</span><span class="link prev"></span><span class="link next"></span></div>
  <div class="lineclear"></div>
  </div>
</div>