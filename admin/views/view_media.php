<div class="content">
  <h1><?php echo $message_user; ?></h1>
  <div class="push_dahboard">
  <?php if(!empty($msg_error)){ ?>
    <div class="errors">
    <ul>
      <?php foreach ($msg_error as $key => $msg) { ?>
        <li><?php echo $msg; ?></li>
      <?php } ?>
      </ul>
    </div>
  <?php } ?>
  <?php if(!empty($msg_valid)){ ?>
    <div class="valid"><p><?php echo $msg_valid; ?> <?php if($edit_link){ ?><a href="<?php echo $edit_link; ?>">Modifier</a><?php } ?></p></div>
  <?php } ?>
  <div class="lineclear"></div>
  <div class="lineclear"></div>
  <form class="element" name="view_media" method="post" action="" enctype="multipart/form-data">


    <div class="half_left">
       <img src="<?php echo FRONT_URL . $lien; ?>" alt="<?php echo $libelle; ?>" class="img_media">
    </div><div class="half_left">
        <label>Texte Alternatif :</label>
        <label><strong><?php if (!empty($_POST['libelle'])) {
          echo $_POST['libelle'];
        }else{
          echo (isset($libelle)) ? utf8_encode($libelle) : '';
          } ?></strong></label>
          <?php if(!$used){ ?><p class="red">Ce média n'est pas utilisé.</p><?php } ?>
    </div>
  <div class="lineclear"></div>
  </form>
  </div>
  <a class="delete" href="<?php echo $admin_url ?>/delete?type=media&id=<?php echo $id; ?>">Supprimer le média</a>
</div>
