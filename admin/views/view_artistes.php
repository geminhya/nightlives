<div class="content">
  <form class="form_search" name="artist_search" method="post" action="">
    <input type="text" name='input_user' value="<?php if(!empty($_POST['input_user'])){ echo $_POST['input_user']; } ?>"  placeholder="Rechercher un artiste" />
    <input type="submit" value="" />
  </form>
  <h1>Artistes</h1>
  <div class="push_dahboard">
  <a class="add_sur" href="<?php echo $admin_url; ?>/artiste">Ajouter un artiste</a>
  <?php if($pages > 1){ ?>
    <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
    <?php if($current_page > 1){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
    <?php } ?>

    <?php if($current_page < $pages){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
    <?php } ?>
    </div>
    <div class="lineclear"></div>
  <?php } ?>
  <div class="lineclear"></div>
  <ul class="admin_list artists">
    <?php foreach ($elements as $key => $user) { ?>

    <li class="with_photo"><div class="img_container"><img src="<?php echo  FRONT_URL . $user['photo']; ?>"></div><a href="<?php echo $admin_url; ?>/artiste?id=<?php  echo $user['id']; ?>"><?php  echo utf8_encode($user["pseudo"]); ?> <span class="status"><?php if($user['status'] == 0){ echo 'Non publié'; }else{  echo 'Publié'; } ?></span></a></li>
    <?php } ?>
    
  </ul>
  <div class="lineclear"></div>
  <?php if($pages > 1){ ?>
    <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
    <?php if($current_page > 1){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
    <?php } ?>

    <?php if($current_page < $pages){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
    <?php } ?>
    </div>
    <div class="lineclear"></div>
  <?php } ?>
  </div>
</div>
