<link rel="stylesheet" href="<?php echo ADMIN_URL; ?>/styles/articles.css" />
<div class="afficheArticles">
    <?php
    foreach ($articles as $kk => $vv) {?>
        <div class="article">
            <h2><?php echo $vv['titre_article']; ?><br>
                <span id="date_publication">Publié le <?php echo $vv['date_parution'] ?> - par <?php echo $vv['prenom'] ?></span>
            </h2>
            <hr>
            <div class="text">
                <p class="paragraphe_article"><?php echo $vv['contenu']; ?></p>
                <?php if (!empty($vv['photo'])) { ?>
                    <img class="thumbnail" src="<?php echo FRONT_URL . $vv['photo']; ?>" atl="<?php echo (isset($alt)) ? $alt : ''; ?>">
                <?php } ?>
            </div>
            <div class="delete">
                <form class='deleteArticle' method="post" action="" >
                    <input type="hidden" name="titre_article" value="<?php echo $vv['titre_article']; ?>"><br>
                    <input type="hidden" name="article_ecrit" value="<?php echo $vv['contenu']; ?>"><br>
                    <input type="hidden" name="photo" value="<?php echo $vv['photo']; ?>" /><br>
                    <input type='hidden' name="deleteArticle" value='<?php echo $vv['id']; ?>'>
                    <input type="submit" name="submitDelete" value="Supprimer l'article">
                </form>
            </div>
        </div>
    <?php } ?>

</div>
<!--Pagination-->
        <?php if($pages > 1){ ?>
            <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
            <?php if($current_page > 1){ ?>
                <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
            <?php } ?>

            <?php if($current_page < $pages){ ?>
                <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
            <?php } ?>
            </div>
            <div class="lineclear"></div>
        <?php } ?>
        <!--Fin pagination-->
