<div class="content">
  <h1><?php echo $message_user; ?></h1>
  <div class="push_dahboard">
  <?php if(!empty($msg_error)){ ?>
    <div class="errors">
    <ul>
      <?php foreach ($msg_error as $key => $msg) { ?>
        <li><?php echo $msg; ?></li>
      <?php } ?>
      </ul>
    </div>
  <?php } ?>
  <?php if(!empty($msg_valid)){ ?>
    <div class="valid"><p><?php echo $msg_valid; ?> <?php if($edit_link){ ?><a href="<?php echo $edit_link; ?>">Modifier</a><?php } ?></p></div>
  <?php } ?>
  <?php if(!empty($_GET["create"]) && $_GET["create"] = "new"){ ?>
    <div class="valid"><p>Evênement créé.</p></div>
  <?php } ?>
  <div class="lineclear"></div>
  <form class="element" name="view_event" method="post" action="" enctype="multipart/form-data">
  <label>Nom</label>
  <input type="text" name="libelle" value="<?php if (!empty($_POST['libelle'])) {
    echo $_POST['libelle'];
  }else{
    echo (isset($libelle)) ? utf8_encode($libelle) : '';
    } ?>" />
  <div class="lineclear"></div>
  <label>Date de début</label>
  <input type="date" name="date_deb" value="<?php if (!empty($_POST['date_deb'])) {
    echo $_POST['date_deb'];
  }else{
    echo (isset($date_deb)) ? utf8_encode($date_deb) : '';
    } ?>" />
  <div class="lineclear"></div>
  <label>Date de fin</label>
  <input type="date" name="date_fin" value="<?php if (!empty($_POST['date_fin'])) {
    echo $_POST['date_fin'];
  }else{
    echo (isset($date_fin)) ? utf8_encode($date_fin) : '';
    } ?>" />
  <div class="lineclear"></div>
  <label>Type</label>
  <select name="id_type_event">
    <option value="1">Festival</option>
    <option value="2">Concert</option>
    <option value="3">Soirée</option>
  </select>

  <div class="lineclear"></div>
  <label>Lieu</label>
  <select name="id_lieu">
    <?php if(!empty($lieux)){ foreach ($lieux as $key => $lieu) { ?>
      <option value="<?php echo $lieu['id'] ?>" <?php if (!empty($_POST['id_lieu']) && $_POST['id_lieu'] == $lieu['id']) {
    echo 'selected';
  }elseif(isset($id_lieu) && $id_lieu == $lieu['id']){
    echo 'selected';
    } ?> ><?php echo $lieu['code_postal'] . ' - ' . utf8_encode($lieu['libelle']); ?></option>
    <?php } } ?>
  </select>
  <div class="lineclear"></div>
  <label>Ville</label>
  <input type="text" name="ville" value="<?php if (!empty($_POST['ville'])) {
    echo $_POST['ville'];
  }else{
    echo (isset($ville)) ? $ville : '';
    } ?>" />
    <div class="lineclear"></div>
  <label>Adresse Complète</label>
  <input type="text" name="adresse" value="<?php if (!empty($_POST['adresse'])) {
    echo $_POST['adresse'];
  }else{
    echo (isset($adresse)) ? utf8_encode($adresse) : '';
    } ?>" />
  <div class="lineclear"></div>
  <div class="autocomplete autocomplete_artistes">
    <label>Artistes Présents</label>
    <br>
    <div class="autocomplete_valid <?php if(empty($artistes_event) || empty($artistes_event[0])){ echo 'hidden'; } ?>">

      <?php if(!empty($artistes_event) && !empty($artistes_event[0])){
        foreach ($artistes_event as $key => $art) {
          $data = get_artist_by_id($art);
          echo "<span class='autocomplete_valid_span' data-id='" . $art . "' id='id_artistes_" . $art . "'>" . $data['pseudo'] . "</span>";
        }
        } ?>
    </div>
    <div class="autocomplete_input_container">
      <input type="text" class="autocomplete_input" name="autocomplete[artistes]" placeholder="Entrez les noms des artistes" />
      <div class="autcomplete_list" data-element="artiste">
      </div>
    </div>
   <ul class="listing_artistes" style="display:none;">
     <?php foreach ($artistes as $key => $artiste) { ?>

       <li><label> <input type="checkbox" name="artistes[]" class="artistes_hidden_<?php echo $artiste['id']; ?>" value="<?php echo $artiste['id']; ?>" <?php if(isset($artistes_event) &&  in_array($artiste['id'],
      $artistes_event)){ echo 'checked'; } ?> /></label> </li>
  <?php } ?>
   </ul>
  </div>
  <div class="lineclear"></div>
  <label>Photo Principale</label>
  <img class="thumbnail" src="<?php echo (isset($photo)) ? FRONT_URL . $photo : ''; ?>" atl="<?php echo (isset($alt)) ? $alt : ''; ?>">
  <input type="file" name="photo" value="" />
  <div class="lineclear"></div>
  <label>Couverture</label>
  <img class="cover" src="<?php echo (isset($banner)) ? FRONT_URL . $banner : ''; ?>" atl="<?php echo (isset($alt)) ? $alt : ''; ?>">
  <input type="file" name="banner" class="cover" value="" />
  <div class="lineclear"></div>
  <label>Information</label>
  <textarea name="informations"><?php if (!empty($_POST['informations'])) {
    echo $_POST['informations'];
  }else{
    echo (isset($informations)) ? utf8_encode($informations) : '';
    } ?></textarea>
  <div class="lineclear"></div>
  <label>Lien d'achats de tickets</label>
  <input type="text" name="link_buy" value="<?php if (!empty($_POST['link_buy'])) {
    echo $_POST['link_buy'];
  }else{
    echo (isset($link_buy)) ? $link_buy : '';
    } ?>" />
  <div class="lineclear"></div>
  <label>Lien externe pour plus d'informations</label>
  <input type="text" name="link_more" value="<?php if (!empty($_POST['link_more'])) {
    echo $_POST['link_more'];
  }else{
    echo (isset($link_more)) ? $link_more : '';
    } ?>" />
  <div class="lineclear"></div>
  <input type="hidden" name="admin_view_event" value="submitted" />
  <input type="hidden" name="id_photo" value="<?php echo (isset($id_photo)) ? utf8_encode($id_photo) : ''; ?>" />
  <input type="hidden" name="id_banner" value="<?php echo (isset($id_banner)) ? utf8_encode($id_banner) : ''; ?>" />
  <input type="submit" value="Valider" />
  </form>
  </div>
  <a class="delete" href="<?php echo $admin_url ?>/delete?type=event&id=<?php echo $id; ?>">Supprimer l'evenement</a>

</div>
