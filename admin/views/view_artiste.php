<div class="content">
  <h1><?php echo $message_user; ?></h1>
  <div class="push_dahboard">

  <?php if(!empty($msg_error)){ ?>
    <div class="errors">
    <ul>
      <?php foreach ($msg_error as $key => $msg) { ?>
        <li><?php echo $msg; ?></li>
      <?php } ?>
      </ul>
    </div>
  <?php } ?>
  <?php if(!empty($msg_valid)){ ?>
    <div class="valid"><p><?php echo $msg_valid; ?> <?php if($edit_link){ ?><a href="<?php echo $edit_link; ?>">Modifier</a><?php } ?></p></div>
  <?php } ?>
  <?php if(!empty($_GET["create"]) && $_GET["create"] = "new"){ ?>
    <div class="valid"><p>Utilisateur créé.</p></div>
  <?php } ?>
  <div class="lineclear"></div>
  <form class="element" name="artist_search" method="post" action="" enctype="multipart/form-data">
  <h2>Informations</h2>
  <label>Pseudo</label>
  <input type="text" name="pseudo" value="<?php if (!empty($_POST['pseudo'])) {
    echo $_POST['pseudo'];
  }else{
    echo (isset($pseudo)) ? utf8_encode($pseudo) : '';
    } ?>" />
<div class="lineclear"></div>

<label>Genre</label>
<select name="id_type_musique">
  <option value="1" <?php if(isset($id_type_musique) && $id_type_musique == "1"){ echo 'selected'; } ?>>Electro</option>
  <option value="2" <?php if(isset($id_type_musique) && $id_type_musique == "2"){ echo 'selected'; } ?>>Pop</option>
  <option value="3" <?php if(isset($id_type_musique) && $id_type_musique == "3"){ echo 'selected'; } ?>>Rock</option>
  <option value="4" <?php if(isset($id_type_musique) && $id_type_musique == "4"){ echo 'selected'; } ?>>Autre</option>
</select>
<div class="lineclear"></div>

<label>Pays</label>
<select name="pays">
<?php foreach ($liste_pays as $key => $ps) {; ?>
  <option value="<?php echo $ps[3]; ?>" <?php if(isset($pays) && $pays == $ps[3]){ echo 'selected'; } ?>><?php echo $ps[3]; ?></option>
<?php } ?>
  
</select>
<div class="lineclear"></div>
  <label>Biographie</label>
  <textarea name="biographie"><?php if (!empty($_POST['biographie'])) {
    echo $_POST['biographie'];
  }else{
    echo (isset($biographie)) ? $biographie : '';
    } ?></textarea>
<div class="lineclear"></div>
  <label>Statut activé</label>
  <input type="checkbox" name="status" value="1" <?php if (!empty($_POST['status']) && $_POST['status'] == '1') {
    echo 'checked';
  }else{
    echo (isset($status) && $status == '1') ? 'checked' : '';
    } ?> />
<div class="lineclear"></div>
  <label>Photo</label>
  <img class="thumbnail" src="<?php echo (isset($photo)) ? FRONT_URL . $photo : ''; ?>" atl="<?php echo (isset($alt)) ? $alt : ''; ?>">
  <input type="file" name="photo" value="" />
  <div class="lineclear"></div>
  <label>Couverture
  <br>
  <input type="file" name="banner" value="" />
  </label>
  <img class="cover" src="<?php echo (isset($banner)) ? FRONT_URL . $banner : ''; ?>" atl="<?php echo (isset($alt)) ? $alt : ''; ?>">
  <div class="lineclear"></div>
    <label>Iframe <i style="font-size: 12px;">(Souncloud, Spotify ou Autre)</i></label>
    <textarea name="iframe"><?php if (!empty($_POST['iframe'])) {
      echo $_POST['iframe'];
    }else{
      echo (isset($iframe)) ? utf8_encode($iframe) : '';
    } ?></textarea>
  <div class="lineclear"></div>

  <label>Facebook</label>
  <input type="text" name="facebook" value="<?php if (!empty($_POST['facebook'])) {
    echo $_POST['facebook'];
  }else{
    echo (isset($facebook)) ? utf8_encode($facebook) : '';
    } ?>" />
  <div class="lineclear"></div>

  <label>Twitter</label>
  <input type="text" name="twitter" value="<?php if (!empty($_POST['twitter'])) {
    echo $_POST['twitter'];
  }else{
    echo (isset($twitter)) ? utf8_encode($twitter) : '';
    } ?>" />
  <div class="lineclear"></div>

  <label>Youtube</label>
  <input type="text" name="youtube" value="<?php if (!empty($_POST['youtube'])) {
    echo $_POST['youtube'];
  }else{
    echo (isset($youtube)) ? utf8_encode($youtube) : '';
    } ?>" />
  <div class="lineclear"></div>

  <label>Instagram</label>
  <input type="text" name="instagram" value="<?php if (!empty($_POST['instagram'])) {
    echo $_POST['instagram'];
  }else{
    echo (isset($instagram)) ? utf8_encode($instagram) : '';
    } ?>" />
  <div class="lineclear"></div>

  <label>Soundcloud</label>
  <input type="text" name="soundcloud" value="<?php if (!empty($_POST['soundcloud'])) {
    echo $_POST['soundcloud'];
  }else{
    echo (isset($soundcloud)) ? utf8_encode($soundcloud) : '';
    } ?>" />
  <div class="lineclear"></div>

  <input type="hidden" name="admin_view_user" value="submitted" />
  <input type="hidden" name="id_photo" value="<?php echo (isset($id_photo)) ? utf8_encode($id_photo) : 0; ?>" />
  <input type="hidden" name="id_banner" value="<?php echo (isset($id_banner)) ? utf8_encode($id_banner) : 1; ?>" />
  <input type="submit" value="Valider" />
  </form>
  </div>
  
  <a class="delete" href="<?php echo $admin_url ?>/delete?type=artiste&id=<?php echo $id; ?>">Supprimer l'artiste</a>

</div>
