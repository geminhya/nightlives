<div class="content">
  <h1><?php echo $message_user; ?></h1>
  <div class="push_dahboard">
  <?php if(!empty($msg_error)){ ?>
    <div class="errors">
    <ul>
      <?php foreach ($msg_error as $key => $msg) { ?>
        <li><?php echo $msg; ?></li>
      <?php } ?>
      </ul>
    </div>
  <?php } ?>
  <?php if(!empty($msg_valid)){ ?>
    <div class="valid"><p><?php echo $msg_valid; ?> <?php if($edit_link){ ?><a href="<?php echo $edit_link; ?>">Modifier</a><?php } ?></p></div>
  <?php } ?>
  <?php if(!empty($_GET["create"]) && $_GET["create"] = "new"){ ?>
    <div class="valid"><p>Evênement créé.</p></div>
  <?php } ?>
  <div class="lineclear"></div>
  <form class="element" name="view_event" method="post" action="" enctype="multipart/form-data">
  <label>Nom</label>
  <input type="text" name="libelle" value="<?php if (!empty($_POST['libelle'])) {
    echo $_POST['libelle'];
  }else{
    echo (isset($libelle)) ? utf8_encode($libelle) : '';
    } ?>" />
  <div class="lineclear"></div>
  <label>Adresse</label>
  <input type="text" name="adresse" value="<?php if (!empty($_POST['adresse'])) {
    echo $_POST['adresse'];
  }else{
    echo (isset($adresse)) ? utf8_encode($adresse) : '';
    } ?>" />
  <div class="lineclear"></div>
  <label>Code Postal</label>
  <input type="text" name="code_postal" value="<?php if (!empty($_POST['code_postal'])) {
    echo $_POST['code_postal'];
  }else{
    echo (isset($code_postal)) ? utf8_encode($code_postal) : '';
    } ?>" />
  <div class="lineclear"></div>
  <label>Ville</label>
  <input type="text" name="ville" value="<?php if (!empty($_POST['ville'])) {
    echo $_POST['ville'];
  }else{
    echo (isset($ville)) ? $ville : '';
    } ?>" />
  <div class="lineclear"></div>
  <label>Photo</label>
  <img class="thumbnail" src="<?php echo (isset($photo)) ? FRONT_URL . $photo : ''; ?>" atl="<?php echo (isset($alt)) ? $alt : ''; ?>">
  <input type="file" name="photo" value="" />
  <div class="lineclear"></div>
  <input type="hidden" name="admin_view_event" value="submitted" />
  <input type="hidden" name="id_photo" value="<?php echo (isset($id_photo)) ? utf8_encode($id_photo) : ''; ?>" />
  <input type="submit" value="Valider" />
  </form>

  </div></div>
