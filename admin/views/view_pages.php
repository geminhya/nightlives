<div class="content">
  <form class="form_search" name="artist_search" method="post" action="">
    <input type="text" value="" placeholder="Rechercher un evênement" />
    <input type="submit" value="" />
  </form>
  <h1>Pages</h1>
  <div class="push_dahboard">
  <?php if(!empty($msg_error)){ ?>
    <div class="errors">
    <ul>
      <?php foreach ($msg_error as $key => $msg) { ?>
        <li><?php echo $msg; ?></li>
      <?php } ?>
      </ul>
    </div>
  <?php } ?>
  <?php if(!empty($msg_valid)){ ?>
    <div class="valid"><p><?php echo $msg_valid; ?> <?php if($edit_link){ ?><a href="<?php echo $edit_link; ?>">Modifier</a><?php } ?></p></div>
  <?php } ?>
  <form id="admin_form_pages" class="element" method="post" action="" enctype="multipart/form-data">
    <h2>Homepage</h2>
    <h3>Slider</h3>
    <?php foreach ($slides as $key => $slide) { ?>
      <div class="bloc slide">
      <div class="inline">
        <img src="<?php if(!empty($slide["photo"])){ echo FRONT_URL . $slide["photo"]; }; ?>">
      </div>
      <div class="inline">
        <div class="lineclear"></div>
        <label><strong><?php echo $slide['title']; ?></strong></label>
        <div class="lineclear"></div>
        <label><?php echo $slide['subtitle']; ?></label>
        <div class="lineclear"></div>
        <a href="<?php echo $admin_url ?>/delete?type=slide&id=<?php echo $slide['id']; ?>">Supprimer la slide</a>
      </div>
      
    </div>
    <?php } ?>
    <div class="bloc virgin">
      <input name="photo" type="file" />
      <div class="lineclear"></div>
      <input name="title" type="text" placeholder="Titre" />
      <div class="lineclear"></div>
      <input name="subtitle" type="text" placeholder="Sous-titre" />
      <div class="lineclear"></div>
      <input name="lien" type="text" placeholder="Url" />
    </div>
    <input type="submit" value="Valider" />
  </form>
  <div class="lineclear"></div>
  <div class="lineclear"></div>
  </div>
</div>