<script type="text/javascript" src="<?php echo $home_url; ?>/ckeditor/ckeditor.js"></script>
<div class="edition_articles">
    <h1>
        Modifiez les articles
    </h1>
    <?php foreach ($articles as $kk => $vv) { ?>
    <h2>
        <?php echo $vv['titre_article']; ?> <br> publié le <?php echo $vv['date_parution']; ?>
    </h2>
        <form id="articles_creation" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="id_article" value="<?php echo $vv['id'] ?>">
            <input type="text" name="titre_article" placeholder="Entrez le titre de l'article" value="<?php echo $vv['titre_article'] ?>"><br>
            <textarea name="article_ecrit" class="ckeditor"><?php echo $vv['contenu'] ?></textarea><br>
            <input type="file" name="photo" value="<?php echo $vv['photo'] ?>" /><br>
            <input type="submit" value="Enregistrer">
        </form>
    <hr>
    <?php } ?>
</div>
<!--Pagination-->
        <?php if($pages > 1){ ?>
            <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
            <?php if($current_page > 1){ ?>
                <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
            <?php } ?>

            <?php if($current_page < $pages){ ?>
                <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
            <?php } ?>
            </div>
            <div class="lineclear"></div>
        <?php } ?>
        <!--Fin pagination-->
