<div class="content">
  <h1><?php echo $message_user; ?></h1>
  <div class="push_dahboard">

  <?php if(!empty($msg_error)){ ?>
    <div class="errors">
    <ul>
      <?php foreach ($msg_error as $key => $msg) { ?>
        <li><?php echo $msg; ?></li>
      <?php } ?>
      </ul>
    </div>
  <?php } ?>
  <?php if(!empty($msg_valid)){ ?>
    <div class="valid"><p><?php echo $msg_valid; ?> <?php if($edit_link){ ?><a href="<?php echo $edit_link; ?>">Modifier</a><?php } ?></p></div>
  <?php } ?>
  <?php if(!empty($_GET["create"]) && $_GET["create"] = "new"){ ?>
    <div class="valid"><p>Utilisateur créé.</p></div>
  <?php } ?>
  <div class="lineclear"></div>
  <form class="element" name="artist_search" method="post" action="" enctype="multipart/form-data">
  <h2>Informations</h2>
  <label>Pseudo</label>
  <input type="text" name="pseudo" value="<?php if (!empty($_POST['pseudo'])) {
    echo $_POST['pseudo'];
  }else{
    echo (isset($pseudo)) ? utf8_encode($pseudo) : '';
    } ?>" />
<div class="lineclear"></div>
  <label>Nom</label>
  <input type="text" name="nom" value="<?php if (!empty($_POST['nom'])) {
    echo $_POST['nom'];
  }else{
    echo (isset($nom)) ? utf8_encode($nom) : '';
    } ?>" />
<div class="lineclear"></div>
  <label>Prénom</label>
  <input type="text" name="prenom" value="<?php if (!empty($_POST['prenom'])) {
    echo $_POST['prenom'];
  }else{
    echo (isset($prenom)) ? utf8_encode($prenom) : '';
    } ?>" />
<div class="lineclear"></div>
  <label>Ville</label>
  <input type="text" name="ville" value="<?php if (!empty($_POST['ville'])) {
    echo $_POST['ville'];
  }else{
    echo (isset($ville)) ? utf8_encode($ville) : '';
    } ?>" />
<div class="lineclear"></div>
  <label>Email</label>
  <input type="text" name="email" value="<?php if (!empty($_POST['email'])) {
    echo $_POST['email'];
  }else{
    echo (isset($email)) ? utf8_encode($email) : '';
    } ?>" />
<div class="lineclear"></div>
  <label>Statut activé</label>
  <input type="checkbox" name="status" value="1" <?php if (!empty($_POST['status']) && $_POST['status'] == '1') {
    echo 'checked';
  }else{
    echo (isset($status) && $status == '1') ? 'checked' : '';
    } ?> />
<div class="lineclear"></div>
  <label>Changer le mot de passe</label>
  <input type="password" placeholder="Mot de passe" name="password" value="<?php if (!empty($_POST['password'])) {
    echo $_POST['password'];
  }else{
    echo (isset($password)) ? utf8_encode($password) : '';
    } ?>" />
    <label> </label>
  <input type="password" placeholder="Confirmation Mot de passe" name="password_conf" value="<?php if (!empty($_POST['password_conf'])) {
    echo $_POST['password_conf'];
  }else{
    echo (isset($password_conf)) ? utf8_encode($password_conf) : '';
    } ?>" />
<div class="lineclear"></div>
  <label>Photo</label>
  <img class="thumbnail" src="<?php echo (isset($photo)) ? FRONT_URL . $photo : ''; ?>" atl="<?php echo (isset($alt)) ? $alt : ''; ?>">
  <input type="file" name="photo" value="" />
  <div class="lineclear"></div>
  <input type="hidden" name="admin_view_user" value="submitted" />
  <input type="hidden" name="id_media" value="<?php echo (isset($id_media)) ? utf8_encode($id_media) : ''; ?>" />
  <input type="submit" value="Valider" />
  </form>
  </div>
  <a class="delete" href="<?php echo $admin_url ?>/delete?type=user&id=<?php echo $id; ?>">Supprimer l'utilisateur</a>
</div>

