<div class="content">
  <form class="form_search" name="artist_search" method="post" action="">
    <input type="text" value="" placeholder="Rechercher un evênement" />
    <input type="submit" value="" />
  </form>
  <h1>Événements</h1>
  <div class="push_dahboard">
  <a class="add_sur" href="<?php echo $admin_url; ?>/event">Ajouter un evênement</a>
  <?php if($pages > 1){ ?>
    <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
    <?php if($current_page > 1){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
    <?php } ?>

    <?php if($current_page < $pages){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
    <?php } ?>
    </div>
    <div class="lineclear"></div>
  <?php } ?>
  <div class="lineclear"></div>
  <ul class="admin_list artists">
    <?php foreach ($elements as $key => $user) { ?>

    <li><a href="<?php echo $admin_url; ?>/event?id=<?php  echo $user['id']; ?>"><?php  echo utf8_encode($user["libelle"]); ?></a></li>
    <?php } ?>

  </ul>
  <?php if($pages > 1){ ?>
    <div class="pagination"><span>Page <?php echo $current_page; ?> sur <?php echo $pages; ?></span>
    <?php if($current_page > 1){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $prev_page; ?>"><span class="link prev"></span></a>
    <?php } ?>

    <?php if($current_page < $pages){ ?>
    <a href="<?php echo $current_url; ?>?page=<?php echo $next_page; ?>"><span class="link next"></span></a>
    <?php } ?>
    </div>
    <div class="lineclear"></div>
  <?php } ?>
  <div class="lineclear"></div>
  </div>
</div>
