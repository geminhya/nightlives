<?php


class eventsController{

    public function indexAction($args){

        $users = [];
        $v = new view();

        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $current_url = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        $v->assign("current_url",$current_url);
        $nb_per_page = 15;
        $start = 0;
        if(!empty($_GET['page'])){
            $start += (intval($_GET['page']) - 1) * $nb_per_page;
        }
        $v->assign("start",$start);
        $next_page = 2;
        $prev_page = 0;
        if ($start > 0) {
            $next_page = intval($_GET['page']) + 1;
            $prev_page = intval($_GET['page']) - 1;
        }
        $v->assign("next_page",$next_page);
        $v->assign("prev_page",$prev_page);
        $v->assign("current_page", (!empty($_GET['page'])) ? $_GET['page'] : 1 );



        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
            
            // On garde met les notes_mark_read(database_name, user_name, note_id)s des attributs de la classe enfant dans $columns

            if(!empty($_POST) && !empty(trim($_POST['input_user']))){
                $users = $this->post($v);
            }else{
                $sql = "SELECT * FROM evenement WHERE archive = 0;";

                $stmt = $this->pdo->query($sql);
                if($stmt){
                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                        $users[] = $row;
                    }
                }
            }

            $sql = "SELECT COUNT(DISTINCT(id)) as total FROM evenement WHERE archive = 0;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $total_elements = $row['total'];
                }
            }
            $v->assign("total_elements",$total_elements);
            $v->assign("pages", ceil($total_elements / $nb_per_page));



        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }

        
        $v->setView("events");
        $v->assign("elements",$users);

        if(!empty($_POST) && !empty(trim($_POST['input_user']))){
            $users = $this->post();
        }
    }
    
    public function testAction($args){
        echo "bonjour";
    }

    public function post(){
        $users = [];
        if(!empty(trim($_POST['input_user']))){
            $input_user = trim($_POST['input_user']);
            $sql = "SELECT u.*, m.lien as photo, m.libelle as alt
            FROM user u, media m
            WHERE (pseudo LIKE '$input_user'
            OR nom LIKE '$input_user'
            OR prenom LIKE '$input_user'
            OR ville LIKE '$input_user'
            OR email LIKE '$input_user')
            AND archive = 0
            AND u.id_media = m.id;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $users[] = $row;
                }
            }
        }
        return $users;
    }
}

