
<?php


class mediaController{

    public function indexAction($args){
        global $t_departements;
        $v = new view();
        $v->setView("media");
        $v->assign("t_departements",$t_departements);

        $new_artist = !isset($_GET["id"]);

        if(!empty($_POST)){
            $this->post($v, $new_artist);
        }

        $this->get($v, $new_artist);
    }

    private function post($v, $new_artist){
        global $pdo;

        if(!empty($_POST) && isset($_POST["admin_view_event"]) && $_POST["admin_view_event"] == "submitted"){
            $error = false;
            $pseudo = trim($_POST['libelle']);
            $easy_name = easy_name($pseudo);
            $biographie = trim($_POST['informations']);
            $id_photo = trim($_POST['id_photo']);
            $id_banner = trim($_POST['id_banner']);
            $ville = trim($_POST['ville']);
            $id_lieu = $_POST['id_lieu'];
            $id_type_musique = $_POST['id_type_event'];
            $date_deb = $_POST['date_deb'];
            $date_fin = $_POST['date_fin'];
            if(!empty($_POST['artistes'])){
                $artistes = "," . implode(',', $_POST['artistes']) . ",";
            }else{
                $artistes = "";
            }

            $url_tickets = trim($_POST['link_buy']);
            $msg_error = [];


                if(empty($pseudo)) {
                  $error = TRUE;
                  $msg_error[] = "Le libellé doit être rempli.";
                }

                if(!isset($date_deb) || !(strtotime($date_deb) > 0)  || !(strtotime($date_deb) > strtotime("now"))) {
                  $error = TRUE;
                  $msg_error[] = "La date de début n'est pas correcte.";
                }

                if(empty($_POST['id_type_event'])) {
                  $error = TRUE;
                  $msg_error[] = "Le type n'est pas rempli.";
                }

                if(!isset($date_fin) || !(strtotime($date_fin) > 0)  || !(strtotime($date_fin) > strtotime("now")) || !(strtotime($date_deb) <= strtotime($date_fin))) {
                  $error = TRUE;
                  $msg_error[] = "La date de fin n'est pas correcte.";
                }

                if($new_artist && event_exists($easy_name)){
                  $error = TRUE;
                  $msg_error[] = "L'evênement est déjà existant.";
                }

                // si une photo a été uploadée
                if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                    $file = $_FILES["photo"];
                    if($file["error"] != 0) {
                      $error = TRUE;
                      $msg_error[] = "Erreur lors de l'importation de l'image.";
                    }
                    if($file["type"] != "image/jpeg") {
                      $error = TRUE;
                      $msg_error[] = "L'image doit être au format jpeg.";
                    }
                    if($file["size"] > "1000000") {
                      $error = TRUE;
                      $msg_error[] = "La photo ne doit pas dépasser 1Mo.";
                    }
                }

                // si une bannière a été uploadée
                if (!empty($_FILES["banner"]) && $_FILES["banner"]["size"] > 0) {
                    $file = $_FILES["banner"];
                    if($file["error"] != 0) {
                      $error = TRUE;
                      $msg_error[] = "Erreur lors de l'importation de la bannière.";
                    }
                    if($file["type"] != "image/jpeg") {
                      $error = TRUE;
                      $msg_error[] = "La bannière doit être au format jpeg.";
                    }
                    if($file["size"] > "2000000") {
                      $error = TRUE;
                      $msg_error[] = "La bannière ne doit pas dépasser 2Mo";
                    }
                }
              //$error = true;
            if(!$error){
                $artist = new evenement();
                $pseudo = utf8_decode(addslashes($pseudo));
                $artist->set_libelle($pseudo);
                $artist->set_easy_name($easy_name);
                $artist->set_informations(addslashes($biographie));
                $artist->set_id_type_event($id_type_musique);
                $artist->set_date_deb($date_deb);
                $artist->set_date_fin($date_fin);
                $artist->set_artistes($artistes);
                $artist->set_ville($ville);
                $artist->set_url_tickets($url_tickets);
                $artist->set_archive(0);
                $artist->set_id_lieu($id_lieu);

                if(isset($_POST["status"])){
                    $artist->set_status(1);
                }else{
                    $artist->set_status(0);
                }

                if(!$new_artist){
                    $user_id = $_GET["id"];
                    $artist->set_id($user_id);

                    // ajout de la photo de BD
                    if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                        $file = $_FILES["photo"];
                        $md5 = "uploads/event/profile/" . md5($easy_name) . ".jpg";
                        $libelle = "Photo de profil de " . addslashes($pseudo);
                        $libelle = utf8_encode($libelle);
                        if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            if(isset($_POST["id_photo"]) &&  $_POST["id_photo"] > 0){
                                $id_photo = $_POST["id_photo"];
                                $sql = "UPDATE media SET lien='$md5', libelle='$libelle' WHERE id=$id_photo;";
                                $query = $pdo->prepare($sql);
                                $query->execute();
                            }else{
                                $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('1', '$md5', '$libelle')";
                                $query = $pdo->prepare($sql);
                                $query->execute();

                                $sql = "SELECT id FROM media WHERE lien='$md5';";
                                $query = $pdo->query($sql);
                                if($query){
                                    $row = $query->fetch(PDO::FETCH_ASSOC);
                                    if($row){
                                        $artist->set_id_photo($row["id"]);
                                    }
                                }
                            }
                        }
                    }else{
                        $artist->set_id_photo($_POST["id_photo"]);
                    }

                    // ajout de la bannière de BD
                    if (!empty($_FILES["banner"]) && $_FILES["banner"]["size"] > 0) {
                        $file = $_FILES["banner"];
                        $md5 = "uploads/event/banner/" . md5($easy_name) . ".jpg";
                        $libelle = "Photo de couverture de " . addslashes($pseudo);
                        $libelle = utf8_encode($libelle);

                        if (move_uploaded_file($_FILES['banner']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            if(isset($_POST["id_banner"]) &&  $_POST["id_banner"] > 1){
                                $id_banner = $_POST["id_banner"];
                                $sql = "UPDATE media SET lien='$md5', libelle='$libelle' WHERE id=$id_banner;";
                                $query = $pdo->prepare($sql);
                                $query->execute();
                            }else{
                                $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('2', '$md5', '$libelle')";
                                $query = $pdo->prepare($sql);
                                $query->execute();

                                $sql = "SELECT id FROM media WHERE lien='$md5';";
                                $query = $pdo->query($sql);
                                if($query){
                                    $row = $query->fetch(PDO::FETCH_ASSOC);
                                    if($row){
                                        $artist->set_id_banner($row["id"]);
                                    }
                                }

                            }

                        }
                    }else{
                        $artist->set_id_banner($_POST["id_banner"]);
                    }
                    $v->assign("edit_link",false);
                    $artist->save();
                    $v->assign("msg_valid","Evênement mis à jour.");
                }else{
                    $artist->set_status(1);
                    $v->assign("msg_valid","Evênement créé.");
                    if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                        $artist->set_id_photo(0);
                        $file = $_FILES["photo"];
                        $md5 = "uploads/event/profile/" . md5($easy_name) . ".jpg";
                        global $pdo;
                        $libelle = "Photo de profil de " . $pseudo;
                        if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('1', '$md5', '$libelle')";
                            $query = $pdo->prepare($sql);
                            $query->execute();
                            $sql = "SELECT id FROM media WHERE lien='$md5';";
                            $query = $pdo->query($sql);
                            if($query){
                                $row = $query->fetch(PDO::FETCH_ASSOC);
                                if($row){
                                    $artist->set_id_photo($row["id"]);
                                }
                            }
                        }
                    }

                    if (!empty($_FILES["banner"]) && $_FILES["banner"]["size"] > 0) {

                        $artist->set_id_banner(1);
                        $file = $_FILES["banner"];
                        $md5 = "uploads/event/banner/" . md5($easy_name) . ".jpg";
                        global $pdo;
                        $libelle = "Photo de couverture de " . $pseudo;
                        if (move_uploaded_file($_FILES['banner']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('2', '$md5', '$libelle')";
                            $query = $pdo->prepare($sql);
                            $query->execute();
                            $sql = "SELECT id FROM media WHERE lien='$md5';";
                            $query = $pdo->query($sql);
                            if($query){
                                $row = $query->fetch(PDO::FETCH_ASSOC);
                                if($row){
                                    $artist->set_id_banner($row["id"]);
                                }
                            }
                        }
                    }
                    $artist->save();
                    $new_id = artist_exists($easy_name);
                    $link = 'event?id=' . $new_id;
                    $v->assign("edit_link",$link);
                }

            }else{
                $v->assign("msg_error",$msg_error);
            }
        }
    }

    private function get($v, $new_artist){
        $artistes = [];
        $lieux = [];
        if(!$new_artist){
            $user_id = $_GET["id"];
            $v->assign("message_user","Média");

            try {
                $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
                $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

                // On garde met les notes_mark_read(database_name, user_name, note_id)s des attributs de la classe enfant dans $columns
                $sql = "SELECT u.*
                FROM media u
                WHERE u.id=$user_id
                AND u.archive = '0';";
                $stmt = $this->pdo->query($sql);
                if($stmt){
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    if(!empty($row)){
                        $used = false;
                        foreach ($row as $key => $line) {
                            $v->assign($key, $line);

                        }
                        $sql2 = "SELECT m.id FROM media m, artiste a, evenement e, slide s, user u WHERE (a.id_photo = m.id OR a.id_banner = m.id OR e.id_media = m.id OR s.id_media = m.id OR u.id_media = m.id) AND m.id = $user_id AND m.archive = '0' ORDER BY id;";
                        $stmt2 = $this->pdo->query($sql2);
                        if($stmt2){
                            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)){
                                if(!empty($row2)){
                                    $used = true;
                                }
                            }
                        }
                        $v->assign('used', $used);
                    }
                }

            } catch (Exception $e) {
                die("Erreur SQL : " . $e->getMessage());
            }

        }else{
            $v->assign("message_user","Nouvel évênement");
            $v->assign("new_user",true);
        }
        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

            $sql = "SELECT * FROM artiste ORDER BY pseudo;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $artistes[] = $row;
                }
            }

            $sql = "SELECT * FROM lieu ORDER BY code_postal, libelle;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $lieux[] = $row;
                }
            }

        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }

        $v->assign("lieux",$lieux);
        $v->assign("artistes",$artistes);
    }
}

