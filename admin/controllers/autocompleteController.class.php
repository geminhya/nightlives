
<?php


class autocompleteController{

    public function indexAction($args){


    }

    public function artistesAction($args){
        global $home_url, $pdo;

        $users = [];
        if(!empty(trim($_POST['autocomplete_input']))){
            $input_user = trim($_POST['autocomplete_input']);
            $sql = "SELECT u.id, u.pseudo
            FROM artiste u
            WHERE (pseudo LIKE '%$input_user%'
            OR easy_name LIKE '%$input_user%'
            OR id LIKE '%$input_user%')
            AND archive = 0
            ORDER BY pseudo ASC;";
            $stmt = $pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $users[] = $row;
                }
            }
        }
        echo json_encode($users);
    }

}
