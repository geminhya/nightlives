<?php


class artisteController{
    
    public function indexAction($args){

        $v = new view();
        $v->setView("artiste");

        $new_artist = !isset($_GET["id"]);

        if(!empty($_POST)){
            $this->post($v, $new_artist);
        }

        $this->get($v, $new_artist);
    }

    private function post($v, $new_artist){
        global $pdo;

        if(!empty($_POST) && isset($_POST["admin_view_user"]) && $_POST["admin_view_user"] == "submitted"){
            $error = false;
            $pseudo = trim($_POST['pseudo']);
            $easy_name = easy_name($pseudo);
            $biographie = trim($_POST['biographie']);
            $id_photo = trim($_POST['id_photo']);
            $id_banner = trim($_POST['id_banner']);
            $id_type_musique = $_POST['id_type_musique'];
            $iframe = trim($_POST['iframe']);
            $facebook = $_POST['facebook'];
            $twitter = $_POST['twitter'];
            $youtube = $_POST['youtube'];
            $instagram = $_POST['instagram'];
            $soundcloud = $_POST['soundcloud'];
            $pays = $_POST['pays'];
            $msg_error = [];

            if( isset($_POST['pseudo']) ){

                if(strlen($pseudo) < 2 || strlen($pseudo) > 200) {
                  $error = TRUE;
                  $msg_error[] = "Le Pseudo doit être de 2 à 200 caractères";
                }

                if($new_artist && event_exists($easy_name)){
                  $error = TRUE;
                  $msg_error[] = "L'artiste est déjà existant";
                }

                // si une photo a été uploadée
                if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                    $file = $_FILES["photo"];
                    if($file["error"] != 0) {
                      $error = TRUE;
                      $msg_error[] = "Erreur lors de l'importation de l'image.";
                    }
                    if($file["type"] != "image/jpeg") {
                      $error = TRUE;
                      $msg_error[] = "L'image doit être au format jpeg.";
                    }
                    if($file["size"] > "1000000") {
                      $error = TRUE;
                      $msg_error[] = "L'image ne doit pas dépasser 1Mo";
                    }
                }

                // si une bannière a été uploadée
                if (!empty($_FILES["banner"]) && $_FILES["banner"]["size"] > 0) {
                    $file = $_FILES["banner"];
                    if($file["error"] != 0) {
                      $error = TRUE;
                      $msg_error[] = "Erreur lors de l'importation de la bannière.";
                    }
                    if($file["type"] != "image/jpeg") {
                      $error = TRUE;
                      $msg_error[] = "La bannière doit être au format jpeg.";
                    }
                    if($file["size"] > "3000000") {
                      $error = TRUE;
                      $msg_error[] = "La bannière ne doit pas dépasser 3Mo";
                    }
                }

              }
              //$error = true;
            if(!$error){
                $artist = new artiste();
                $pseudo = utf8_decode($pseudo);
                $artist->set_pseudo($pseudo);
                $artist->set_easy_name($easy_name);
                $artist->set_biographie(addslashes($biographie));
                $artist->set_id_type_musique($id_type_musique);
                $artist->set_iframe(addslashes($iframe));
                $artist->set_facebook($facebook);
                $artist->set_twitter($twitter);
                $artist->set_youtube($youtube);
                $artist->set_instagram($instagram);
                $artist->set_soundcloud($soundcloud);
                $artist->set_pays($pays);
                $artist->set_archive(0);

                if(isset($_POST["status"])){
                    $artist->set_status(1);
                }else{
                    $artist->set_status(0);                
                }

                if(!$new_artist){
                    $user_id = $_GET["id"];
                    $artist->set_id($user_id);
                    $artist->set_id_photo($id_photo);
                    $artist->set_id_banner($id_banner);
                    $v->assign("msg_valid","Artiste mis à jour.");

                    // ajout de la photo de BD
                    if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                        $file = $_FILES["photo"];
                        $md5 = "uploads/artist/profile/" . md5($easy_name) . ".jpg";
                        $libelle = "Photo de profil de " . $pseudo;
                        if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            if(isset($_POST["id_photo"]) &&  $_POST["id_photo"] > 0){
                                $id_photo = $_POST["id_photo"];
                                $sql = "UPDATE media SET lien='$md5', libelle='$libelle' WHERE id=$id_photo;";
                                $query = $pdo->prepare($sql);
                                $query->execute();
                            }else{
                                $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('1', '$md5', '$libelle')";
                                $query = $pdo->prepare($sql);
                                $query->execute();

                                $sql = "SELECT id FROM media WHERE lien='$md5';";
                                $query = $pdo->query($sql);
                                if($query){
                                    $row = $query->fetch(PDO::FETCH_ASSOC);
                                    if($row){
                                        $artist->set_id_photo($row["id"]);
                                    }
                                }
                            }
                        }
                    }

                    // ajout de la bannière de BD
                    if (!empty($_FILES["banner"]) && $_FILES["banner"]["size"] > 0) {
                        $file = $_FILES["banner"];
                        $md5 = "uploads/artist/banner/" . md5($easy_name) . ".jpg";
                        $libelle = "Photo de couverture de " . $pseudo;
                        if (move_uploaded_file($_FILES['banner']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            if(isset($_POST["id_banner"]) &&  $_POST["id_banner"] > 0){
                                $id_banner = $_POST["id_banner"];
                                $sql = "UPDATE media SET lien='$md5', libelle='$libelle' WHERE id=$id_banner;";
                                $query = $pdo->prepare($sql);
                                $query->execute();
                            }else{
                                $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('2', '$md5', '$libelle')";
                                $query = $pdo->prepare($sql);
                                $query->execute();

                                $sql = "SELECT id FROM media WHERE lien='$md5';";
                                $query = $pdo->query($sql);
                                if($query){
                                    $row = $query->fetch(PDO::FETCH_ASSOC);
                                    if($row){
                                        $artist->set_id_banner($row["id"]);
                                    }
                                }
                                
                            }
                            
                        }
                    }
                    $v->assign("edit_link",false);
                    $artist->save();
                }else{
                    $artist->set_status(1);
                    $v->assign("msg_valid","Artiste créé.");
                    if (!empty($_FILES["photo"])) {
                        $file = $_FILES["photo"];
                        $md5 = "uploads/artist/profile/" . md5($easy_name) . ".jpg";
                        global $pdo;
                        $libelle = "Photo de profil de " . $pseudo;
                        if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('1', '$md5', '$libelle')";
                            $query = $pdo->prepare($sql);
                            $query->execute();
                            $sql = "SELECT id FROM media WHERE lien='$md5';";
                            $query = $pdo->query($sql);
                            if($query){
                                $row = $query->fetch(PDO::FETCH_ASSOC);
                                if($row){
                                    $artist->set_id_photo($row["id"]);
                                }
                            }
                        }
                    }else{
                        $artist->set_id_photo(0);
                    }

                    if (!empty($_FILES["banner"]) && $_FILES["banner"]['size'] > 0) {
                        $file = $_FILES["banner"];
                        $md5 = "uploads/artist/banner/" . md5($easy_name) . ".jpg";
                        global $pdo;
                        $libelle = "Photo de couverture de " . $pseudo;
                        if (move_uploaded_file($_FILES['banner']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('2', '$md5', '$libelle')";
                            $query = $pdo->prepare($sql);
                            $query->execute();
                            $sql = "SELECT id FROM media WHERE lien='$md5';";
                            $query = $pdo->query($sql);
                            if($query){
                                $row = $query->fetch(PDO::FETCH_ASSOC);
                                if($row){
                                    $artist->set_id_banner($row["id"]);
                                }
                            }
                        }
                    }else{
                        $artist->set_id_banner(1);
                    }
                    $artist->save(); 
                    $new_id = artist_exists($easy_name);
                    $link = 'artiste?id=' . $new_id;
                    $v->assign("edit_link",$link);
                }
                
            }else{
                $v->assign("msg_error",$msg_error);
            }
        }
    }

    private function get($v, $new_artist){
        $liste_pays = get_pays();
        $v->assign("liste_pays",$liste_pays);

        if(!$new_artist){
            $user_id = $_GET["id"];
            $v->assign("message_user","Modifier l'artiste");

            try {
                $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
                $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
                
                // On garde met les notes_mark_read(database_name, user_name, note_id)s des attributs de la classe enfant dans $columns
                $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt
                FROM artiste u, media mp, media mb
                WHERE u.id=$user_id
                AND u.id_photo = mp.id
                AND u.id_banner = mb.id;";
                $stmt = $this->pdo->query($sql);
                if($stmt){
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    if($row){
                        foreach ($row as $key => $line) {
                            $v->assign($key, $line);
                        }
                    }
                }
            } catch (Exception $e) {
                die("Erreur SQL : " . $e->getMessage());
            }

        }else{
            $v->assign("message_user","Nouvel artiste");
            $v->assign("new_user",true);
            $v->assign("photo","uploads/user/profile/nightlives_default_user.jpg");
        }
    }
}

