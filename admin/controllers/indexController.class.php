<?php

class indexController{
    
    public function indexAction($args){

        $v = new view();
        $v->setView("index");

        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

            $this->set_comments($v);
        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }
        
    }

    private function set_comments($v){
        $slides = [];
        $sql = "SELECT * FROM commentaire WHERE valid = 0 ORDER BY id DESC;";
        $stmt = $this->pdo->query($sql);
        if($stmt){
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $slides[] = $row;
            }
        }
        $v->assign("comments",$slides);
    }
}

