
<?php

class mediathequeController{

    public function indexAction($args){
        $users = [];

        $v = new view();
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $current_url = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        $v->assign("current_url",$current_url);
        $nb_per_page = 48;
        $start = 0;
        if(!empty($_GET['page'])){
            $start += (intval($_GET['page']) - 1) * $nb_per_page;
        }
        $v->assign("start",$start);
        $next_page = 2;
        $prev_page = 0;
        if ($start > 0) {
            $next_page = intval($_GET['page']) + 1;
            $prev_page = intval($_GET['page']) - 1;
        }
        $v->assign("next_page",$next_page);
        $v->assign("prev_page",$prev_page);
        $v->assign("current_page", (!empty($_GET['page'])) ? $_GET['page'] : 1 );

        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

            if(!empty($_POST) && !empty(trim($_POST['input_user']))){
                $users = $this->post($v);
            }else{
                $type = "";
                if(!empty($_GET['type']) && $_GET['type'] != 'all'){
                    $type = " AND m.id_type='" . $_GET['type'] . "' ";
                }
                if(!empty($_GET['sort']) && $_GET['sort'] != 'all'){
                    $sql = "SELECT m.* FROM media m WHERE m.archive = '0'" . $type . " ORDER BY id DESC;";
                }else{
                    $sql = "SELECT m.* FROM media m WHERE m.archive = '0'" . $type . " ORDER BY id DESC LIMIT $start,$nb_per_page;";
                }
                $stmt = $this->pdo->query($sql);
                if($stmt){
                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                        $media_id = $row['id'];
                        $row['used'] = false;
                        $sql2 = "SELECT m.id FROM media m, artiste a, evenement e, slide s, user u WHERE (a.id_photo = m.id OR a.id_banner = m.id OR e.id_media = m.id OR s.id_media = m.id OR u.id_media = m.id) AND m.id = $media_id AND m.archive = '0' ORDER BY id;";
                        $stmt2 = $this->pdo->query($sql2);
                        if($stmt2){
                            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)){
                                if(!empty($row2)){
                                    $row['used'] = true;
                                }
                            }
                        }
                        if(empty($_GET)){
                            $users[] = $row;
                        }elseif (!empty($_GET['sort']) && $_GET['sort'] == 'unused') {
                            if(!$row['used']){
                                $users[] = $row;
                            }
                        }elseif (!empty($_GET['sort']) && $_GET['sort'] == 'used') {
                            if($row['used']){
                                $users[] = $row;
                            }
                        }else{
                            $users[] = $row;
                        }

                    }
                }
            }
            $sql = "SELECT COUNT(DISTINCT(id)) as total FROM media;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $total_elements = $row['total'];
                }
            }
            $v->assign("total_elements",$total_elements);

            $v->assign("pages", ceil($total_elements / $nb_per_page));


        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }


        $v->setView("mediatheque");
        $v->assign("elements",$users);

        if(!empty($_POST)){
            $this->post();
        }
    }

    public function post(){
        $users = [];
        return $users;

    }
}

