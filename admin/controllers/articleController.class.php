<?php

class articleController {

    public function indexAction($args) {
        $v = new view();
        $v->setView("articles");
    }

    public function articlesecritureAction($args) {
        $v = new view();
        $v->setView("articlesecriture");
        $msg_error = [];
        $error = false;
        if (!empty($_POST['article_ecrit'])) {
            if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                $file = $_FILES["photo"];
                if ($file["error"] != 0) {
                    $error = TRUE;
                    $msg_error[] = "Erreur lors de l'importation de la photo.";
                }
                if ($file["type"] != "image/jpeg") {
                    $error = TRUE;
                    $msg_error[] = "La photo doit être au format jpeg.";
                }
                if ($file["size"] > "2000000") {
                    $error = TRUE;
                    $msg_error[] = "La photo ne doit pas dépasser 2Mo";
                }
            }
            if (strlen($_POST['titre_article']) < 2 || strlen($_POST['titre_article']) > 200) {
                $error = TRUE;
                $msg_error[] = "Le titre de l'article doit comporter entre 2 et 200 caractères";
            }
            if (!$error) {
                $article = new article();
                $article->set_id_redacteur($_SESSION["user"]["id"]);
                $article->set_titre_article($_POST['titre_article']);
                $article->set_contenu($_POST['article_ecrit']);

                if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                    $article->set_photo(0);
                    $file = $_FILES["photo"];
                    $md5 = "images/articles/" . md5($_FILES["photo"]["name"]) . ".jpg";

                    global $pdo;
                    $libelle = "Image de l'article nommé : " . $_POST['titre_article'];
                    if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                        $sql = 'INSERT INTO media (id_type, lien, libelle) VALUES ("3", "' . $md5 . '", "' . $libelle . '")';
                        $query = $pdo->prepare($sql);
                        $query->execute();
                        $article->set_photo($md5);
                    }
                }
                $article->set_date_parution(date("Y-m-d-G-i-s", strtotime("now")));
//                echo '<pre>';
//                print_r(date("Y-m-d-G-i-s", strtotime("now")));
//                echo '<pre>';
//                die();
                $article->save();


                header("Refresh:0");
            } else {
                $v->assign("error_message", $msg_error);
            }
        }
    }

    public function editArticleAction() {
        $v = new view();
        $v->setView("articlesedition");
        //-------Pagination--------
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $current_url = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        $v->assign("current_url", $current_url);
        $nb_per_page = 3;
        $start = 0;
        if (!empty($_GET['page'])) {
            $start += (intval($_GET['page']) - 1) * $nb_per_page;
        }
        $v->assign("start", $start);
        $next_page = 2;
        $prev_page = 0;
        if ($start > 0) {
            $next_page = intval($_GET['page']) + 1;
            $prev_page = intval($_GET['page']) - 1;
        }
        $v->assign("next_page", $next_page);
        $v->assign("prev_page", $prev_page);
        $v->assign("current_page", (!empty($_GET['page'])) ? $_GET['page'] : 1 );
        //-------Fin-pagination--------
        $articles = getArticles($start, $nb_per_page);
        $v->assign("articles", $articles);
        if (!empty($_POST['article_ecrit'])) {
            $article = new article();
            $article->set_id($_POST['id_article']);
            $article->set_id_redacteur($_SESSION["user"]["id"]);
            $article->set_titre_article($_POST['titre_article']);
            $article->set_contenu($_POST['article_ecrit']);
            if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                $article->set_photo(0);
                $file = $_FILES["photo"];
                $md5 = "images/articles/" . md5($_FILES["photo"]["name"]) . ".jpg";

                global $pdo;
                $libelle = "Image de l'article nommé : " . $_POST['titre_article'];
                if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                    $sql = 'INSERT INTO media (id_type, lien, libelle) VALUES ("3", "' . $md5 . '", "' . $libelle . '")';
                    $query = $pdo->prepare($sql);
                    $query->execute();
                    $article->set_photo($md5);
                }
            }
            $article->set_date_modification(date("Y-m-d", strtotime("now")));
            $article->save();
            header("Refresh:0");
        }
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
        $sql = "SELECT COUNT(DISTINCT(id)) as total FROM article WHERE archive = 0;";
        $stmt = $this->pdo->query($sql);
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $total_elements = $row['total'];
            }
        }
        $v->assign("total_elements", $total_elements);
        $v->assign("pages", ceil($total_elements / $nb_per_page));
    }

    public function deleteArticleAction() {
        $v = new view();
        $v->setView("articlesdelete");
        //-------Pagination--------
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $current_url = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        $v->assign("current_url", $current_url);
        $nb_per_page = 3;
        $start = 0;
        if (!empty($_GET['page'])) {
            $start += (intval($_GET['page']) - 1) * $nb_per_page;
        }
        $v->assign("start", $start);
        $next_page = 2;
        $prev_page = 0;
        if ($start > 0) {
            $next_page = intval($_GET['page']) + 1;
            $prev_page = intval($_GET['page']) - 1;
        }
        $v->assign("next_page", $next_page);
        $v->assign("prev_page", $prev_page);
        $v->assign("current_page", (!empty($_GET['page'])) ? $_GET['page'] : 1 );
        //-------Fin-pagination--------
        $articles = getArticles($start, $nb_per_page);
        $v->assign("articles", $articles);
        if (!empty($_POST['deleteArticle'])) {
            $article = new article();
            $article->set_id($_POST['deleteArticle']);
            $article->set_id_redacteur($_SESSION["user"]["id"]);
            $article->set_titre_article($_POST['titre_article']);
            $article->set_contenu($_POST['article_ecrit']);
            $article->set_photo($_POST['photo']);
            $article->set_date_modification(date("Y-m-d", strtotime("now")));
            $article->set_archive(1);
            $article->save();
            header("Refresh:0");
        }
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
        $sql = "SELECT COUNT(DISTINCT(id)) as total FROM article WHERE archive = 0;";
        $stmt = $this->pdo->query($sql);
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $total_elements = $row['total'];
            }
        }
        $v->assign("total_elements", $total_elements);
        $v->assign("pages", ceil($total_elements / $nb_per_page));
    }

}
