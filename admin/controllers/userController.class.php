<?php


class userController{


    
    public function indexAction($args){

        $v = new view();
        $v->setView("user");

        $new_user = !isset($_GET["user_id"]);

        if(!empty($_POST)){
            $this->post($v, $new_user);
        }

        $this->get($v, $new_user);
    }

    private function post($v, $new_user){
        global $pdo;

        if(!empty($_POST) && isset($_POST["admin_view_user"]) && $_POST["admin_view_user"] == "submitted"){
            $error = false;
            $pseudo = trim($_POST['pseudo']);
            $nom = trim($_POST['nom']);
            $prenom = trim($_POST['prenom']);
            $ville = trim($_POST['ville']);
            $email = trim($_POST['email']);
            $id_media = trim($_POST['id_media']);

            $password = trim($_POST['password']);
            $password_conf = trim($_POST['password_conf']);
            $msg_error = [];

            if( isset($_POST['pseudo']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['ville']) && isset($_POST['email'])){
                
                if(strlen($pseudo) < 3 || strlen($pseudo) > 20) {
                  $error = TRUE;
                  $msg_error[] = "Le Pseudo doit être de 3 à 20 caractères";
                }

                // if (!ctype_alpha($nom)) {
                //   $error = TRUE;
                //   $msg_error[] = "Le nom ne doit pas contenir de chiffres";
                // }

                if (strlen($nom) < 1) {
                  $error = TRUE;
                  $msg_error[] = "Le nom doit être de plus d'un caractère";
                }

                // if (!ctype_alpha($prenom)) {
                //   $error = TRUE;
                //   $msg_error[] = "Le prénom ne doit pas contenir de chiffres";
                // }

                if (strlen($prenom) < 1) {
                  $error = TRUE;
                  $msg_error[] = "Le prenom doit être de plus d'un caractère";
                }

                // if( $nom === $prenom){
                //     $error = TRUE;
                //     $msg_error[] = "Le prénom doit être différent du nom";
                // }

                // if (!ctype_alpha($ville)) {
                //   $error = TRUE;
                //   $msg_error[] = "La ville ne doit pas contenir de chiffres";
                // }

                if (strlen($ville) < 1) {
                  $error = TRUE;
                  $msg_error[] = "La ville doit être de plus d'un caractère";
                }

                if( !filter_var($email, FILTER_VALIDATE_EMAIL) || ($new_user && email_exists($email)) ){
                    $error = TRUE;
                    $msg_error[] = "Email invalide ou déjà utilisé";
                }

                // si une photo a été uploadée
                if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                    $file = $_FILES["photo"];
                    if($file["error"] != 0) {
                      $error = TRUE;
                      $msg_error[] = "Erreur lors de l'importation de l'image.";
                    }
                    if($file["type"] != "image/jpeg") {
                      $error = TRUE;
                      $msg_error[] = "L'image doit être au format jpeg.";
                    }
                    if($file["size"] > "300000") {
                      $error = TRUE;
                      $msg_error[] = "L'image ne doit pas dépasser 300Ko";
                    }
                }

                if(!empty($password) && $password != $password_conf){
                    $error = TRUE;
                    $msg_error[] = "Les mots de passes ne sont pas égaux.";
                }
              }
            //$error = true;
            if(!$error){
                $user = new user();
                $pseudo = utf8_decode($pseudo);
                $nom = utf8_decode($nom);
                $prenom = utf8_decode($prenom);
                $ville = utf8_decode($ville);

                $user->set_pseudo($pseudo);
                $user->set_nom($nom);
                $user->set_prenom($prenom);
                $user->set_ville($ville);
                $user->set_email($email);

                $user->set_archive(0);
                if(!empty($password)){
                    $user->set_mdp(md5($password));
                }
                if(isset($_POST["status"])){
                    $user->set_status(1);
                }else{
                    $user->set_status(0);
                }

                if(!$new_user){
                    if(empty($password)){
                        $user->set_mdp(md5("esgipass"));
                    }
                    $user_id = $_GET["user_id"];
                    $user->set_id($user_id);
                    $user->set_id_media($id_media);
                    $v->assign("msg_valid","Utilisateur mis à jour.");

                    // ajout de la photo de BD
                    if (!empty($_FILES["photo"])) {
                        $file = $_FILES["photo"];
                        $md5 = "uploads/user/profile/" . md5($email) . ".jpg";
                        $libelle = "Photo de profil de " . $pseudo;
                        if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            if(isset($_POST["id_media"]) &&  $_POST["id_media"] > 0){
                                $id_media = $_POST["id_media"];
                                $sql = "UPDATE media SET lien='$md5', libelle='$libelle' WHERE id=$id_media;";
                                $query = $pdo->prepare($sql);
                                $query->execute();
                            }else{
                                $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('1', '$md5', '$libelle')";
                                $query = $pdo->prepare($sql);
                                $query->execute();

                                $sql = "SELECT id FROM media WHERE lien='$md5';";
                                $query = $pdo->query($sql);
                                if($query){
                                    $row = $query->fetch(PDO::FETCH_ASSOC);
                                    if($row){
                                        $user->set_id_media($row["id"]);
                                    }
                                }
                                
                            }
                            
                        }
                    }
                    $v->assign("edit_link",false);
                    $user->save(); 
                    
                }else{
                    $user->set_id_media(0);
                    $user->set_status(1);
                    $v->assign("msg_valid","Utilisateur créé.");
                    if (!empty($_FILES["photo"])) {
                        $file = $_FILES["photo"];
                        $md5 = "uploads/user/profile/" . md5($email) . ".jpg";
                        global $pdo;
                        $libelle = "Photo de profil de " . $pseudo;
                        if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('1', '$md5', '$libelle')";
                            $query = $pdo->prepare($sql);
                            $query->execute();
                            $sql = "SELECT id FROM media WHERE lien='$md5';";
                            $query = $pdo->query($sql);
                            if($query){
                                $row = $query->fetch(PDO::FETCH_ASSOC);
                                if($row){
                                    $user->set_id_media($row["id"]);
                                }
                            }
                        }
                    }
                    $user->save(); 
                    $new_id = email_exists($email);
                    $link = 'user?user_id=' . $new_id;
                    $v->assign("edit_link",$link);
                }
                
            }else{
                $v->assign("msg_error",$msg_error);
            }
        }
    }

    private function get($v, $new_user){
        

        if(!$new_user){
            $user_id = $_GET["user_id"];
            $v->assign("message_user","Modifier l'utilisateur");

            try {
                $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
                $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
                
                // On garde met les notes_mark_read(database_name, user_name, note_id)s des attributs de la classe enfant dans $columns
                $sql = "SELECT u.*, m.lien as photo, m.libelle as alt FROM user u, media m WHERE u.id=$user_id AND u.id_media = m.id;";
                $stmt = $this->pdo->query($sql);
                if($stmt){
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    foreach ($row as $key => $line) {
                        $v->assign($key, $line);
                    }
                }
            } catch (Exception $e) {
                die("Erreur SQL : " . $e->getMessage());
            }

        }else{
            $v->assign("message_user","Nouvel utilisateur");
            $v->assign("new_user",true);
            $v->assign("photo","uploads/user/profile/nightlives_default_user.jpg");
        }
    }
}

