<?php

class pagesController{
    
    public function indexAction($args){
    	global $pdo;

        $v = new view();
        $v->setView("pages");
        $slides = [];
        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

            $sql = "SELECT u.*, m.lien as photo, m.libelle as alt FROM slide u, media m WHERE u.id_media = m.id ORDER BY ordre;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $slides[] = $row;
                }
            }


        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }
        $v->assign("slides",$slides);
        
        if(!empty($_POST)){
            $users = $this->post($v);
        }
    }
    
    private function post($v){
        global $pdo;

        if(true){
            $error = false;
            $title = trim($_POST['title']);
            $subtitle = trim($_POST['subtitle']);
            $msg_error = [];

            if( isset($_POST['title']) ){


                // si une photo a été uploadée
                if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                    $file = $_FILES["photo"];
                    if($file["error"] != 0) {
                      $error = TRUE;
                      $msg_error[] = "Erreur lors de l'importation de l'image.";
                    }
                    if($file["type"] != "image/jpeg") {
                      $error = TRUE;
                      $msg_error[] = "L'image doit être au format jpeg.";
                    }
                    if($file["size"] > "300000") {
                      $error = TRUE;
                      $msg_error[] = "L'image ne doit pas dépasser 300Ko";
                    }
                }
              }
            //$error = true;
            if(!$error){
                $user = new slide();
                $title = utf8_decode($title);
                $subtitle = utf8_decode($subtitle);
                $lien = trim($lien);
                

                $user->set_title($title);
                $user->set_subtitle($subtitle);
                $user->set_id_media(1);
                $user->set_lien($lien);

                $artist->set_archive(0);
                
                

                // ajout de la photo de BD
                if (!empty($_FILES["photo"])) {
                    $file = $_FILES["photo"];
                    $md5 = "uploads/user/profile/" . md5($title) . ".jpg";
                    $libelle = "Photo de slide de " . $title;
                    if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                        $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('1', '$md5', '$libelle')";
                        $query = $pdo->prepare($sql);
                        $query->execute();

                        $sql = "SELECT id FROM media WHERE lien='$md5';";
                        $query = $pdo->query($sql);
                        if($query){
                            $row = $query->fetch(PDO::FETCH_ASSOC);
                            if($row){
                                $user->set_id_media($row["id"]);
                            }
                        }
                        
                    }
                    $v->assign("msg_valid","Slide ajouté");
                }
                $v->assign("edit_link",false);
                $user->save(); 

                
            }else{
                $v->assign("msg_error",$msg_error);
            }
        }
    }
}

