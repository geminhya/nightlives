<?php

class articlesController {

    public function indexAction($args) {
        $v = new view();
        $v->setView("articles");
    }

    public function articlesafficheAction($args) {
        $v = new view();
        $v->setView("articlesaffiche");
        $commentaires = getCommentaires();
        $v->assign("commentaires", $commentaires);
         //-------Pagination--------
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $current_url = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        $v->assign("current_url", $current_url);
        $nb_per_page = 3;
        $start = 0;
        if (!empty($_GET['page'])) {
            $start += (intval($_GET['page']) - 1) * $nb_per_page;
        }
        $v->assign("start", $start);
        $next_page = 2;
        $prev_page = 0;
        if ($start > 0) {
            $next_page = intval($_GET['page']) + 1;
            $prev_page = intval($_GET['page']) - 1;
        }
        $v->assign("next_page", $next_page);
        $v->assign("prev_page", $prev_page);
        $v->assign("current_page", (!empty($_GET['page'])) ? $_GET['page'] : 1 );
        //-------Fin-pagination--------
        $articles = getArticles($start, $nb_per_page);
        $v->assign("articles", $articles);
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
        $sql = "SELECT COUNT(DISTINCT(id)) as total FROM article WHERE archive = 0;";
        $stmt = $this->pdo->query($sql);
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $total_elements = $row['total'];
            }
        }
        $v->assign("total_elements", $total_elements);
        $v->assign("pages", ceil($total_elements / $nb_per_page));
    }
}
