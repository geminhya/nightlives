
<?php

class loginController{

    public function indexAction($args){
    	global $pdo;

        $v = new view();
        $v->setView("login");
        $v->assign("is_login", true);

        if(!empty($_POST)){
        	$email = trim($_POST['emailco']);
	        $mdp = trim($_POST['pwdco']);
	        $error = FALSE;
	        $msg_error = "";
	        if (empty($email) && empty($mdp)) {
	            $error = TRUE;
	            $msg_error.= "<li>Veuillez renseigner ces champs.</li> ";
	        } else {

	            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	                $error = TRUE;
	                $msg_error .= "<li>Email invalide.</li>";
	            } else {
	                $user = new user();
	                $userinfos = getUser($email);

	                if (!empty($userinfos)) {
	                	//var_dump("user info not empty");
	                    if ($userinfos['mdp'] === md5($mdp) && $userinfos['status'] == 2) {
	                        if (session_status() == PHP_SESSION_NONE) {
					            session_start();
					        }
	                        //$_SESSION["user"] = $userinfos;
	                        $_SESSION["user"]["id"] = $userinfos["id"];
	                        $_SESSION["user"]["pseudo"] = $userinfos["pseudo"];
	                        $_SESSION["user"]["photo"] = $userinfos["photo"];
	                        $_SESSION["user"]["email"] = $userinfos["email"];
	                        header("Location: " . ADMIN_URL);
	                    } else {
	                        $error = TRUE;
	                        $msg_error.= "<li>L'adresse email ou le mot de passe sont incorrects.</li>";
	                    }
	                } else {
	                    $error = TRUE;
	                    $msg_error.= "<li>L'adresse email ou le mot de passe sont incorrects.</li>";
	                }
	            }
	        }
	        $v->assign("msg_error", $msg_error);
	        $v->assign("valid", !$error);
        }
    }

    public function testAction($args){
        echo "bonjour";
    }
}
