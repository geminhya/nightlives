<?php

class statsController{
    
    public function indexAction($args){

        $v = new view();
        $v->setView("stats");

        $this->get($v);
    }
    
    public function get($v){
    	$stats = [];
    	$stats["pages"] = [];
    	$stats["events"] = [];
    	$stats["artists"] = [];
        
        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
            
            /* ======================================== */
            /* ============= PARTIE PAGES ============= */
            /* ======================================== */

            $sql = "SELECT SUM(count) as sum FROM page WHERE type='page';";
	        $stmt = $this->pdo->query($sql);
	        if($stmt){
	            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	            	$sum = $row["sum"];
	            }
	        }
	        $sql = "SELECT * FROM page WHERE type='page' ORDER BY count DESC LIMIT 0,1;";
	        $stmt = $this->pdo->query($sql);
	        if($stmt){
	            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	            	$max_percent = $row["count"];
	            }
	        }
	        $max_percent = round(($max_percent / $sum) * 100, 2);
	        if($sum > 0){
	        	$sql = "SELECT * FROM page WHERE type='page' ORDER BY count DESC LIMIT 0,10;";
		        $stmt = $this->pdo->query($sql);
		        if($stmt){
		            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		            	$row["percent"] = (round(($row["count"] / $sum) * 100, 2));
		            	$row["class"] = "v" . ceil( (($row["percent"] * 100) / $max_percent)  / 10) * 10;
		                $stats["pages"][] = $row;
		            }
		        }
	        }

	        /* ========================================= */
            /* ============= PARTIE EVENTS ============= */
            /* ========================================= */
            
            $sql = "SELECT SUM(count) as sum FROM page WHERE type='event';";
	        $stmt = $this->pdo->query($sql);
	        if($stmt){
	            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	            	$sum = $row["sum"];
	            }
	        }
	        $sql = "SELECT * FROM page WHERE type='event' ORDER BY count DESC LIMIT 0,1;";
	        $stmt = $this->pdo->query($sql);
	        if($stmt){
	            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	            	$max_percent = $row["count"];
	            }
	        }
	        $max_percent = round(($max_percent / $sum) * 100, 2);
	        if($sum > 0){
	        	$sql = "SELECT * FROM page WHERE type='event' ORDER BY count DESC LIMIT 0,10;";
		        $stmt = $this->pdo->query($sql);
		        if($stmt){
		            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		            	$row["percent"] = (round(($row["count"] / $sum) * 100, 2));
		            	$row["class"] = "v" . ceil( (($row["percent"] * 100) / $max_percent)  / 10) * 10;
		                $stats["events"][] = $row;
		            }
		        }
	        }

	        /* ========================================= */
            /* ============= PARTIE ARTISTES =========== */
            /* ========================================= */
            
            $sql = "SELECT SUM(count) as sum FROM page WHERE type='artist';";
	        $stmt = $this->pdo->query($sql);
	        if($stmt){
	            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	            	$sum = $row["sum"];
	            }
	        }
	        $sql = "SELECT * FROM page WHERE type='artist' ORDER BY count DESC LIMIT 0,1;";
	        $stmt = $this->pdo->query($sql);
	        if($stmt){
	            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	            	$max_percent = $row["count"];
	            }
	        }
	        $max_percent = round(($max_percent / $sum) * 100, 2);
	        if($sum > 0){
	        	$sql = "SELECT * FROM page WHERE type='artist' ORDER BY count DESC LIMIT 0,10;";
		        $stmt = $this->pdo->query($sql);
		        if($stmt){
		            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		            	$row["percent"] = (round(($row["count"] / $sum) * 100, 2));
		            	$row["class"] = "v" . ceil( (($row["percent"] * 100) / $max_percent)  / 10) * 10;
		                $stats["artist"][] = $row;
		            }
		        }
	        }
        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }

        $v->assign("stats",$stats);
    }
}

