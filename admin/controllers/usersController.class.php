<?php

class usersController{
    
    public function indexAction($args){
        $users = [];

        $v = new view();

        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
            
            // On garde met les notes_mark_read(database_name, user_name, note_id)s des attributs de la classe enfant dans $columns

            if(!empty($_POST) && !empty(trim($_POST['input_user']))){
                $users = $this->post($v);
            }else{

                $sql = "SELECT u.*, m.lien as photo, m.libelle as alt FROM user u, media m WHERE u.id_media = m.id AND archive = 0;";
                $stmt = $this->pdo->query($sql);
                if($stmt){
                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                        $users[] = $row;
                    }
                }
            }
            


        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }

        
        $v->setView("users");
        $v->assign("users",$users);

        if(!empty($_POST) && !empty(trim($_POST['input_user']))){
            $users = $this->post();
        }
    }
    
    public function testAction($args){
        echo "bonjour";
    }

    public function post(){
        $users = [];
        if(!empty(trim($_POST['input_user']))){
            $input_user = trim($_POST['input_user']);
            $sql = "SELECT u.*, m.lien as photo, m.libelle as alt
            FROM user u, media m
            WHERE (pseudo LIKE '$input_user'
            OR nom LIKE '$input_user'
            OR prenom LIKE '$input_user'
            OR ville LIKE '$input_user'
            OR email LIKE '$input_user')
            AND u.id_media = m.id;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $users[] = $row;
                }
            }
        }
        return $users;
    }
}

