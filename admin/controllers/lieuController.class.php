
<?php


class lieuController{

    public function indexAction($args){
        global $t_departements;
        $v = new view();
        $v->setView("lieu");

        $new_artist = !isset($_GET["id"]);

        if(!empty($_POST)){
            $this->post($v, $new_artist);
        }

        $this->get($v, $new_artist);
    }

    private function post($v, $new_artist){
        global $pdo;

        if(!empty($_POST) && isset($_POST["admin_view_event"]) && $_POST["admin_view_event"] == "submitted"){
            $error = false;
            $pseudo = trim($_POST['libelle']);
            $easy_name = easy_name($pseudo);
            $id_photo = trim($_POST['id_photo']);

            $adresse = trim($_POST['adresse']);
            $code_postal = trim($_POST['code_postal']);
            $ville = trim($_POST['ville']);
            $msg_error = [];


                if(empty($pseudo)) {
                  $error = TRUE;
                  $msg_error[] = "Le Nom doit être rempli.";
                }
                if(empty($adresse)) {
                  $error = TRUE;
                  $msg_error[] = "L'adresse doit être remplie.";
                }
                if(empty($code_postal)) {
                  $error = TRUE;
                  $msg_error[] = "Le code postal doit être rempli.";
                }else{
                    if(strlen($code_postal) < 3 || strlen($code_postal) > 5){
                        $error = TRUE;
                        $msg_error[] = "Le code postal doit être numéric et de 3 à 5 caractères.";
                    }
                }
                if(empty($ville)) {
                  $error = TRUE;
                  $msg_error[] = "La ville doit être remplie.";
                }

                // si une photo a été uploadée
                if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                    $file = $_FILES["photo"];
                    if($file["error"] != 0) {
                      $error = TRUE;
                      $msg_error[] = "Erreur lors de l'importation de l'image.";
                    }
                    if($file["type"] != "image/jpeg") {
                      $error = TRUE;
                      $msg_error[] = "L'image doit être au format jpeg.";
                    }
                    if($file["size"] > "1000000") {
                      $error = TRUE;
                      $msg_error[] = "La photo ne doit pas dépasser 1Mo.";
                    }
                }
              //$error = true;
            if(!$error){
                $artist = new lieu();
                $pseudo = utf8_decode(addslashes($pseudo));
                $artist->set_libelle($pseudo);
                $artist->set_easy_name($easy_name);
                $artist->set_ville($ville);
                $artist->set_code_postal($code_postal);
                $artist->set_adresse($adresse);

                if(!$new_artist){
                    $user_id = $_GET["id"];
                    $artist->set_id($user_id);

                    // ajout de la photo de BD
                    if (!empty($_FILES["photo"]) && $_FILES["photo"]["size"] > 0) {
                        $file = $_FILES["photo"];
                        $md5 = "uploads/event/profile/" . md5($easy_name) . ".jpg";
                        $libelle = "Photo de profil de " . addslashes($pseudo);
                        $libelle = utf8_encode($libelle);
                        if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            if(isset($_POST["id_photo"]) &&  $_POST["id_photo"] > 0){
                                $id_photo = $_POST["id_photo"];
                                $sql = "UPDATE media SET lien='$md5', libelle='$libelle' WHERE id=$id_photo;";
                                $query = $pdo->prepare($sql);
                                $query->execute();
                            }else{
                                $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('1', '$md5', '$libelle')";
                                $query = $pdo->prepare($sql);
                                $query->execute();

                                $sql = "SELECT id FROM media WHERE lien='$md5';";
                                $query = $pdo->query($sql);
                                if($query){
                                    $row = $query->fetch(PDO::FETCH_ASSOC);
                                    if($row){
                                        $artist->set_logo($row["id"]);
                                    }else{
                                        $artist->set_logo(0);
                                    }
                                }else{
                                    $artist->set_logo(0);
                                }
                            }
                        }

                    }

                    // ajout de la bannière de BD
                    if (!empty($_FILES["banner"]) && $_FILES["banner"]["size"] > 0) {
                        $file = $_FILES["banner"];
                        $md5 = "uploads/event/banner/" . md5($easy_name) . ".jpg";
                        $libelle = "Photo de couverture de " . addslashes($pseudo);
                        $libelle = utf8_encode($libelle);

                        if (move_uploaded_file($_FILES['banner']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            if(isset($_POST["id_banner"]) &&  $_POST["id_banner"] > 1){
                                $id_banner = $_POST["id_banner"];
                                $sql = "UPDATE media SET lien='$md5', libelle='$libelle' WHERE id=$id_banner;";
                                $query = $pdo->prepare($sql);
                                $query->execute();
                            }else{
                                $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('2', '$md5', '$libelle')";
                                $query = $pdo->prepare($sql);
                                $query->execute();

                                $sql = "SELECT id FROM media WHERE lien='$md5';";
                                $query = $pdo->query($sql);
                                if($query){
                                    $row = $query->fetch(PDO::FETCH_ASSOC);
                                    if($row){
                                        $artist->set_logo($row["id"]);
                                    }else{
                                        $artist->set_logo(1);
                                    }
                                }else{
                                    $artist->set_logo(1);
                                }

                            }

                        }
                    }
                    $v->assign("edit_link",false);
                    $artist->save();
                    $v->assign("msg_valid","Lieu mis à jour.");
                }else{
                    $v->assign("msg_valid","Lieu créé.");
                    if (!empty($_FILES["photo"])) {
                        $file = $_FILES["photo"];
                        $md5 = "uploads/event/profile/" . md5($easy_name) . ".jpg";
                        global $pdo;
                        $libelle = "Photo de profil de " . $pseudo;
                        if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('1', '$md5', '$libelle')";
                            $query = $pdo->prepare($sql);
                            $query->execute();
                            $sql = "SELECT id FROM media WHERE lien='$md5';";
                            $query = $pdo->query($sql);
                            if($query){
                                $row = $query->fetch(PDO::FETCH_ASSOC);
                                if($row){
                                    $artist->set_logo($row["id"]);
                                }
                            }
                        }
                    }

                    if (!empty($_FILES["banner"])) {
                        $file = $_FILES["banner"];
                        $md5 = "uploads/event/banner/" . md5($easy_name) . ".jpg";
                        global $pdo;
                        $libelle = "Photo de couverture de " . $pseudo;
                        if (move_uploaded_file($_FILES['banner']['tmp_name'], BASE_URL_FRONT . $md5)) {
                            $sql = "INSERT INTO media (id_type, lien, libelle) VALUES ('2', '$md5', '$libelle')";
                            $query = $pdo->prepare($sql);
                            $query->execute();
                            $sql = "SELECT id FROM media WHERE lien='$md5';";
                            $query = $pdo->query($sql);
                            if($query){
                                $row = $query->fetch(PDO::FETCH_ASSOC);
                                if($row){
                                    $artist->set_logo($row["id"]);
                                }
                            }
                        }
                    }
                    $artist->save();
                    $new_id = artist_exists($easy_name);
                    $link = 'event?id=' . $new_id;
                    $v->assign("edit_link",$link);
                }

            }else{
                $v->assign("msg_error",$msg_error);
            }
        }
    }

    private function get($v, $new_artist){
        $artistes = [];
        if(!$new_artist){
            $user_id = $_GET["id"];
            $v->assign("message_user","Modifier le lieu");

            try {
                $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
                $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

                // On garde met les notes_mark_read(database_name, user_name, note_id)s des attributs de la classe enfant dans $columns
                $sql = "SELECT u.*, m.lien as photo, m.lien as banner, m.libelle as alt
                FROM lieu u, media m
                WHERE u.id=$user_id
                AND u.logo = m.id;";
                $stmt = $this->pdo->query($sql);
                if($stmt){
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    if(!empty($row)){
                        foreach ($row as $key => $line) {
                            if($key == 'artistes'){
                                $artistes_event = explode(',', trim($line, ',') );
                                $v->assign('artistes_event', $artistes_event);
                            }else{
                                $v->assign($key, $line);
                            }

                    }

                    }
                }
            } catch (Exception $e) {
                die("Erreur SQL : " . $e->getMessage());
            }

        }else{
            $v->assign("message_user","Nouveau lieu");
            $v->assign("new_user",true);
        }
        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

            $sql = "SELECT * FROM artiste ORDER BY pseudo;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $artistes[] = $row;
                }
            }

        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }

        $v->assign("artistes",$artistes);
    }
}

