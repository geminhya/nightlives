
<?php


class commentController{

    public function indexAction($args){
        global $pdo, $home_url;
        $v = new view();
        $v->setView("comment");
        $comment_id = $_GET['id'];
        $sql = "SELECT * FROM commentaire WHERE id = '$comment_id';";
        $stmt = $pdo->query($sql);
        $is_valid = false;
        if($stmt){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if(!empty($row) && !empty($row['id'])){
                $comment = $row;
                $v->assign("comment",$comment);
                $element = [];
                $type_name = "Element";
                if($comment['type_element'] == 0){
                    // Evenement
                    $type_name = "Evenemenet";
                    $element_id = $comment['id_element'];
                    $sql = "SELECT id, libelle, easy_name FROM evenement WHERE id = '$element_id';";
                    $stmt = $pdo->query($sql);
                    if($stmt){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if(!empty($row) && !empty($row['id'])){
                            $element = $row;
                            $element['name'] = $element['libelle'];
                            $element['link'] = FRONT_URL . "event/" . $element['easy_name'];
                        }
                    }
                }elseif($comment['type_element'] == 1){
                    // Evenement
                    $type_name = "Artiste";
                    $element_id = $comment['id_element'];
                    $element = [];
                    $sql = "SELECT id, pseudo, easy_name FROM artiste WHERE id = '$element_id';";
                    $stmt = $pdo->query($sql);
                    if($stmt){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if(!empty($row) && !empty($row['id'])){
                            $element = $row;
                            $element['link'] = FRONT_URL . "artiste/" . $element['easy_name'];
                            $element['name'] = $element['pseudo'];
                        }
                    }
                }elseif($comment['type_element'] == 2){
                    // Evenement
                    $type_name = "Article";
                    $element_id = $comment['id_element'];
                    $element = [];
                    $sql = "SELECT id, titre_article FROM article WHERE id = '$element_id';";
                    $stmt = $pdo->query($sql);
                    if($stmt){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if(!empty($row) && !empty($row['id'])){
                            $element = $row;
                            $element['link'] = FRONT_URL . "articles";
                            $element['name'] = $element['titre_article'];
                        }
                    }
                }
                $v->assign("type_name",$type_name);
                $v->assign("element",$element);

                if(!empty($_POST['valid_comment'])){
                    $sql = "UPDATE commentaire SET valid = 1 WHERE id = $comment_id";
                    $query = $pdo->prepare($sql);
                    $query->execute();
                    $is_valid = true;
                }
            }else{
                header("Location: " . ADMIN_URL);
            }
            $v->assign("is_valid",$is_valid);
        }


    }
}

