<?php

/**
*
*/
class user extends Base_SQL
{
    protected $id;
    protected $nom;
    protected $prenom;
    protected $ville;
    protected $email;
    protected $pseudo;
    protected $id_media;
    protected $mdp;
    protected $status;
    protected $archive;

    public function __construct()
    {
        parent::__construct();
    }

    // SETTERS


    public function set_nom($data){
        $this->nom = trim($data);
    }
    public function set_id($data){
        $this->id = trim($data);
    }
    public function set_archive($data){
        $this->archive = trim($data);
    }

    public function set_prenom($data){
        $this->prenom = trim($data);
    }

    public function set_ville($data){
        $this->ville = trim($data);
    }

    public function set_email($data){
        $this->email = trim($data);
    }

    public function set_pseudo($data){
        $this->pseudo = trim($data);
    }

    public function set_mdp($data){
        $this->mdp= $data;
    }

    public function set_status($data){
      $this->status = $data;
    }

    public function set_id_media($data){
      $this->id_media = $data;
    }

    // GETTERS

    public function get_id($title){
        return $this->id;
    }

    public function get_nom($title){
        return $this->nom;
    }

    public function get_prenom($content){
        return $this->prenom;
    }

    public function get_email($content){
        return $this->email;
    }

    public function get_status($content){
        return $this->status;
    }

    public function get_id_media($content){
        return $this->id_media;
    }
    
    
}
