<?php

/**
* 
*/
class artiste extends Base_SQL
{
    protected $id, $pseudo, $biographie, $id_type_musique, $status, $easy_name, $id_photo, $id_banner, $iframe, $pays, $archive;
    protected $facebook, $twitter, $youtube, $soundcloud, $instagram;
    
    public function __construct()
    {
        parent::__construct();
    }

    // SETTERS

    public function set_id($id){
        $this->id = $id;
    }

    public function set_pseudo($data){
        $this->pseudo = trim($data);
    }

    public function set_biographie($data){
        $this->biographie = trim($data);
    }

    public function set_id_type_musique($data){
        $this->id_type_musique = trim($data);
    }
    public function set_archive($data){
        $this->archive = trim($data);
    }

    public function set_status($data){
        $this->status = trim($data);
    }

    public function set_id_photo($data){
        $this->id_photo = trim($data);
    }

    public function set_id_banner($data){
        $this->id_banner = trim($data);
    }

    public function set_easy_name($data){
        $this->easy_name = trim($data);
    }

    public function set_iframe($data){
        $this->iframe = trim($data);
    }

    public function set_facebook($data){
        $this->facebook = trim($data);
    }

    public function set_twitter($data){
        $this->twitter = trim($data);
    }

    public function set_youtube($data){
        $this->youtube = trim($data);
    }

    public function set_instagram($data){
        $this->instagram = trim($data);
    }

    public function set_soundcloud($data){
        $this->soundcloud = trim($data);
    }

    public function set_pays($data){
        $this->pays = trim($data);
    }

    // GETTERS

    public function get_id($id){
        return $this->id;
    }

    public function get_pseudo($title){
        return $this->pseudo;
    }

    public function get_biographie($content){
        return $this->biographie;
    }

    public function get_id_type_musique($content){
        return $this->id_type_musique;
    }
}
