<?php
class commentaire extends Base_SQL{
    protected $id, $id_user, $id_article, $contenu, $date_ecriture, $type_commentaire;

    public function __construct()
    {
        parent::__construct();
    }

    public function set_Id($data){
        $this->id = $data;
    }
    public function set_id_user($data){
        $this->id_user = $data;
    }
    public function set_id_article($data){
        $this->id_article = $data;
    }
    public function set_contenu($data){
        $this->contenu = $data;
    }
    public function set_date_ecriture($data){
        $this->date_ecriture = $data;
    }
    public function set_type_commentaire($data){
        $this->type_commentaire = $data;
    }
    function getId() {
        return $this->id;
    }

    function getId_user() {
        return $this->id_user;
    }

    function getId_article() {
        return $this->id_article;
    }

    function getContenu() {
        return $this->contenu;
    }

    function getDate_ecriture() {
        return $this->date_ecriture;
    }

    function getType_commentaire() {
        return $this->type_commentaire;
    }


}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

