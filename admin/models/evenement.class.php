<?php

/**
* CLASSE EVENEMENT
*/
class evenement extends Base_SQL
{
    protected $id;
    protected $id_type_event;
    protected $libelle;
    protected $artistes, $id_lieu, $date_deb, $date_fin, $id_media, $informations, $easy_name, $link_buy, $link_more;
    public    $id_photo, $id_banner;
    protected $archive;
    
    public function __construct()
    {
        parent::__construct();
    }

    // SETTERS

    public function set_id($id){
        $this->id = $id;
    }
    public function set_archive($data){
        $this->archive = trim($data);
    }

    public function set_id_type_event($data){
        $this->id_type_event = trim($data);
    }

    public function set_libelle($data){
        $this->libelle = trim($data);
    }

    public function set_informations($data){
        $this->informations = trim($data);
    }

    public function set_id_lieu($data){
        $this->id_lieu = trim($data);
    }

    public function set_date_deb($data){
        $this->date_deb = trim($data);
    }

    public function set_date_fin($data){
        $this->date_fin = trim($data);
    }

    public function set_id_photo($data){
        $this->id_photo = trim($data);
    }

    public function set_id_banner($data){
        $this->id_banner = trim($data);
    }

    public function set_priorite($data){
        $this->priorite = trim($data);
    }

    public function set_easy_name($data){
        $this->easy_name = trim($data);
    }

    public function set_status($data){
        $this->status = trim($data);
    }

    public function set_artistes($data){
        $this->artistes = trim($data);
    }

    public function set_url_tickets($data){
        $this->link_buy = trim($data);
    }

    public function set_link_more($data){
        $this->link_more = trim($data);
    }

    public function set_region($data){
        $this->region = trim($data);
    }

    public function set_ville($data){
        $this->ville = trim($data);
    }

    // GETTERS

    public function get_id($id){
        return $this->id;
    }

    public function get_id_type_evenement($title){
        return $this->id_type_evenement;
    }

    public function get_libelle($title){
        return $this->libelle;
    }

    public function get_id_artiste($title){
        return $this->artistes;
    }

    public function get_id_lieu($title){
        return $this->id_lieu;
    }

    public function get_date_deb($title){
        return $this->date_deb;
    }

    public function get_date_fin($title){
        return $this->date_fin;
    }

    public function get_id_media($title){
        return $this->id_media;
    }

    public function get_id_photo(){
        return $this->id_photo;
    }

    public function get_priorite($title){
        return $this->priorite;
    }
}
