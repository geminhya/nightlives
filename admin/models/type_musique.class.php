<?php

/**
* 
*/
class type_musique extends Base_SQL
{
    protected $id, $libelle;
    
    public function __construct()
    {
        parent::__construct();
    }

    // SETTERS

    public function set_id($id){
        $this->id = $id;
    }

    public function set_libelle($data){
        $this->libelle = trim($data);
    }

    // GETTERS

    public function get_id($id){
        return $this->id;
    }

    public function get_libelle($title){
        return $this->libelle;
    }
}
