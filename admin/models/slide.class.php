<?php

/**
* 
*/
class slide extends Base_SQL
{
    protected $id, $id_media, $title, $subtitle, $lien, $archive;
    
    public function __construct()
    {
        parent::__construct();
    }

    // SETTERS

    public function set_id($id){
        $this->id = $id;
    }
    public function set_archive($data){
        $this->archive = trim($data);
    }

    public function set_id_media($data){
        $this->id_media = trim($data);
    }

    public function set_title($data){
        $this->title = trim($data);
    }

    public function set_subtitle($data){
        $this->subtitle = trim($data);
    }

    public function set_lien($data){
        $this->lien = trim($data);
    }
}
