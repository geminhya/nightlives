<?php

/**
 *
 */
class Base_SQL {

    private $table;
    private $pdo;
    private $columns = [];

    public function __construct() {
        $this->table = get_called_class();
        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

            // On garde met les noms des attributs de la classe enfant dans $columns
            $cols = get_object_vars($this);
            $class_vars = get_class_vars(get_class());
            $this->columns = array_keys(array_diff_key($cols, $class_vars));
        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }
    }

    public function save() {
        if (is_numeric($this->id)) {
            $cols = $this->columns;
            unset($cols[0]);
            $sets = "";
            $cpt = 1;

            foreach ($cols as $key => $value) {
                $sets .= $value . '="' . $this->$value . '"';
                if ($cpt < sizeof($cols)) {
                    $sets .= ',';
                }
                $cpt++;
            }
//            var_dump($this->table);
//            var_dump($sets);
//            die();
            $sql = "UPDATE " . strtolower($this->table) . " SET " . $sets . " WHERE id=" . strtolower($this->id);
            $query = $this->pdo->prepare($sql);

            foreach ($this->columns as $key => $value) {
                $data[$value] = $this->$value;
            }
            $result = $query->execute($data);
        } else {

            $cols = $this->columns;
            unset($cols[0]);
            $values_line = "";
            foreach ($cols as $key => $value) {
                $values_line .= '"' . $this->$value . '",';
            }
            $values_line = trim($values_line, ',');
            $sql = 'INSERT INTO ' . strtolower($this->table) . ' (' . implode(',', $cols) . ') VALUES (' . $values_line . ');';
            $query = $this->pdo->prepare($sql);
//            print_r($query);
//            die();
            $result = $query->execute();
        }
    }

    public function get_element_by_id($table, $id) {
        try {
            $sql = "SELECT * FROM $table WHERE id=$id";


            $stmt = $this->pdo->query($sql);
            if ($stmt) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    return $row;
                }
            }
        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }
    }

}
