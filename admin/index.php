<?php

require_once "conf.inc.php";
require_once "functions.php";

function autoloader($class) {
    $class = strtolower($class);
    // Vérifie s'il éxiste dans le dossier core un fichier "$class.class.php
    // Si oui, alors include

    if (file_exists("core/" . $class . ".class.php")) {
        include BASE_URL . "core/" . $class . ".class.php";
    }elseif (file_exists("models/" . $class . ".class.php")) {
        include BASE_URL . "models/" . $class . ".class.php";
    }
}

spl_autoload_register('autoloader');

$route = routing::setRouting();


$name_controller = $route["c"] . "Controller";
$path_controller = BASE_URL . "controllers/" . $name_controller . ".class.php";
// A AJOUTER : SI NON ADMIN, ON DEGAGE !
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if($route["c"] != 'login' && !is_logged_in()){
    $url = "http://" . $_SERVER['HTTP_HOST'] . ADMIN_URL . "/login";
    header("Location: " . $url);
    exit;
}

if(file_exists($path_controller)){
    include $path_controller;
    
    $controller = new $name_controller;
    $name_action = $route["a"] . "Action";
    if(method_exists($controller, $name_action)){
        //$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        //save_page_count($actual_link);
    	$controller->$name_action($route["args"]);
        // ok ça roule !
    }else{
    	header("Location: " . ADMIN_URL . "/error");
        exit;
    }
}else{
    header("Location: " . ADMIN_URL . "/error");
    exit;
}
