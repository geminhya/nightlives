
jQuery(document).ready(function() {
	jQuery("input.autocomplete_input").keyup(function(){
		var value = jQuery(this).val();
		var autcomplete_list = jQuery(this).parent().find(".autcomplete_list");

		if (value.length > 0) {
			autcomplete_list.show();
			$.ajax({
	            method: "POST",
	            url: "/nightlives/admin/autocomplete/artistes",
	            data: { autocomplete_input: value }
	        })
            .done(function (data) {
            	data = jQuery.parseJSON(data);
            	var total = "";
                for (var i = 0; i < data.length; i++) {
                	total += "<div ";
                	total += "data-id='" + data[i].id + "' class='autcomplete_list_element' >";
                	total += data[i].pseudo;
                	total += "</div>";
                }
                autcomplete_list.html(total);
                autocomplete_item_click("artistes");
                autocomplete_item_delete("artistes");
            });
		}else{
			autcomplete_list.hide();
		}
	});

	autocomplete_item_click("artistes");
    autocomplete_item_delete("artistes");

});


function autocomplete_item_click(items){
	jQuery(".autocomplete_" + items + " .autcomplete_list_element").click(function(){
		var element_id = jQuery(this).data('id');
		var temp_html = jQuery(".autocomplete_valid").html();
		// on ajoute le bouton de l'artiste ajouté
		temp_html += "<span class='autocomplete_valid_span' id='id_" + items + "_" + element_id + "' data-id='" + element_id + "'>" + jQuery(this).text() + "</span>";
		jQuery(".autocomplete_" + items + " .autocomplete_valid").html(temp_html);
		jQuery(".autocomplete_" + items + " .autocomplete_valid").show();
		// on vide le champ texte
		jQuery(".autocomplete_" + items + " .autocomplete_input").val("");
		// on click su l'input
		jQuery(".autocomplete_" + items + " ." + items + "_hidden_" + element_id).click();
		jQuery(".autocomplete_" + items + " .autcomplete_list").hide();

		autocomplete_item_delete(items);
	});
}

function autocomplete_item_delete(items){
	console.log(".autocomplete_" + items + " span.autocomplete_valid_span");
	console.log( jQuery(".autocomplete_" + items + " span.autocomplete_valid_span").length );
	jQuery(".autocomplete_" + items + " span.autocomplete_valid_span").click(function(){
		var element_id = jQuery(this).data('id');
		console.log(".autocomplete_" + items + " .listing_artistes ." + items + "_hidden_" + element_id);
		jQuery(".autocomplete_" + items + " .listing_artistes ." + items + "_hidden_" + element_id).click();
		jQuery(".autocomplete_" + items + " span#id_" + items + "_" + element_id).remove();
		jQuery(".autocomplete_" + items + " .autcomplete_list").hide();
		if(jQuery(".autocomplete_" + items + " span.autocomplete_valid_span").length == 0){
			jQuery(".autocomplete_" + items + " .autocomplete_valid").hide();
		}
	});

}
