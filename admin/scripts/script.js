jQuery(document).ready(function() {
	titlePercentage();


	to_crop_h();

	auto_sumbit();
});


function titlePercentage() {
    jQuery(".percent").each(function() {
        $(this).attr("title",$(this).html());
    });
}


function to_crop_h(){
	if(jQuery(".to_crop_h").length > 0){
		jQuery(".to_crop_h").each(function(){
			jQuery(this).bind('load', function() {
			 	var img_width = jQuery(this).width();
				var parent_width = jQuery(this).parent().width();
				if(img_width > parent_width){
					var result = "-" + ( (img_width - parent_width) / 2 ) + "px";
					jQuery(this).css('margin-left', result );
				}
			});
		});
	}
}

function auto_sumbit(){
	jQuery("form.auto_sumbit select").change(function(){
		jQuery(this).parent().submit();
	});
}
