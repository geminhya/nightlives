<?php

class articlesController {

    public function indexAction($args) {
        $v = new view();
        $v->setView("articles");

        $pp_noemie = getProfilePicture(22);
        $pp_michael = getProfilePicture(72);
        $pp_nadir = getProfilePicture(73);
        $pp_momo = getProfilePicture(74);
        $v->assign("pp_noemie", $pp_noemie);
        $v->assign("pp_michael", $pp_michael);
        $v->assign("pp_nadir", $pp_nadir);
        $v->assign("pp_momo", $pp_momo);
        //-------Pagination--------
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $current_url = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        
        $v->assign("current_url", $current_url);
        $nb_per_page = 5;
        $start = 0;
        if (!empty($_GET['page'])) {
            $start += (intval($_GET['page']) - 1) * $nb_per_page;
        }else{
            
        }
        $v->assign("start", $start);
        $next_page = 2;
        $prev_page = 0;
        if ($start > 0) {
            $next_page = intval($_GET['page']) + 1;
            $prev_page = intval($_GET['page']) - 1;
        }
        $v->assign("next_page", $next_page);
        $v->assign("prev_page", $prev_page);
        $v->assign("current_page", (!empty($_GET['page'])) ? $_GET['page'] : 1 );
        //-------Fin-pagination--------
        if (!empty($_GET['id_redacteur'])) {
            $v->assign("id_redac", $_GET['id_redacteur']);
            $articles = getArticles_redacteur($_GET['id_redacteur'], $start, $nb_per_page);
        } else {
            $articles = getArticles($start, $nb_per_page);
        }
        $commentaires = getCommentaire();
        $v->assign("articles", $articles);
        $v->assign("commentaires", $commentaires);
        if (!empty($_POST['blocCommentaire'])) {
            $commentaire = new commentaire();
            $commentaire->set_id_user($_SESSION['user']['id']);
            $commentaire->set_contenu(htmlentities($_POST['blocCommentaire'],ENT_HTML5, 'UTF-8'));
            $commentaire->set_id_element(htmlentities($_POST['id_article']));
            $commentaire->set_date_comment(date("Y-m-d", strtotime("now")));
            $commentaire->set_type_element(2);
            $commentaire->set_valid(0);
            $commentaire->save();
            header("Refresh:0");
        }
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
        if (!empty($_GET['id_redacteur'])) {
            $sql = "SELECT COUNT(DISTINCT(id)) as total FROM article WHERE archive = 0 AND id_redacteur = " . $_GET['id_redacteur'] . ";";
        } else {
            $sql = "SELECT COUNT(DISTINCT(id)) as total FROM article WHERE archive = 0 ;";
        }
        $stmt = $this->pdo->query($sql);
        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $total_elements = $row['total'];
            }
        }
        $v->assign("total_elements", $total_elements);
        $v->assign("pages", ceil($total_elements / $nb_per_page));
    }

}
