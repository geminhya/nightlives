<?php


class soireesController{

    public function indexAction($args){


        $v = new view();

        //-------Filter------------
        $type = 0;
        $conditions = "";
        $v->assign("head_title","");

        if(!empty($_GET['type']) && $_GET['type'] != 'all'){
            if($_GET['type'] == 'electro'){
                $type = 1;
                $v->assign("title","Soiree Electro");
                $v->assign("head_title","- Electro");
            }elseif($_GET['type'] == 'pop'){
                $type = 2;
                $v->assign("title","Soiree Pop");
                $v->assign("head_title","- Pop");
            }else{
                $type = 3;
                $v->assign("title","Soiree Rock");
                $v->assign("head_title","- Rock");
            }
            $conditions .= " AND u.id_type_evenement = $type";
        }
        //Recherche
        if(!empty($_GET['text'])){
            $v->assign("subtitle",'Résultats de recherche pour "' . trim($_GET['text']) . '"' );
            $conditions .= " AND u.libelle LIKE '%" . trim($_GET['text']) . "%'";
        }
        $v->assign("type",$type);
        //-------Fin filter--------

        //-------Pagination--------
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $current_url = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        $v->assign("current_url",$current_url);
        $nb_per_page = 6;
        $start = 0;
        if(!empty($_GET['page'])){
            $start += (intval($_GET['page']) - 1) * $nb_per_page;
        }
        $v->assign("start",$start);
        $next_page = 2;
        $prev_page = 0;
        if ($start > 0) {
            $next_page = intval($_GET['page']) + 1;
            $prev_page = intval($_GET['page']) - 1;
        }
        $v->assign("next_page",$next_page);
        $v->assign("prev_page",$prev_page);
        $v->assign("current_page", (!empty($_GET['page'])) ? $_GET['page'] : 1 );
        //-------Fin-pagination--------

        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
            $events = [];

            if(!empty($_POST) && !empty(trim($_POST['input_user']))){
                $events = $this->post($v);
            }else{
                $type_event = 3;
                $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt
                FROM evenement u, media mp, media mb
                WHERE u.id_type_event = $type_event
                AND u.id_photo = mp.id
                AND u.archive = 0
                AND u.id_banner = mb.id" .$conditions ."
                ORDER BY id DESC LIMIT $start,$nb_per_page";

                $stmt = $this->pdo->query($sql);
                if($stmt){
                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                        $events[] = $row;
                    }
                }
            }

            //Récupération du nombre totlal des evenement
             $sql = "SELECT COUNT(DISTINCT(id)) as total FROM evenement WHERE id_type_event = 2;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $total_elements = $row['total'];
                }
            }
            $v->assign("total_elements",$total_elements);
            $v->assign("pages", ceil($total_elements / $nb_per_page));


        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }


        $v->setView("soirees");
        $v->assign("elements",$events);

        if(!empty($_POST) && !empty(trim($_POST['input_user']))){
            $events = $this->post();
        }
    }
    
    public function testAction($args){
        echo "bonjour";
    }

    public function post(){
        $events = [];
        if(!empty(trim($_POST['input_user']))){
            $input_user = trim($_POST['input_user']);
            $sql = "SELECT u.*, m.*, m.lien as photo, m.libelle as alt
            FROM user u, media m
            WHERE (pseudo LIKE '$input_user'
            OR nom LIKE '$input_user'
            OR prenom LIKE '$input_user'
            OR ville LIKE '$input_user'
            OR email LIKE '$input_user')
            AND u.id_media = m.id;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $events[] = $row;
                }
            }

         return $events;
        }
    }
}


