<?php

class artisteController{
    
    public function indexAction($args){


    	if(empty($_GET['id']) && empty($_GET['name'])){
            header("Location: " . FRONT_URL);
        }

        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        //$result = save_page_count($actual_link, "Artist: Nom de l'artiste", 'artist');

        $v = new view();
        $v->setView("artiste");


        if(!empty($_GET["id"])){
            $user_id = $_GET["id"];
        }

        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);


            if (!empty($_GET["name"]) && empty($_GET['id'])) {
                $post_name = $_GET["name"];

                $sql = "SELECT id FROM artiste WHERE easy_name = '$post_name';";
                $stmt = $this->pdo->query($sql);
                if($stmt){
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    if(!empty($row) && !empty($row['id'])){
                        $user_id = $row['id'];
                    }else{
                        header("Location: " . FRONT_URL);
                    }
                }
            }
            $artist = [];
            // On garde met les notes_mark_read(database_name, user_name, note_id)s des attributs de la classe enfant dans $columns
            $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt
            FROM artiste u, media mp, media mb
            WHERE u.id=$user_id
            AND u.id_photo = mp.id
            AND u.id_banner = mb.id;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if(!empty($row)){
                    foreach ($row as $key => $line) {
                        $artist[$key] = $line;
                        $v->assign($key, $line);
                        if($key == 'pseudo'){
                            save_page_count($actual_link, $line, 'artist');
                            $v->assign($key, $line);
                        }
                    }
                }
            }
            //var_dump($artist);
            $v->assign("head_title", utf8_encode($artist['pseudo']) . " - ");

            $events = [];
            $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt FROM evenement u, media mp, media mb WHERE u.id_photo = mp.id AND u.id_banner = mb.id AND u.artistes LIKE '%," . $user_id . ",%';";
            $stmt = $this->pdo->query($sql);
            $count = 0;
            if($stmt){
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    if(!empty($row)){
                        foreach ($row as $key => $line) {
                            $events[$count][$key] = $line;
                        }
                        $count++;
                    }
                }
            }
            $v->assign('events', $events);

            //Recuperation des fans
            $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt
            FROM artiste u, media mp, media mb
            WHERE u.id=$user_id
            AND u.id_photo = mp.id
            AND u.id_banner = mb.id;";
            $stmt = $this->pdo->query($sql);
            $participants_event = [];
            if($stmt){
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if(!empty($row)){
                    foreach ($row as $key => $line) {
                        if($key == 'fans'){
                            $participants_event = explode(',', trim($line) );
                            //Virer les cases vides du debut et de la fin(virgule du debut et de fin)
                            $participants_event = array_filter($participants_event);
                            $v->assign('participants_event', $participants_event);
                        }else{
                            $v->assign($key, $line);
                        }
                    }
                }
            }

            if(!empty($participants_event)){
                $participants_results = [];
                $count = 0;
                foreach ($participants_event as $key => $participant) {
                    $sql = "SELECT u.pseudo, u.id, mp.lien as photo
                    FROM user u, media mp
                    WHERE u.id=$participant
                    AND u.id_media = mp.id;";
                    $stmt = $this->pdo->query($sql);
                    if($stmt){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if(!empty($row)){
                            foreach ($row as $key => $line) {
                                $participants_results[$count][$key] = $line;
                            }
                        }

                    }
                    $count++;
                }
                $v->assign('participants_results', $participants_results);
                $v->assign('count', $count);
            }

            //Gestion du bouton "fan"
            if (!empty($_SESSION['user'])){
                $user = [];
                $sql = "SELECT * FROM user where id = ".$_SESSION['user']['id'];
                $stmt = $this->pdo->query($sql);
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $user[] = $row;
                }

                $already_in = 0;
                if (!empty($participants_results)){
                    foreach ($participants_results as $value){
                        if ($value['id'] == $_SESSION['user']['id']){
                            $already_in = 1;
                        }
                    }
                }


                if($already_in == 0){
                    $participate_button = '<a id="bouton_fan" class="call_to long" data-userid="'. $_SESSION['user']['id']. '" data-eventid="'. $user_id. '" href="#">Je suis fan moi aussi !</a>';
                }
                if($already_in == 1){
                    $participate_button = '<a id="bouton_nonfan" class="call_to long" data-userid="'. $_SESSION['user']['id']. '" data-eventid="'. $user_id. '" href="#">Je ne suis plus fan !</a>';
                }

                $v->assign('user', $user);
                $v->assign('participate_button', $participate_button);
            }

    //Recuperation des commentaires
            //Recuperation de la chaine de caractere contenant les id des commentaires
            $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt
            FROM artiste u, media mp, media mb
            WHERE u.id=$user_id
            AND u.id_photo = mp.id
            AND u.id_banner = mb.id;";
            $stmt = $this->pdo->query($sql);
            $commentaires_event = [];
            if($stmt){
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if(!empty($row)){
                    foreach ($row as $key => $line) {
                        if($key == 'commentaires'){
                            //Virer les virgules et mettre dans un tab
                            $commentaires_event = explode(',', trim($line) );
                            //Virer les cases vides du debut et de la fin(virgule du debut et de fin)
                            $commentaires_event = array_filter($commentaires_event);
                            $v->assign('commentaires_event', $commentaires_event);
                        }else{
                            $v->assign($key, $line);
                        }
                    }
                }
            }

            //Recupérer pour chaque id le commentaire correspondant
            if(!empty($commentaires_event)){
                $commentaires_results = [];
                $count = 0;
                foreach ($commentaires_event as $key => $commentaire) {
                    $sql = "SELECT c.*, u.pseudo, u.id, mp.lien as photo
                    FROM commentaire c, user u, media mp
                    WHERE c.id=$commentaire
                    AND u.id = c.id_user
                    AND u.id_media = mp.id;";
                    $stmt = $this->pdo->query($sql);
                    if($stmt){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if(!empty($row)){
                            foreach ($row as $key => $line) {
                                $commentaires_results[$count][$key] = $line;
                            }
                        }

                    }
                    $count++;
                }
                $v->assign('commentaires_results', $commentaires_results);
            }

            //Gestion du bouton commentaire

            if (!empty($_SESSION['user'])){
                $user = [];
                $sql = "SELECT * FROM user where id = ".$_SESSION['user']['id'];
                $stmt = $this->pdo->query($sql);
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $user[] = $row;
                }
                //Bouton commenter
                $commente_button = '<a id="bouton_fun_commentaire" class="call_to" data-userid="'.$_SESSION['user']['id'].'" data-eventid="'. $user_id.'" href="#">Envoyer !</a>';

                $v->assign('user', $user);
                $v->assign('commente_button', $commente_button);

            }

        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }
    }

    public function participeAction($args) {
        $id_participant = $_POST["id_participant"];
        $event_id = $_POST["id_evenement"];
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
        //Si la case fans est vide on insere une virgule
        $sql = "SELECT fans FROM artiste WHERE id=$event_id";
        $query = $pdo->query($sql);
        if($query){
            $row = $query->fetch(PDO::FETCH_ASSOC);
            if($row){
                $participants =  $row["fans"];
            }
        }
        if ($participants == ''){
            $sql = "UPDATE artiste SET fans = ',' WHERE id=$event_id";
            $query = $pdo->prepare($sql);
            $query->execute();
        }
        //Inserer l'id de l'utilisateur
        $sql = "UPDATE artiste SET fans = CONCAT(fans,'$id_participant,') WHERE id=$event_id";
        $query = $pdo->prepare($sql);
        $query->execute();
    }

     //Bouton pour ne plus etre fan
    public function unparticipeAction($args) {
        $id_participant = $_POST["id_participant"];
        $event_id = $_POST["id_evenement"];
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
        //recuperation des participants
        $sql = "SELECT fans FROM artiste WHERE id=$event_id";
        $query = $pdo->query($sql);
        if($query){
            $row = $query->fetch(PDO::FETCH_ASSOC);
            if($row){
                $participants =  $row["fans"];
            }
        }
        //Enlever le participant et casser le tableau en 2
        $exp = explode(',' . $id_participant . ',', $participants);
        //On concatene les deux tableau en string avec une virgule
        $new_participants = $exp[0] . ',' . $exp[1];
        //Inserer la nouvelle chaine de caractere
        $sql = "UPDATE artiste SET fans='$new_participants' WHERE id=$event_id";
        $query = $pdo->prepare($sql);
        $query->execute();
    }

    //Bouton pour commenter
    public function commentaireAction($args) {
        $id_user = $_POST["id_user"];
        $event_id = $_POST["id_evenement"];
        //htmlspecialchars pour ne pas gérer les script
        //utf8_decode pour convertir en ISO
        $commentaire = htmlspecialchars(utf8_decode($_POST["commentaire"]));
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

        //Inserer le commentaire dans la table commentaire
        if (!empty($commentaire)){
            $sql = "INSERT INTO commentaire (contenu, id_user, id_element, type_element, date_comment) VALUES (\"$commentaire\", $id_user, $event_id, 1, NOW())";
            $query = $pdo->prepare($sql);
            $query->execute();

            //Recupérer l'id du commentaire insérer
            $id_commentaire = $pdo->lastInsertId();

            ///Si la case commentaires est vide on insere une virgule
            $sql = "SELECT commentaires FROM artiste WHERE id=$event_id";
            $query = $pdo->query($sql);
            if($query){
                $row = $query->fetch(PDO::FETCH_ASSOC);
                if($row){
                    $commentaires =  $row["commentaires"];
                }
            }

            if ($commentaires == ''){
                $sql = "UPDATE artiste SET commentaires = ',' WHERE id=$event_id";
                $query = $pdo->prepare($sql);
                $query->execute();
            }

            //Inserer l'id du commentaire dans l'event
            $sql = "UPDATE artiste SET commentaires = CONCAT(commentaires,'$id_commentaire,') WHERE id=$event_id";
            $query = $pdo->prepare($sql);
            $query->execute();
        }
    }


}

