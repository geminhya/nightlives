<?php

class artistesController{
    
    public function indexAction($args){

        $v = new view();
        $v->setView("artistes");
        $v->assign("title","Artistes");
        $background_url = "images/artistes/artistes_banner_rock.jpg";
        $uri_parts = explode('&', $_SERVER['REQUEST_URI'], 2);
        $actual_link = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        save_page_count($actual_link, "Artistes", 'page');

        //---------Filter--------
        $type = 0;
        $order = "pseudo";
        $conditions = "";
        $v->assign("head_title","Artistes - ");
        
        if(!empty($_GET['type']) && $_GET['type'] != 'all'){
            if($_GET['type'] == 'electro'){
                $type = 1;
                $v->assign("title","Artistes Electro");
                $v->assign("head_title","- Artistes Electro");
                $background_url = "images/artistes/artistes_banner_electro.jpg";
                save_page_count($actual_link, "Artistes : Electro", 'page');
            }elseif($_GET['type'] == 'pop'){
                $type = 2;
                $v->assign("title","Artistes Pop");
                $v->assign("head_title","- Artistes Pop");
                $background_url = "images/artistes/artistes_banner_pop.jpg";
                save_page_count($actual_link, "Artistes : Pop", 'page');
            }else{
                $type = 3;
                $v->assign("title","Artistes Rock");
                $v->assign("head_title","- Artistes Rock");
                save_page_count($actual_link, "Artistes : Rock", 'page');
            }
            $conditions .= " AND u.id_type_musique = $type";
        }else{
            save_page_count($actual_link, "Artistes : Tous", 'page');
        }
        if(!empty($_GET['text'])){
            $v->assign("subtitle",'Résultats de recherche pour "' . trim($_GET['text']) . '"' );
            $conditions .= " AND u.pseudo LIKE '%" . trim($_GET['text']) . "%'";
        }
        if(!empty($_GET['pays']) && $_GET['pays'] != 'all'){
            $conditions .= " AND u.pays LIKE '" . $_GET['pays'] . "'";
        }
        if(!empty($_GET['order'])){
            if($_GET['order'] == 'rate'){
                $order = "rate ASC";
            }elseif($_GET['order'] == 'date'){
                $order = "id DESC";
            }else{
                $order = "pseudo ASC";
            }  
        }
        $conditions .= " ORDER BY " . $order;
        $v->assign("type",$type);
        $v->assign("background_url",$background_url);
        //---------Fin filter-----------


         //-------Pagination--------
        $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $current_url = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        $v->assign("current_url",$current_url);
        $nb_per_page = 12;
        $start = 0;
        if(!empty($_GET['page'])){
            $start += (intval($_GET['page']) - 1) * $nb_per_page;
        }
        $v->assign("start",$start);
        $next_page = 2;
        $prev_page = 0;
        if ($start > 0) {
            $next_page = intval($_GET['page']) + 1;
            $prev_page = intval($_GET['page']) - 1;
        }
        $v->assign("next_page",$next_page);
        $v->assign("prev_page",$prev_page);
        $v->assign("current_page", (!empty($_GET['page'])) ? $_GET['page'] : 1 );
        //-------Fin-pagination--------

        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
            
            $elements = [];
            $sql = "SELECT u.pseudo, u.id, mp.lien as photo, u.easy_name
                    FROM artiste u, media mp
                    WHERE u.archive = 0 AND u.id_photo = mp.id " . $conditions ."
                    LIMIT $start,$nb_per_page;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $elements[] = $row;
                }
            }

            $v->assign("elements", $elements);

            $select_pays = [];
            $sql = "SELECT DISTINCT(pays) FROM artiste WHERE archive = 0 AND pays IS NOT NULL ORDER BY pays;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $select_pays[] = $row;
                }
            }
            $v->assign("select_pays", $select_pays);
            
            //Récupération du nombre totlal des evenement
             $sql = "SELECT COUNT(DISTINCT(id)) as total FROM artiste WHERE archive = 0;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $total_elements = $row['total'];
                }
            }
            $v->assign("total_elements",$total_elements);
            $v->assign("pages", ceil($total_elements / $nb_per_page));


        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }
    }
    
    public function testAction($args){
        echo "bonjour";
    }
}

