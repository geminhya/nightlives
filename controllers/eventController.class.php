<?php

class eventController{

    public function indexAction($args){

        if(empty($_GET['id']) && empty($_GET['name'])){
            header("Location: " . FRONT_URL);
        }


        $v = new view();
        $v->setView("event");
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        save_page_count($actual_link, "Event : Nom", 'event');

        // Récupération des données de l'evênement
        if(!empty($_GET["id"])){
            $user_id = $_GET["id"];
        }

        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

            if (!empty($_GET["name"]) && empty($_GET['id'])) {
                $post_name = $_GET["name"];

                $sql = "SELECT id FROM evenement WHERE easy_name = '$post_name';";
                $stmt = $this->pdo->query($sql);
                if($stmt){
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    if(!empty($row) && !empty($row['id'])){
                        $user_id = $row['id'];
                    }else{
                        header("Location: " . FRONT_URL);
                    }
                }
            }

            // On garde met les notes_mark_read(database_name, user_name, note_id)s des attributs de la classe enfant dans $columns
            $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt, l.adresse as lieu
            FROM evenement u, media mp, media mb, lieu l
            WHERE u.id=$user_id
            AND u.id_photo = mp.id
            AND u.id_banner = mb.id
            AND u.id_lieu = l.id;";
            $stmt = $this->pdo->query($sql);
            $artistes_event = [];
            if($stmt){
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if(!empty($row)){
                    foreach ($row as $key => $line) {
                        if($key == 'artistes'){
                            $artistes_event = explode(',', trim($line) );
                            $v->assign('artistes_event', $artistes_event);
                        }else{
                            $v->assign($key, $line);
                        }
                    }
                }
            }
            if(!empty($artistes_event)){
                $artistes_results = [];
                $count = 0;
                foreach ($artistes_event as $key => $artist) {
                    $sql = "SELECT u.pseudo, u.id, mp.lien as photo
                    FROM artiste u, media mp
                    WHERE u.id=$artist
                    AND u.id_photo = mp.id;";
                    $stmt = $this->pdo->query($sql);
                    if($stmt){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if(!empty($row)){
                            foreach ($row as $key => $line) {
                                $artistes_results[$count][$key] = $line;
                            }
                        }

                    }
                    $count++;
                }
                $v->assign('artistes_results', $artistes_results);
            }

            //Recuperation des participants
            $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt, l.adresse as lieu
            FROM evenement u, media mp, media mb, lieu l
            WHERE u.id=$user_id
            AND u.id_photo = mp.id
            AND u.id_banner = mb.id
            AND u.id_lieu = l.id;";
            $stmt = $this->pdo->query($sql);
            $participants_event = [];
            if($stmt){
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if(!empty($row)){
                    foreach ($row as $key => $line) {
                        if($key == 'participants'){
                            $participants_event = explode(',', trim($line) );
                            //Virer les cases vides du debut et de la fin
                            $participants_event = array_filter($participants_event);
                            $v->assign('participants_event', $participants_event);
                        }else{
                            $v->assign($key, $line);
                        }
                    }
                }
            }

            if(!empty($participants_event)){
                $participants_results = [];
                $count = 0;
                foreach ($participants_event as $key => $participant) {
                    $sql = "SELECT u.pseudo, u.id, mp.lien as photo
                    FROM user u, media mp
                    WHERE u.id=$participant
                    AND u.id_media = mp.id;";
                    $stmt = $this->pdo->query($sql);
                    if($stmt){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if(!empty($row)){
                            foreach ($row as $key => $line) {
                                $participants_results[$count][$key] = $line;
                            }
                        }

                    }
                    $count++;
                }
                $v->assign('participants_results', $participants_results);
                $v->assign('count', $count);
            }


            //Gestion du bouton "participe"
            if (!empty($_SESSION['user'])){
                $user = [];
                $sql = "SELECT * FROM user where id = ".$_SESSION['user']['id'];
                $stmt = $this->pdo->query($sql);
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $user[] = $row;
                }

                $already_in = 0;
                if (!empty($participants_results)){
                    foreach ($participants_results as $value){
                        if ($value['id'] == $_SESSION['user']['id']){
                            $already_in = 1;
                        }
                    }
                }

                /*$already_in = 0;
                if (!empty($participants_results)){
                    for ($i=0; $i<$count; $i++){
                        if ($participants_results[$i]['id'] == $_SESSION['user']['id']){
                            $already_in = 1;
                            $i = $count;
                        }
                    }
                }*/

                if($already_in == 0){
                    $participate_button = '<a id="bouton_participe" class="call_to long" data-userid="'. $_SESSION['user']['id']. '" data-eventid="'. $user_id . '" href="#">Je participe moi aussi !</a>';
                }
                if($already_in == 1){
                    $participate_button = '<a id="bouton_unparticipe" class="call_to long" data-userid="'. $_SESSION['user']['id']. '" data-eventid="'. $user_id . '" href="#">Je ne participe plus !</a>';
                }

                $v->assign('user', $user);
                $v->assign('participate_button', $participate_button);
            }

            //Recuperation des commentaires
            $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt, l.adresse as lieu
            FROM evenement u, media mp, media mb, lieu l
            WHERE u.id=$user_id
            AND u.id_photo = mp.id
            AND u.id_banner = mb.id
            AND u.id_lieu = l.id;";
            $stmt = $this->pdo->query($sql);
            $commentaires_event = [];
            if($stmt){
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if(!empty($row)){
                    foreach ($row as $key => $line) {
                        if($key == 'commentaires'){
                            $commentaires_event = explode(',', trim($line) );
                            $v->assign('commentaires_event', $commentaires_event);
                        }else{
                            $v->assign($key, $line);
                        }
                    }
                }
            }

            if(!empty($commentaires_event)){
                $commentaires_results = [];
                $count = 0;
                foreach ($commentaires_event as $key => $commentaire) {
                    $sql = "SELECT c.*, u.pseudo, u.id, mp.lien as photo
                    FROM commentaire c, user u, media mp
                    WHERE c.id=$commentaire
                    AND u.id = c.id_user
                    AND u.id_media = mp.id;";
                    $stmt = $this->pdo->query($sql);
                    if($stmt){
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        if(!empty($row)){
                            foreach ($row as $key => $line) {
                                $commentaires_results[$count][$key] = $line;
                            }
                        }

                    }
                    $count++;
                }
                $v->assign('commentaires_results', $commentaires_results);
            }

            //Gestion du bouton commentaire

            if (!empty($_SESSION['user'])){
                $user = [];
                $sql = "SELECT * FROM user where id = ".$_SESSION['user']['id'];
                $stmt = $this->pdo->query($sql);
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $user[] = $row;
                }
                //Bouton commenter
                $commente_button = '<a id="bouton_commentaire" class="call_to" data-userid="'.$_SESSION['user']['id'].'" data-eventid="'. $user_id.'" href="#">Envoyer !</a>';

                $v->assign('user', $user);
                $v->assign('commente_button', $commente_button);

            }


        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }


    }



    public function participeAction($args) {
        $id_participant = $_POST["id_participant"];
        $event_id = $_POST["id_evenement"];
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
        //Si la case participants est vide on insere une virgule
        $sql = "SELECT participants FROM evenement WHERE id=$event_id";
        $query = $pdo->query($sql);
        $participants = '';
        if($query){
            $row = $query->fetch(PDO::FETCH_ASSOC);
            if($row){
                $participants =  $row["participants"];
            }
        }
        if ($participants == ''){
            $sql = "UPDATE evenement SET participants = ',' WHERE id=$event_id";
            $query = $pdo->prepare($sql);
            $query->execute();
        }
        //Inserer l'id de l'utilisateur
        $sql = "UPDATE evenement SET participants = CONCAT(participants,'$id_participant,') WHERE id=$event_id";
        $query = $pdo->prepare($sql);
        $query->execute();

        $result = [];
        $result["id_participant"] = $id_participant;
        $result["event_id"] = $event_id;
        echo json_encode($result);
    }

    //Bouton pour ne plus participer
    public function unparticipeAction($args) {
        $id_participant = $_POST["id_participant"];
        $event_id = $_POST["id_evenement"];
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
        //recuperation des participants
        $sql = "SELECT participants FROM evenement WHERE id=$event_id";
        $query = $pdo->query($sql);
        if($query){
            $row = $query->fetch(PDO::FETCH_ASSOC);
            if($row){
                $participants =  $row["participants"];
            }
        }
        //Enlever le participant et casser le tableau en 2
        $exp = explode(',' . $id_participant . ',', $participants);
        //On concatene les deux tableau en string avec une virgule
        $new_participants = $exp[0] . ',' . $exp[1];
        //Inserer la nouvelle chaine de caractere
        $sql = "UPDATE evenement SET participants='$new_participants' WHERE id=$event_id";
        $query = $pdo->prepare($sql);
        $query->execute();
    }

    //Bouton pour commenter
    public function commentaireAction($args) {
        $id_user = $_POST["id_user"];
        $event_id = $_POST["id_evenement"];
        //htmlspecialchars pour ne pas gérer les script
        //utf8_decode pour convertir en ISO
        $commentaire = utf8_decode(htmlspecialchars($_POST["commentaire"]));
        $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
        $pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

        //Inserer le commentaire dans la table commentaire
        if (!empty($commentaire)){
            $sql = "INSERT INTO commentaire (contenu, id_user, id_element, date_comment) VALUES (\"$commentaire\", $id_user, $event_id, NOW())";
            $query = $pdo->prepare($sql);
            $query->execute();

            //Recupérer l'id du commentaire insérer
            $id_commentaire = $pdo->lastInsertId();

            ///Si la case commentaires est vide on insere une virgule
            $sql = "SELECT commentaires FROM evenement WHERE id=$event_id";
            $query = $pdo->query($sql);
            if($query){
                $row = $query->fetch(PDO::FETCH_ASSOC);
                if($row){
                    $commentaires =  $row["commentaires"];
                }
            }
            //inserer une virgule au debut
            if ($commentaires == ''){
                $sql = "UPDATE evenement SET commentaires = ',' WHERE id=$event_id";
                $query = $pdo->prepare($sql);
                $query->execute();
            }

            //Inserer l'id du commentaire dans l'event
            $sql = "UPDATE evenement SET commentaires = CONCAT(commentaires,'$id_commentaire,') WHERE id=$event_id";
            $query = $pdo->prepare($sql);
            $query->execute();
        }
    }


    public function testAction($args){
        $post = new post();
        //$post->set_id(1);
        $post->set_title("Mon Article");
        $post->set_content("Ma description");
        //$post->save();

        $v = new view();
        $v->setView("event");
        $v->assign("pseudo","Toto");

    }
}

