<?php
global $home_url;
require 'phpmailer/class.phpmailer.php';
require 'phpmailer/PHPMailerAutoload.php';
require 'phpmailer/class.smtp.php';

class userController {

    public function indexAction($args) {
      global $home_url;


        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
            $pseudo = $_GET['pseudo'];
            $user = [];
            $mini_bio = "";
            $participating = [];
            $participated = [];
            $fan = [];
            $personnalDashboard = NULL;
            $v = new View();
            session_start();

            if (!empty($_POST) && !empty(trim($_POST['input_user']))) {
                $user = $this->post($v);
            } else {
                if(!empty($_SESSION['user']) && !empty($_SESSION['user']['id'])){
                  $session_user_id = $_SESSION['user']['id'];
                }else{
                  $session_user_id = 0;
                }
                $id_user = $session_user_id;

                $sql = "SELECT * FROM user WHERE pseudo='$pseudo'";

                $stmt = $this->pdo->query($sql);

                if ($stmt) {
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $user[] = $row;
                    }

                    if($user[0]["id"] == $id_user){
                      if ($user[0]["status"] == 2){
                        $personnalDashboard .= '<p><a href="'.$home_url.'/admin">Accéder à la page d\'administration</a></p>';
                        $personnalDashboard .= '<br>';
                      }
                      $personnalDashboard .= '<p><a href="'.$home_url.'/user/modifyAvatar">Changer votre avatar</a></p>';
                      $personnalDashboard .= '<p><a href="'.$home_url.'/user/modifyTown">Changer votre ville</a></p>';
                      $personnalDashboard .= '<p><a href="'.$home_url.'/user/modifyBio">Changer votre mini bio</a></p>';
                      $personnalDashboard .= '<p><a href="'.$home_url.'/user/modifyPassword">Changer de mot de passe</a></p>';
                      $personnalDashboard .= '<br>';
                      $personnalDashboard .= '<p><a href="'.$home_url.'/user/deleteAccount" class="delete">Supprimer mon compte</a></p>';
                    }

                    //Recuperation de l'image et de la bio
                    $sql2 = "SELECT lien FROM media WHERE id =".$user[0]['id_media'];
                    $stmt2 = $this->pdo->query($sql2);
                    $result = $stmt2->fetch(PDO::FETCH_ASSOC);
                    $profile_pic = $result['lien'];
                    if (!empty($user[0]['mini_bio'])){
                      $mini_bio = "\"".$user[0]['mini_bio']."\"";
                    }

                    //Gestion des évènements qui vont arriver
                    $sql3 = "SELECT evenement.id, evenement.libelle as libelle_event, evenement.date_deb, evenement.date_fin, p.lien as photo, b.lien as banniere FROM evenement, media p, media b WHERE id_photo = p.id AND id_banner = b.id  AND evenement.date_fin > CURDATE() AND participants LIKE '%,".$user[0]['id'].",%'";
                    $stmt3 = $this->pdo->query($sql3);
                    while ($row = $stmt3->fetch(PDO::FETCH_ASSOC)) {
                        $participating[] = $row;
                    }

                    //Gestion des évènements qui sont passés
                    $sql4 = "SELECT evenement.id, evenement.libelle as libelle_event, evenement.date_deb, evenement.date_fin, p.lien as photo, b.lien as banniere FROM evenement, media p, media b WHERE id_photo = p.id AND id_banner = b.id  AND evenement.date_fin < CURDATE() AND participants LIKE '%,".$user[0]['id'].",%'";
                    $stmt4 = $this->pdo->query($sql4);
                    while ($row = $stmt4->fetch(PDO::FETCH_ASSOC)) {
                        $participated[] = $row;
                    }

                    //Gestion des artistes favoris
                    $sql5 = "SELECT artiste.id, pseudo, lien as photo FROM artiste, media WHERE id_photo = media.id AND fans LIKE '%,".$user[0]['id'].",%'";
                    $stmt5 = $this->pdo->query($sql5);
                    while ($row = $stmt5->fetch(PDO::FETCH_ASSOC)) {
                        $fan[] = $row;
                    }
                }
            }
        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }

        $v->setView("user");
        $v->assign("user", $user);
        $v->assign("personnalDashboard", $personnalDashboard);
        $v->assign("profile_pic", $profile_pic);
        $v->assign("mini_bio", $mini_bio);
        $v->assign("participating", $participating);
        $v->assign("participated", $participated);
        $v->assign("fan", $fan);
    }

    public function modifyAvatarAction($args){
      global $home_url;
      $home_url = "http://" . $_SERVER["HTTP_HOST"] . "/nightlives";
      $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
      $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
      $v = new View();
      session_start();

      $sql = "SELECT * FROM user WHERE id=".$_SESSION['user']['id'];
      $stmt = $this->pdo->query($sql);

      if ($stmt) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $user[] = $row;
        }
      }
      $sql2 = "SELECT lien FROM media WHERE id =".$user[0]['id_media'];
      $stmt2 = $this->pdo->query($sql2);
      $result = $stmt2->fetch(PDO::FETCH_ASSOC);
      $profile_pic = $result['lien'];

      //Génère la vue spéciale
      $section = '<div class="user_data" id="modifySection">';
      $section .= '<h2>Changer mon avatar</h2>';
      $section .= '<div class="avatarDisplay">';
      $section .= '<p>Votre avatar actuel:</p> ';
      $section .= '<img src="'.$home_url.'/'.$profile_pic.'">';
      $section .= '</div>';
      $section .= '<br>';

      $section .= '<div id="errorModify">';
      $section .= '</div>';
      $section .= '<br/>';

      $section .= '<div class="avatarDisplay">';
      $section .= '<form method="POST" id="formModify" enctype="multipart/form-data" class="element">';
      $section .= '<label>Mon nouvel avatar (Taille max: 2Mo - Format recommandé: carré): </label>';
      $section .= '<input type="file" name="avatar"></p>';
      $section .= '<input type="hidden" name="type" value="5">';
      $section .= '<input type="submit" value="Confirmer">';
      $section .= '</form>';
      $section .= '</div>';
      $section .= '</div>';

      $v->setView("modify");
      $v->assign("home_url", $home_url);
      $v->assign("user", $user);
      $v->assign("profile_pic", $profile_pic);
      $v->assign("section", $section);

    }


    public function modifyTownAction($args){
      global $home_url;
      $home_url = "http://" . $_SERVER["HTTP_HOST"] . "/nightlives";
      $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
      $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
      $v = new View();
      session_start();

      $sql = "SELECT * FROM user WHERE id=".$_SESSION['user']['id'];
      $stmt = $this->pdo->query($sql);

      if ($stmt) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $user[] = $row;
        }
      }
      $sql2 = "SELECT lien FROM media WHERE id =".$user[0]['id_media'];
      $stmt2 = $this->pdo->query($sql2);
      $result = $stmt2->fetch(PDO::FETCH_ASSOC);
      $profile_pic = $result['lien'];

      //Génère la vue spéciale
      $section = '<div class="user_data" id="modifySection">';
      $section .= '<h2>Changer ma ville</h2>';
      $section .= '<p>Ville actuelle: '.$user[0]['ville'].'</p>';
      $section .= '<br>';

      $section .= '<div id="errorModify">';
      $section .= '</div>';
      $section .= '<br/>';

      $section .= '<form method="POST" id="formModify" class="element">';
      $section .= '<label>Ma nouvelle ville:</label>';
      $section .= '<input type="text" name="town"></p>';
      $section .= '<input type="hidden" name="type" value="1">';
      $section .= '<div class="lineclear"></div>';
      $section .= '<input type="submit" value="Confirmer">';
      $section .= '</form>';
      $section .= '</div>';

      $v->setView("modify");
      $v->assign("home_url", $home_url);
      $v->assign("user", $user);
      $v->assign("profile_pic", $profile_pic);
      $v->assign("section", $section);

    }

    public function modifyBioAction($args){
      global $home_url;
      $home_url = "http://" . $_SERVER["HTTP_HOST"] . "/nightlives";
      $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
      $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
      $v = new View();
      session_start();

      $sql = "SELECT * FROM user WHERE id=".$_SESSION['user']['id'];
      $stmt = $this->pdo->query($sql);

      if ($stmt) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $user[] = $row;
        }
      }
      $sql2 = "SELECT lien FROM media WHERE id =".$user[0]['id_media'];
      $stmt2 = $this->pdo->query($sql2);
      $result = $stmt2->fetch(PDO::FETCH_ASSOC);
      $profile_pic = $result['lien'];

      //Génère la vue spéciale
      $section = '<div class="user_data" id="modifySection">';
      $section .= '<h2>Changer ma mini bio</h2>';
      $section .= '<p>Mini Bio actuelle: "'.$user[0]['mini_bio'].'"</p>';
      $section .= '<br>';

      $section .= '<div id="errorModify">';
      $section .= '</div>';
      $section .= '<br/>';

      $section .= '<form method="POST" id="formModify" class="element">';
      $section .= '<label>Ma nouvelle mini bio: </label>';
      $section .= '<input type="text" name="bio"></p>';
      $section .= '<input type="hidden" name="type" value="2">';
      $section .= '<input type="submit" value="Confirmer">';
      $section .= '</form>';
      $section .= '</div>';

      $v->setView("modify");
      $v->assign("home_url", $home_url);
      $v->assign("user", $user);
      $v->assign("profile_pic", $profile_pic);
      $v->assign("section", $section);

    }

    public function modifyPasswordAction($args){
      $home_url = "http://" . $_SERVER["HTTP_HOST"] . "/nightlives";
      $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
      $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
      $v = new View();
      session_start();

      $sql = "SELECT * FROM user WHERE id=".$_SESSION['user']['id'];
      $stmt = $this->pdo->query($sql);

      if ($stmt) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $user[] = $row;
        }
      }
      $sql2 = "SELECT lien FROM media WHERE id =".$user[0]['id_media'];
      $stmt2 = $this->pdo->query($sql2);
      $result = $stmt2->fetch(PDO::FETCH_ASSOC);
      $profile_pic = $result['lien'];

      //Génère la vue spéciale
      $section = '<div class="user_data" id="modifySection">';
      $section .= '<h2>Changer mon mot de passe</h2>';

      $section .= '<div id="errorModify">';
      $section .= '</div>';
      $section .= '<br/>';

      $section .= '<form method="POST" id="formModify" class="element">';
      $section .= '<label>Saisissez votre nouveau mot de passe:</label>';
      $section .= '<input type="password" name="newPassword">';
      $section .= '<label>Confirmez votre mot de passe actuel:</label>';
      $section .= '<input type="password" name="confirmPassword">';
      $section .= '<input type="hidden" name="type" value="3">';
      $section .= '<input type="submit" value="Confirmer">';
      $section .= '</form>';
      $section .= '</div>';

      $v->setView("modify");
      $v->assign("home_url", $home_url);
      $v->assign("user", $user);
      $v->assign("profile_pic", $profile_pic);
      $v->assign("section", $section);

    }

    public function deleteAccountAction($args){
      global $home_url;
      $home_url = "http://" . $_SERVER["HTTP_HOST"] . "/nightlives";
      $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
      $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
      $v = new View();
      session_start();

      $sql = "SELECT * FROM user WHERE id=".$_SESSION['user']['id'];
      $stmt = $this->pdo->query($sql);

      if ($stmt) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $user[] = $row;
        }
      }
      $sql2 = "SELECT lien FROM media WHERE id =".$user[0]['id_media'];
      $stmt2 = $this->pdo->query($sql2);
      $result = $stmt2->fetch(PDO::FETCH_ASSOC);
      $profile_pic = $result['lien'];

      //Génère la vue spéciale
      $section = '<div class="user_data" id="modifySection">';
      $section .= '<h2>Supprimer mon compte</h2>';

      $section .= '<form method="POST" id="formModify">';
      $section .= '<p>Souhaitez-vous réellement supprimer votre compte?</p>';
      $section .= '<input type="hidden" name="type" value="4">';
      $section .= '<input type="submit" value="Confirmer">';
      $section .= '<a href="'.$home_url.'/user?pseudo='.$_SESSION['user']['pseudo'].'"><input type="button" value="Annuler"></a>';
      $section .= '</form>';
      $section .= '</div>';

      $v->setView("modify");
      $v->assign("home_url", $home_url);
      $v->assign("user", $user);
      $v->assign("profile_pic", $profile_pic);
      $v->assign("section", $section);
    }

    public function modificationAccountAction($args){
      global $home_url;
      $home_url = "http://" . $_SERVER["HTTP_HOST"] . "/nightlives";
      $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
      $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
      $type = $_POST['type'];
      session_start();
      $id_user = $_SESSION['user']['id'];
      $error = FALSE;
      $msg_error = "";

      if ($type == 1){
        $town = htmlentities($_POST['town']);
        $town = addslashes($town);
        if(isset($town)){
          $town = strtolower($town);
          $town = ucfirst($town);
          if (strlen($town) <= 1) {
            $error = TRUE;
            $msg_error = "<li>La ville doit être de plus d'un caractère !</li>";
          }
        }
        if ($error) {
          echo $msg_error;
        } else {
          $sql = "UPDATE user SET ville = '".$town."' WHERE id = ".$id_user;
          $stmt = $this->pdo->query($sql);
        }
      }

      if ($type == 2){
        $bio = htmlentities($_POST['bio']);
        $bio = addslashes($bio);
        if(isset($bio)){
          $town = strtolower($bio);
          $town = ucfirst($bio);
          if (strlen($bio) < 0 || strlen($bio) > 70) {
            $error = TRUE;
            $msg_error = "<li>La mini-bio doit être entre 1 et 70 caractères!</li>";
          }
        }
        if ($error) {
          echo $msg_error;
        } else {
          $sql = "UPDATE user SET mini_bio = '".$bio."' WHERE id = ".$id_user;
          $stmt = $this->pdo->query($sql);
        }
      }

      if ($type == 3){
        $newPassword = $_POST['newPassword'];
        $confirmPassword = $_POST['confirmPassword'];
        if(isset($newPassword) && isset($confirmPassword)){
          if (strlen($newPassword) < 6 || strlen($newPassword) > 20) {
            $error = TRUE;
            $msg_error .= "<li>Le mot de passe doit être de 6 à 20 caractères</li>";
          }
          if ($newPassword != $confirmPassword) {
            $error = TRUE;
            $msg_error .= "<li>Le mot de passe de confirmation est différent du mot de passe saisi</li>";
          }
        }
        if ($error) {
          echo $msg_error;
        } else {
          $newPassword = md5($newPassword);
          $sql = "UPDATE user SET mdp = '".$newPassword."' WHERE id = ".$id_user;
          $stmt = $this->pdo->query($sql);
        }
      }

      if ($type == 4){
        $sql = "UPDATE user SET archive = '1' WHERE id = ".$id_user;
        $stmt = $this->pdo->query($sql);
        header("Refresh: 1; URL=".$home_url.""); // NE MARCHE PAS...
        session_destroy();
      }

      if ($type == 5){
        if (isset($_FILES['avatar']) && !empty($_FILES['avatar']['name'])){
          $maxFile = 2097152; // Taille max de 2Mo
          $extensionsOk = [".png", ".jpg", ".jpeg", ".gif"];
          if ($_FILES['avatar']['size'] <= $maxFile){
            $extensionUpload = strtolower(strstr($_FILES['avatar']['name'], '.'));
            if(in_array($extensionUpload, $extensionsOk)){
              $path = move_uploaded_file($_FILES['avatar']['tmp_name'], "./uploads/user/profile/".$_SESSION['user']['pseudo'].$extensionUpload);
              //var_dump( "./uploads/user/profile/".$_SESSION['user']['pseudo'].$extensionUpload);
              if($path){
                $sql = "SELECT id_media FROM user WHERE id = ".$_SESSION['user']['id'];
                $stmt = $this->pdo->query($sql);
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($result['id_media'] == 0){
                  $sql2 = "INSERT INTO media VALUES ('', '1', 'uploads/user/profile/".$_SESSION['user']['pseudo'].$extensionUpload."', 'Photo de profil de ".$_SESSION['user']['pseudo']."', 0)";
                  $stmt2 = $this->pdo->query($sql2);
                  $sql3 = "UPDATE user, media SET user.id_media = media.id WHERE user.pseudo = '".$_SESSION['user']['pseudo']."' AND media.lien LIKE '%".$_SESSION['user']['pseudo']."%'";
                  $stmt3 = $this->pdo->query($sql3);
                } else {
                  $sql2 = "UPDATE media SET lien = 'uploads/user/profile/".$_SESSION['user']['pseudo'].$extensionUpload."' WHERE lien LIKE '%".$_SESSION['user']['pseudo']."%'";
                  $stmt2 = $this->pdo->query($sql2);
                }
              } else {
                $error = TRUE;
                $msg_error .= "<li>Erreur lors du chargement du fichier</li>";
              }
            } else {
              $error = TRUE;
              $msg_error .= "<li>Formats acceptés pour l'avatar: jpg, jpeg, png ou gif</li>";
            }
          } else {
            $error = TRUE;
            $msg_error .= "<li>L'avatar ne doit pas dépasser 2Mo</li>";
          }
        } else {
          $error = TRUE;
          $msg_error .= "<li>Erreur lors du chargement du fichier</li>";
        }
        if ($error) {
          echo $msg_error;
        }
      }
    }

    public function subscribeAction($args) {
        global $pdo, $home_url;
        $pseudo = trim($_POST['pseudo']);
        $nom = trim($_POST['nom']);
        $prenom = trim($_POST['prenom']);
        $ville = trim($_POST['ville']);
        $email = trim($_POST['email']);
        $mdp = $_POST['mdp'];
        $confirmmdp = $_POST['confirmmdp'];
        $mini_bio = "Bébé Nightliver";
        $status = 0; // change to 0
        $id_media = 0;


        $error = FALSE;
        $msg_error = "";

        if (isset($_POST['pseudo']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['ville']) && isset($_POST['email']) && isset($_POST['mdp']) && isset($_POST['confirmmdp'])) {

            if (strlen($pseudo) < 3 || strlen($pseudo) > 20) {
                $error = TRUE;
                $msg_error .= "<li>Le Pseudo doit être de 3 à 20 caractères</li>";
            }

            if (!ctype_alpha($nom)) {
                $error = TRUE;
                $msg_error .= "<li>Le nom ne doit pas contenir de chiffres</li>";
            }

            if (strlen($nom) <= 1) {
                $error = TRUE;
                $msg_error = "<li>Le nom doit être de plus d'un caractère</li>";
            }

            if (!ctype_alpha($prenom)) {
                $error = TRUE;
                $msg_error .= "<li>Le prénom ne doit pas contenir de chiffres</li>";
            }

            if (strlen($prenom) <= 1) {
                $error = TRUE;
                $msg_error = "<li>Le prenom doit être de plus d'un caractère</li>";
            }

            if ($nom === $prenom) {
                $error = TRUE;
                $msg_error .= "<li>Le prénom doit être différent du nom</li>";
            }

            /* if (!ctype_alpha($ville)) {
              $error = TRUE;
              $msg_error .= "<li>La ville ne doit pas contenir de chiffres</li>";
              } */

            if (strlen($ville) <= 1) {
                $error = TRUE;
                $msg_error = "<li>La ville doit être de plus d'un caractère</li>";
            }

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error = TRUE;
                $msg_error .= "<li>Email invalide</li>";
            }

            if (strlen($mdp) < 6 || strlen($mdp) > 20) {
                $error = TRUE;
                $msg_error .= "<li>Le mot de passe doit être de 6 à 20 caractères</li>";
            }

            if ($mdp != $confirmmdp) {
                $error = TRUE;
                $msg_error .= "<li>Le mot de passe de confirmation est différent du mot de passe</li>";
            }
        }

        if ($error) {
            echo $msg_error;
        } else {
            $user = new user();
            $user->set_pseudo($pseudo);
            $user->set_nom($nom);
            $user->set_prenom($prenom);
            $user->set_ville($ville);
            $user->set_email($email);
            $user->set_mdp(md5($mdp));
            $user->set_status($status);
            $user->set_id_media($id_media);
            $user->set_archive(0);
            $user->set_mini_bio($mini_bio);
            $user->set_date_inscription( date("Y-m-d", strtotime("now")) );
            $user->save();

        //Envoie mail
            $user = new user();
            $userinfos = $user->getUser($email);
            $id_user = $userinfos["id"];
            $home_url = "http://" . $_SERVER["HTTP_HOST"] . "/nightlives";
            // Token

            $access_token = md5(microtime(TRUE) * 100000);
            $sql = "UPDATE user SET access_token='$access_token' WHERE id=$id_user;"; //update dans la bdd
            $query = $pdo->prepare($sql);
            $query->execute();

            $destinataire = $email;
            $nom_destinataire = $nom;
            $objet_mail = 'Activer votre compte';
            //$corp_mail = file_get_contents('./emailconfirm.php');

            $corp_mail = '<h2>Bienvenue sur Nightlives,</h2><br><br>
            <p>Pour activer votre compte, veuillez cliquer sur le lien ci dessous ou copier/coller dans votre navigateur internet.<p><br>
            ' . $home_url . '/validinscription?id=' . urlencode($id_user) . '&access_token=' . urlencode($access_token) . '
<br><br>
---------------------------------------------------------<br>
Ceci est un mail automatique, Merci de ne pas y répondre.';

            $mail = new PHPmailer();
            //$mail->SMTPDebug = 4;
            $mail->CharSet = 'UTF-8';
            //$mail->IsSMTP();
            $mail->IsHTML(true);
            $mail->IsQMAIL();
            //$mail->Host = 'smtp.nightlives.fr';  //aussi essayé smtp.mondomaine.com
            $mail->Host = 'smtp.nightlives.fr';  //aussi essayé smtp.mondomaine.com
            $mail->Port = 587;
            // OVH 587
            $mail->Username = 'nightlives@nightlives.fr';      // SMTP login
            $mail->Password = 'nightlives';        // SMTP password
            $mail->SMTPAuth = true;      // Active l'uthentification par smtp
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('nightlives@nightlives.fr', 'nightlives');
            $mail->AddAddress($destinataire, $nom_destinataire);
            //$mail->AddReplyTo('admin@dreameur.fr');
            $mail->Subject = $objet_mail;
            $mail->Body = $corp_mail;

            $headers  = "MIME-Version: 1.0 \n";

            $headers .= "Content-Transfer-Encoding: 8bit \n";

            $headers .= "Content-type: text/html; charset=utf-8 \n";
            $headers .= 'From: Nightlives <contact@nightlives.fr>' . "\r\n";
            $mail_reussite = mail($destinataire, $objet_mail, $corp_mail, $headers);
            //if (!$mail->Send() || $mail_reussite) {
            if (!$mail_reussite) { //Teste si le return code est ok.
                echo "L'email de confirmation n'a pas pu être envoyé, veuillez réessayer plus tard.";
                //echo '<br>' . $mail->ErrorInfo;
            } else {
                //echo 'Mail envoyé avec succès à '.$destinataire.'.';
            }
            $mail->SmtpClose();
            unset($mail);

        //Fin Envoie mail


        //Connexion
            // $query = $pdo->prepare("SELECT status FROM user WHERE nom=$nom;");
            // $query->execute();
            // if ($row = $query->fetch()) {
            //     $actif = $row['actif'];
            // }
            // if ($status == '1') {
            //     //On autorise la connexion
            // } else {
            //     //On refuse la connexion
            // }
        }
    }

    public function connexionAction($args) {
      global $home_url;
        $email = trim($_POST['emailco']);
        $mdp = trim($_POST['pwdco']);
        $error = FALSE;
        $msg_error = "";

        if (empty($email) && empty($mdp)) {
            $error = TRUE;
            $msg_error.= "<li>Veuillez renseigner ces champs.</li> ";
            echo $msg_error;
        } else {

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error = TRUE;
                $msg_error .= "<li>Email invalide.</li>";
                echo $msg_error;
            } else {
                $user = new user();
                $userinfos = $user->getUser($email);


                if (!empty($userinfos)) {
                    if ($userinfos['mdp'] === md5($mdp) && ($userinfos['status'] == 1 || $userinfos['status'] == 2) ) {
                        session_start();
                        //$_SESSION["user"] = $userinfos;
                        $_SESSION["user"]["id"] = $userinfos["id"];
                        $_SESSION["user"]["pseudo"] = $userinfos["pseudo"];
                        $_SESSION["user"]["photo"] = $userinfos["photo"];
                    } else {
                        $error = TRUE;
                        $msg_error.= "<li>L'adresse email ou le mot de passe sont incorrects. 1</li>";
                    }
                } else {
                    $error = TRUE;
                    $msg_error.= "<li>L'adresse email ou le mot de passe sont incorrects. 2</li>";
                }
                if ($error) {
                    echo $msg_error;
                }
            }
        }
    }

    public function deconnexionAction() {
        global $home_url;
        session_start();
        session_destroy();
        header('Location: ' . $home_url);
    }

}
