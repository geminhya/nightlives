<?php

class indexController{
    
    public function indexAction($args){
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        save_page_count($actual_link, "Accueil", 'page');
        //echo "<pre>";
        $user = new user();
        $user->set_nom("Kerroux");
        $user->set_prenom("Noémie");
        $user->set_pseudo("geminhya");
        $user->set_email("noemie.kerroux@gmail.com");
        //$user->save();
        // $post->set_content("Ma description");

        $v = new view();
        $v->setView("index");
        $v->assign("pseudo","Toto");
        

        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

            $this->set_slider($v);
            $this->set_festivals($v);
            $this->set_concerts($v);
            $this->set_soirees($v);
            $this->set_artistes($v);
        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }
        
    }

    private function set_slider($v){
        $slides = [];
        $sql = "SELECT u.*, m.lien as photo, m.libelle as alt FROM slide u, media m WHERE u.id_media = m.id ORDER BY id DESC;";
        $stmt = $this->pdo->query($sql);
        if($stmt){
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $slides[] = $row;
            }
        }
        $v->assign("slides",$slides);
    }

    private function set_festivals($v){
        $slides = [];
        $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt FROM evenement u, media mp, media mb WHERE u.id_photo = mp.id AND u.id_banner = mb.id AND date_fin > NOW() AND id_type_event = 1  ORDER BY date_deb, date_fin ASC LIMIT 0,3;";
        $stmt = $this->pdo->query($sql);
        if($stmt){
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $slides[] = $row;
            }
        }
        $v->assign("festivals",$slides);
    }

    private function set_concerts($v){
        $slides = [];
        $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt FROM evenement u, media mp, media mb WHERE u.id_photo = mp.id AND u.id_banner = mb.id AND date_fin > NOW() AND id_type_event = 2  ORDER BY date_deb, date_fin ASC LIMIT 0,3;";
        $stmt = $this->pdo->query($sql);
        if($stmt){
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $slides[] = $row;
            }
        }
        $v->assign("concerts",$slides);
    }

    private function set_soirees($v){
        $slides = [];
        $sql = "SELECT u.*, mp.lien as photo, mb.lien as banner, mp.libelle as alt FROM evenement u, media mp, media mb WHERE u.id_photo = mp.id AND u.id_banner = mb.id AND date_fin > NOW() AND id_type_event = 3  ORDER BY date_deb, date_fin ASC LIMIT 0,3;";
        $stmt = $this->pdo->query($sql);
        if($stmt){
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $slides[] = $row;
            }
        }
        $v->assign("soirees",$slides);
    }

    private function set_artistes($v){
        $slides = [];
        $sql = "SELECT u.pseudo, u.id, mp.lien as photo, u.easy_name
                    FROM artiste u, media mp
                    WHERE u.id_photo = mp.id AND u.archive = 0 ORDER BY id DESC LIMIT 0,4;";
        $stmt = $this->pdo->query($sql);
        if($stmt){
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $slides[] = $row;
            }
        }
        $v->assign("artistes",$slides);
    }
}

