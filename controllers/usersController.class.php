<?php

class usersController{

    public function indexAction($args){

        $v = new view();
        $v->setView("users");
        $v->assign("title","Nightlivers");
        $background_url = "images/users/background.jpeg";
        $uri_parts = explode('&', $_SERVER['REQUEST_URI'], 2);
        $actual_link = "http://$_SERVER[HTTP_HOST]" . $uri_parts[0];
        save_page_count($actual_link, "Artistes", 'page');

        $type = 0;
        $order = "pseudo";
        $conditions = "";
        $v->assign("head_title","Membres - ");

        save_page_count($actual_link, "Membres", 'page');
        $conditions .= " ORDER BY " . $order;
        $v->assign("type",$type);
        $v->assign("background_url",$background_url);

        try {
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);

            $elements = [];
            $admins = [];
            $sql = "SELECT u.pseudo, u.id, mp.lien as photo
                    FROM user u, media mp
                    WHERE u.status = 1 AND u.id_media = mp.id " . $conditions;
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $elements[] = $row;
                }
            }
            $sql = "SELECT u.pseudo, u.id, mp.lien as photo
                    FROM user u, media mp
                    WHERE u.status = 2 AND u.id_media = mp.id " . $conditions;
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $admins[] = $row;
                }
            }

            $v->assign("elements", $elements);
            $v->assign("admins", $admins);

            $select_pays = [];
            $sql = "SELECT DISTINCT(pays) FROM artiste WHERE pays IS NOT NULL ORDER BY pays;";
            $stmt = $this->pdo->query($sql);
            if($stmt){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $select_pays[] = $row;
                }
            }
            $v->assign("select_pays", $select_pays);



        } catch (Exception $e) {
            die("Erreur SQL : " . $e->getMessage());
        }
    }

    public function testAction($args){
        echo "bonjour";
    }
}

