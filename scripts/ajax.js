jQuery(document).ready(function () {


    jQuery("#formsubscribe").submit(function () {
        jQuery.ajax({
            method: "POST",
            url: "/nightlives/user/subscribe",
            data: jQuery("#formsubscribe").serialize()
        })
        .done(function (msg) {
            console.log(msg);
            if (msg.length > 0) {
                jQuery("#errorsubscribe").html(msg);
            } else {
                //console.log(msg);
                $('#formsubscribe').hide();
                $('#errorsubscribe').hide();
                jQuery("#entetepopin").empty();
                $('#entetepopin').append("<h2>Vous êtes presque un Nighliver!</h2><p>Veuillez confirmer votre inscription en cliquant dans le lien fourni dans le mail d'inscription que vous allez recevoir et vous ferez partie de la famille Nightlives pour de bon!</p>");
            }
        });
        return false;
    });


    jQuery("#formconnexion").submit(function () {
        jQuery.ajax({
            method: "POST",
            url: "/nightlives/user/connexion",
            data: jQuery("#formconnexion").serialize()
        })
                .done(function (msg) {
                    if (msg.length > 0) {
                        jQuery("#errorconnexion").html(msg);
                        jQuery("#formsubscribe").hide();
                    } else {
                        $('#formconnexion').hide();
                        $('#errorconnexion').hide();
                        $('#connexion h2').hide();
                        jQuery("#entetepopin").empty();
                        $('#connexion #entetepopin').append("<h2>Salutations, Nighliver!</h2>");
                        location.reload(1000);
                    }
                });
        return false;
    });

    /*jQuery("#bouton_participe").click(function () {
        var userid = $(this).data("userid");
        var eventid = $(this).data("eventid");
        jQuery.ajax({
            type : "POST" ,
            url :  "/nightlives/event/participe" ,
            data: {id_participant: userid,
                   id_evenement: eventid},
        })
        .done(function (result) {
            console.log(result);
            result = jQuery.parseJSON(result);
            console.log(result);
            //jQuery("#bouton_participe").html('Participation prise en compte');
            jQuery("#participate_container").html('<a id="bouton_unparticipe" class="call_to long" href="#">Je ne participe plus !</a>');

            var temp = '<div class="fan"><a href="/nightlives/participant?id=153"><img src="default/nightlives_default_user.jpg"><p class="pseudo">elmomito</p></a></div><div class="lineclear"></div>';

            jQuery(".fans .content").html(jQuery(".fans .content").html() + temp);
        });

    return false;
    });*/

//Bouton pour participer
    jQuery("#bouton_participe").click(function () {
        var userid = $(this).data("userid");
        var eventid = $(this).data("eventid");
        jQuery.ajax({
            type : "POST" ,
            url :  "/nightlives/event/participe" ,
            data: {id_participant: userid,
                   id_evenement: eventid},
        })
        .success(function (result) {
            console.log(result);
            result = jQuery.parseJSON(result);
            console.log(result);
            //jQuery("#bouton_participe").html('Participation prise en compte');
            //on vide la div et on le cache
            //jQuery("#zone_de_rechargement").empty().hide();
            //on affiche les resultats avec la transition
            //jQuery("#zone_de_rechargement").fadeIn(2000);
            location.reload() ;
        });

    return false;
    });

//Bouton pour annuler la participation
    jQuery("#bouton_unparticipe").click(function () {
        //alert("test");
        var userid = $(this).data("userid");
        var eventid = $(this).data("eventid");
        jQuery.ajax({
            type : "POST" ,
            url :  "/nightlives/event/unparticipe" ,
            data: {id_participant: userid,
                   id_evenement: eventid,
                  },
        })
        .success(function () {
            //jQuery("#bouton_unparticipe").html('Participation prise en compte');
            //on vide la div et on le cache
            //jQuery("#zone_de_rechargement").empty().hide();
            //on affiche les resultats avec la transition
            //jQuery("#zone_de_rechargement").fadeIn(2000);
            location.reload() ;
        });

    return false;
    });


    //Bouton pour commentaire
    jQuery("#bouton_commentaire").click(function () {
        //alert("test");
        var userid = $(this).data("userid");
        var eventid = $(this).data("eventid");
        var commentaire = jQuery("#mon_commentaire").val();
        jQuery.ajax({
            type : "POST" ,
            url :  "/nightlives/event/commentaire" ,
            data: {id_user: userid,
                   id_evenement: eventid,
                   commentaire: commentaire,
                  },
        })
        .success(function () {
            //jQuery("#bouton_commentaire").html('Commentaire pris en compte');
            location.reload() ;
        });


    return false;
    });

///////////////////// Page artiste //////////////////

    //Bouton pour participer
    jQuery("#bouton_fan").click(function () {
        var userid = $(this).data("userid");
        var eventid = $(this).data("eventid");
        jQuery.ajax({
            type : "POST" ,
            url :  "/nightlives/artiste/participe" ,
            data: {id_participant: userid,
                   id_evenement: eventid},
        })
        .success(function (result) {
            location.reload() ;
        });

    return false;
    });

    //Bouton pour ne plus etre fan
    jQuery("#bouton_nonfan").click(function () {
        var userid = $(this).data("userid");
        var eventid = $(this).data("eventid");
        jQuery.ajax({
            type : "POST" ,
            url :  "/nightlives/artiste/unparticipe" ,
            data: {id_participant: userid,
                   id_evenement: eventid,
                  },
        })
        .success(function () {
            location.reload() ;
        });

    return false;
    });

    //Bouton pour fan commentaire
    jQuery("#bouton_fun_commentaire").click(function () {
        //alert("test");
        var userid = $(this).data("userid");
        var eventid = $(this).data("eventid");
        var commentaire = jQuery("#fun_commentaire").val();
        jQuery.ajax({
            type : "POST" ,
            url :  "/nightlives/artiste/commentaire" ,
            data: {id_user: userid,
                   id_evenement: eventid,
                   commentaire: commentaire,
                  },
        })
        .success(function () {
            location.reload() ;
        });


    return false;
    });


////////////////////////////////////////////////////

    jQuery("#formModify").submit(function(){
      jQuery.ajax({
            method: "POST",
            url: "/nightlives/user/modificationAccount",
            contentType: false,
            processData: false,
            data: new FormData(this)
        })
          .done(function (msg) {
            if (msg.length > 0) {
              jQuery("#errorModify").html(msg);
            } else {
              $('#modifySection').empty();
              $('#modifySection').append("<p>Modification effectuée!</p>");
            }
          });
        return false;
    });

    jQuery("#form_contribuer_add_event").submit(function(e){
        e.preventDefault();
        var form_id = "#form_contribuer_add_event";
        var form_type = "event";
        var input_name = "";
        var datas = [];
        var errors = [];
        var has_errors = false;


        input_name = form_id + " input[name='" + form_type + "[libelle]']";
        datas['libelle'] = jQuery(input_name);

        input_name = form_id + " input[name='" + form_type + "[date_deb]']";
        datas['date_deb'] = jQuery(input_name);
        
        input_name = form_id + " input[name='" + form_type + "[date_fin]']";
        datas['date_fin'] = jQuery(input_name);
        
        input_name = form_id + " select[name='" + form_type + "[id_type_event]']";
        datas['id_type_event'] = jQuery(input_name);
        
        input_name = form_id + " select[name='" + form_type + "[id_lieu]']";
        datas['id_lieu'] = jQuery(input_name);
        
        input_name = form_id + " input[name='" + form_type + "[date_deb]']";
        datas['date_deb'] = jQuery(input_name);
        
        input_name = form_id + " input[name='" + form_type + "[informations]']";
        datas['informations'] = jQuery(input_name);
        
        input_name = form_id + " input[name='" + form_type + "[artistes]']";
        datas['artistes'] = jQuery(input_name);

        if( datas['libelle'].val().trim().length == 0 ){
            errors['libelle'] = "Le libelle doit être rempli";
        }
        if( datas['date_deb'].val().trim().length == 0 ){
            errors['date_deb'] = "La date de début doit être remplie";
        }
        if( datas['date_fin'].val().trim().length == 0 ){
            errors['date_fin'] = "La date de fin doit être remplie";
        }
        if( datas['id_type_event'].val() == 'X' || datas['id_type_event'].val().length == 0 ){
            errors['id_type_event'] = "La date de fin doit être remplie";
        }
        if( datas['id_lieu'].val() == 'X' ||  datas['id_lieu'].val().length == 0 ){
            errors['id_lieu'] = "Le lieu doit être remplis";
        }
        if( datas['artistes'].val().length == 0 ){
            errors['artistes'] = "La date de fin doit être remplie";
        }
        jQuery("p.error_para").each(function(){
            jQuery(this).remove();
        });
        if(typeof errors['libelle'] != 'undefined' && errors['libelle'].length > 0){
            jQuery('input[name="event[libelle]"]').addClass("error");
            has_errors = true;
        }
        if(typeof errors['date_deb'] != 'undefined' && errors['date_deb'].length > 0){
            jQuery('input[name="event[date_deb]"]').addClass("error");
            has_errors = true;
        }
        if(typeof errors['date_fin'] != 'undefined' && errors['date_fin'].length > 0){
            jQuery('input[name="event[date_fin]"]').addClass("error");
            has_errors = true;
        }
        if(typeof errors['id_type_event'] != 'undefined' && errors['id_type_event'].length > 0){
            jQuery('select[name="event[id_type_event]"]').addClass("error");
            has_errors = true;
        }
        if(typeof errors['id_lieu'] != 'undefined' && errors['id_lieu'].length > 0){
            jQuery('select[name="event[id_lieu]"]').addClass("error");
            has_errors = true;
        }
        if(typeof errors['artistes'] != 'undefined' && errors['artistes'].length > 0){
            jQuery('.autocomplete_input').addClass("error");
            has_errors = true;
        }

        jQuery(".form_container").before("<p class='error_para'>Il y a des erreurs dans le formulaire.</p>");

        if(!has_errors){
            jQuery.ajax({
                method: "POST",
                url: "/nightlives/contribuer/addevent",
                data: { data: datas }
            })
            .done(function (data) {
                data = jQuery.parseJSON(data);
                var total = "";
                for (var i = 0; i < data.length; i++) {
                    total += "<div ";
                    total += "data-id='" + data[i].id + "' class='autcomplete_list_element' >";
                    total += data[i].pseudo;
                    total += "</div>";
                }
            });
        }
        console.log(errors);
        console.log(datas);
        
    });

});
