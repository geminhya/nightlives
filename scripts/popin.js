jQuery(document).ready(function () {
    jQuery(".closePopin").click(function () {
        jQuery("#overlay").fadeToggle();
        jQuery(".popin").each(function () {
            jQuery(this).removeClass("hide");
        });

    });
    jQuery(".callPopin").click(function (e) {
        e.preventDefault();
        var idPopin = jQuery(this).attr("href");
        if (jQuery(window).width() >= 768) {
            var hauteurPopin = jQuery(idPopin).outerHeight();
            var test = "calc( (100vh - " + hauteurPopin + "px) / 2 )";
            jQuery(idPopin).css("top", test);
        }
        jQuery("#overlay").fadeToggle();
        jQuery(".popin").each(function () {
            jQuery(this).removeClass("hide");
        });
        jQuery(idPopin).addClass("hide");
    });
    jQuery("#overlay").click(function () {
        jQuery(".popin").each(function () {
            jQuery(this).removeClass("hide");
        });
        jQuery(this).fadeToggle();
    });
});


