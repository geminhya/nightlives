/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function afficheArticle(postid) {
    $(".article").hide("slow");
        var lesarticles = $('.article[post-id="'+ postid +'"]');
        $.ajax({
            url: "/nightlives/articles/",
            type: "GET",
            data: {
                id_redacteur:postid
            },
            complete:function(){
                console.log("Success !");
//                var url_location = '/' ;
//                var url_splitted = this.url.split('/');
//                url_location += url_splitted[2];
//                url_location += url_splitted[3];
//                console.log(url_location);
                location.href = this.url;
                
                
            }
            
        });
        lesarticles.show("slow");
}

jQuery(document).ready(function () {
    $(".lire_moins").hide();
    jQuery("input[name='clickcomment']").click(function () {
        console.log($(this).val());
        var postid = $(this).attr('post-id');
        var lescomms = $('.lescomms[post-id="' + postid + '"]');
        lescomms.show("slow");
    });
    jQuery(".lire_plus").click(function(){
        var postid = $(this).attr('post-id');
        var lepara = $('.paragraphe_article[post-id="' + postid + '"]');
        var imgpara = $('.thumbnail[post-id="' + postid + '"]');
        lepara.css("white-space","normal");
        imgpara.css("width", "80%");
        var liremoins = $('.lire_moins[post-id="'+ postid +'"]');
        $(".lire_plus[post-id="+postid+"]").hide("slow");
        liremoins.show("slow");
    });
    jQuery(".lire_moins").click(function(){
        
        var postid = $(this).attr('post-id');
        var lireplus = $('.lire_plus[post-id="'+ postid +'"]');
        var lepara = $('.paragraphe_article[post-id="' + postid + '"]');
        var imgpara = $('.thumbnail[post-id="' + postid + '"]');
        lepara.css("white-space","nowrap");
        imgpara.css("width", "30%");
        lireplus.show("slow");
        $(this).hide("slow");
    });
    jQuery("input[name='annuleCommentaire']").click(function () {
        var postid = $(this).attr('post-id');
        var annulecomm = $('.lescomms[post-id="' + postid + '"]');
        annulecomm.hide("slow");
    });
    jQuery(".afficheCom").click(function () {
        var postid = $(this).attr('post-id');
        var afficheCommentaire = $('.afficheCommentaire[post-id="' + postid + '"]');
        var cacheCommentaire = $('.cacherCom[post-id="' + postid + '"]');
        afficheCommentaire.show("slow");
        cacheCommentaire.show("slow");
        $(this).hide("slow");
    });
    jQuery(".cacherCom").click(function () {
        var postid = $(this).attr('post-id');
        var afficheCommentaire = $('.afficheCommentaire[post-id="' + postid + '"]');
        var cacheCommentaire = $('.afficheCom[post-id="' + postid + '"]');
        afficheCommentaire.hide("slow");
        cacheCommentaire.show("slow");
        $(this).hide("slow");
    });
});


