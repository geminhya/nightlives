function fn60sec() {
    jQuery(".home_slider_arrows .right").click();
}
	
jQuery(document).ready(function() {

	/* HOME SLIDER */
	var slider_attr = ".home_slider";
	var slider_width = jQuery(slider_attr).width();

	var counting = "";
	var cpt = 1;
	var interval_home_slider = 4000;
	jQuery(slider_attr + " .slide").each(function(){
		counting += "<span class='point' data-slide='" + cpt + "'></span>";
		cpt++;
	});
	jQuery(slider_attr + " .counting").html(counting);
	jQuery(slider_attr + " .counting .point:first-child").addClass("current");
	jQuery(slider_attr + " .slide:first-child").addClass("current");

	jQuery(slider_attr + " .counting .point").click(function(e){
		e.preventDefault();
		clearInterval(timerID);
		var current_slide = jQuery(slider_attr + " .counting .point.current").data("slide");
		var new_slide = jQuery(this).data("slide");
		jQuery(slider_attr + " .counting .point").each(function(){
			if(jQuery(this).hasClass("current")){
				jQuery(this).removeClass("current");
			}
		});
		jQuery(this).addClass("current");
		jQuery("#home_slider_" + current_slide).removeClass("current");
		jQuery("#home_slider_" + new_slide).addClass("current");
		timerID = setInterval(fn60sec, interval_home_slider);
	});

	jQuery(slider_attr + " .counting .point").click(function(e){
		e.preventDefault();
		clearInterval(timerID);
		var current_slide = jQuery(slider_attr + " .counting .point.current").data("slide");
		var new_slide = jQuery(this).data("slide");
		jQuery(slider_attr + " .counting .point").each(function(){
			if(jQuery(this).hasClass("current")){
				jQuery(this).removeClass("current");
			}
		});
		jQuery(this).addClass("current");
		jQuery("#home_slider_" + current_slide).removeClass("current");
		jQuery("#home_slider_" + new_slide).addClass("current");
		timerID = setInterval(fn60sec, interval_home_slider);
	});

	jQuery(".home_slider_arrows .left").click(function(){
		clearInterval(timerID);
		var current_slide = jQuery(slider_attr + " .counting .point.current").data("slide");
		var new_slide = current_slide - 1;
		if(new_slide == 0){
			new_slide = jQuery(slider_attr + " .counting .point:last-child").data("slide");
		}
		jQuery(slider_attr + " .counting .point").each(function(){
			if(jQuery(this).hasClass("current")){
				jQuery(this).removeClass("current");
			}
		});
		jQuery(slider_attr + " .counting").children().eq(new_slide - 1).addClass("current");
		jQuery("#home_slider_" + current_slide).removeClass("current");
		jQuery("#home_slider_" + new_slide).addClass("current");
		timerID = setInterval(fn60sec, interval_home_slider);
	});

	jQuery(".home_slider_arrows .right").click(function(){
		clearInterval(timerID);
		var current_slide = jQuery(slider_attr + " .counting .point.current").data("slide");
		var new_slide = current_slide + 1;
		if(current_slide == jQuery(slider_attr + " .counting .point:last-child").data("slide")){
			new_slide = 1;
		}
		jQuery(slider_attr + " .counting .point").each(function(){
			if(jQuery(this).hasClass("current")){
				jQuery(this).removeClass("current");
			}
		});
		jQuery(slider_attr + " .counting").children().eq(new_slide - 1).addClass("current");
		jQuery("#home_slider_" + current_slide).removeClass("current");
		jQuery("#home_slider_" + new_slide).addClass("current");
		timerID = setInterval(fn60sec, interval_home_slider);
	});

	
	var timerID = setInterval(fn60sec, interval_home_slider);
});


