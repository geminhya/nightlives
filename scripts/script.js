jQuery(document).ready(function() {
	
	jQuery(".mob_menu .button").click(function(){
		jQuery(".mob_menu ul").toggleClass("open");
	});

	jQuery(".nice_scroll").click(function(e){
		e.preventDefault();
		var target_id = jQuery(this).attr("href");
		jQuery('html, body').animate({
	        scrollTop: jQuery(target_id).offset().top - 118
	    }, 1000);
	});

	if(jQuery(".single_artist").length > 0){
		var text = jQuery(".slider_container h1");
		var text_length = text.text().length;
		if(text_length > 10 && text_length <= 15){
			text.addClass("middle");
		}else if(text_length > 15 && text_length <= 20){
			text.addClass("minim");
		}else if(text_length > 20){
			text.addClass("hardcore");
		}

	}
});


